﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows;
using System.Dynamic;
using System.Diagnostics;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public class iTheckClassMXDeltaBox
    {
        public static string[] Operation;
        public static int TabOperationLength()
        {
            int pAns = 0;
            string opp = ReadOperation(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold");
            for (int i = 0; i < opp.Length; i++) if (opp[i] == ';') pAns++;
            return pAns;
        }
        public static string Delete(string data)
        {
            string del = string.Empty;
            for (int i = 0; i < data.Length - 1; i++) del += data[i];
            return del;
        }
        public static string FParenthese(string data)
        {
            int ouvert = 0, fermer = 0;
            string ansString = string.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == '(') ouvert++;
                if (data[i] == ')') fermer++;
            }
            if (ouvert > fermer) ansString = ")";
            else if (ouvert == fermer) ansString = string.Empty;
            else if (ouvert < fermer) ansString = "(";
            return ansString;
        }
        public static string FValeurAbs(string data)
        {
            int ouvert = 0, fermer = 0;
            string ansString = string.Empty;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == '[') ouvert++;
                if (data[i] == ']') fermer++;
            }
            if (ouvert > fermer) ansString = "]";
            else if (ouvert == fermer) ansString = "[";
            return ansString;
        }
        public static void SaverOperation(string Opp, string Path)
        {
             
            try
            {
                    StreamWriter Write = new StreamWriter(Path, true, Encoding.UTF32);
                    string donnee = Opp + ";";
                    Write.Write(donnee);
                    Write.Close();
                
            }
            catch
            {
            }
        }
        public static string ReadOperation(string Path)
        {
            string data = string.Empty;
            try
            {

                StreamReader Read = new StreamReader(Path);
                data = Read.ReadToEnd();
                Read.Close();
            }
            catch
            {
            }
            return data;
        }
        public static void InitialiseData1()
        {
            int k = -1;
            string data = string.Empty;
            Operation = new string[TabOperationLength()];
            for (int i = 0; i < ReadOperation(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold").Length; i++)
            {
                if (ReadOperation(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold")[i] != ';') data += ReadOperation(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold")[i];
                else
                {
                    k++;
                    Operation[k] = data;
                    data = string.Empty;
                }
            }
        }
        ///
        public static string Saisie(string text, string txt, int pst, int textLength)
        {
            string ans = string.Empty;
            if (textLength != 0)
            {
                if (pst != 0)
                {
                    for (int i = 0; i < textLength; i++)
                    {
                        ans += text[i];
                        if (i == pst - 1) ans += txt;
                    }
                }
                else
                {
                    ans += txt;
                    ans += text;
                }
            }
            else ans = txt;
            return ans;
        }
        ///
       public static string TouchDelete(string text, int pst, int textLength)
        {
            string ans = string.Empty;
            if (textLength > 0)
            {
                if (pst > 0)
                {
                    for (int i = 0; i < textLength; i++)
                    {
                        if (i != pst - 1) ans += text[i];
                    }
                }
            }
            return ans;
        }
       
    }
}
