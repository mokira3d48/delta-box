﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public partial class WellcomeForm : Form
    {
        public WellcomeForm()
        {
            InitializeComponent();
        }
        public int Ttime { get; set; }
        private int Compte = -1;

        private void WellcomeForm_Shown(object sender, EventArgs e)
        {

        }

        private void WellcomeForm_Load(object sender, EventArgs e)
        {

        }

        private void timer_Tick(object sender, EventArgs e)
        {
            Compte++;
            if (Compte == Ttime) this.Close();
        }
    }
}
