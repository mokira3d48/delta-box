﻿namespace AMGT32MXDeltaBox_CodiTheck
{
    partial class MessageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.Rectangle = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Rectangle2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.Message = new System.Windows.Forms.Label();
            this.Title = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.Rectangle,
            this.Rectangle2});
            this.shapeContainer1.Size = new System.Drawing.Size(350, 250);
            this.shapeContainer1.TabIndex = 0;
            this.shapeContainer1.TabStop = false;
            // 
            // Rectangle
            // 
            this.Rectangle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Rectangle.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.Rectangle.BorderColor = System.Drawing.Color.White;
            this.Rectangle.Location = new System.Drawing.Point(30, 25);
            this.Rectangle.Name = "Rectangle";
            this.Rectangle.SelectionColor = System.Drawing.Color.Transparent;
            this.Rectangle.Size = new System.Drawing.Size(270, 180);
            this.Rectangle.Click += new System.EventHandler(this.Rectangle2_Click);
            this.Rectangle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keys);
            // 
            // Rectangle2
            // 
            this.Rectangle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Rectangle2.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.Rectangle2.BorderColor = System.Drawing.Color.White;
            this.Rectangle2.Location = new System.Drawing.Point(7, 8);
            this.Rectangle2.Name = "Rectangle2";
            this.Rectangle2.SelectionColor = System.Drawing.Color.Transparent;
            this.Rectangle2.Size = new System.Drawing.Size(270, 180);
            this.Rectangle2.Click += new System.EventHandler(this.Rectangle2_Click);
            this.Rectangle2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keys);
            // 
            // Message
            // 
            this.Message.AutoSize = true;
            this.Message.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Message.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Message.Location = new System.Drawing.Point(47, 35);
            this.Message.Name = "Message";
            this.Message.Size = new System.Drawing.Size(54, 15);
            this.Message.TabIndex = 1;
            this.Message.Text = "Message";
            this.Message.Click += new System.EventHandler(this.Message_Click);
            this.Message.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.keys);
            // 
            // Title
            // 
            this.Title.AutoSize = true;
            this.Title.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Title.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.Title.Location = new System.Drawing.Point(27, 9);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(32, 14);
            this.Title.TabIndex = 2;
            this.Title.Text = "Title";
            this.Title.Click += new System.EventHandler(this.Title_Click);
            this.Title.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.keys);
            // 
            // MessageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gray;
            this.ClientSize = new System.Drawing.Size(350, 250);
            this.Controls.Add(this.Title);
            this.Controls.Add(this.Message);
            this.Controls.Add(this.shapeContainer1);
            this.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ForeColor = System.Drawing.Color.White;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessageForm";
            this.Opacity = 0.9D;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "MessageForm";
            this.TransparencyKey = System.Drawing.Color.Gray;
            this.Load += new System.EventHandler(this.MessageForm_Load);
            this.Click += new System.EventHandler(this.MessageForm_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.keys);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        public System.Windows.Forms.Label Message;
        public System.Windows.Forms.Label Title;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape Rectangle2;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape Rectangle;
    }
}