﻿namespace AMGT32MXDeltaBox_CodiTheck
{
    partial class AMGTProprieteForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonThemeDefault = new System.Windows.Forms.Button();
            this.buttonThemeOrit = new System.Windows.Forms.Button();
            this.buttonThemeGreenCyan = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.buttonThemeAditoid = new System.Windows.Forms.Button();
            this.buttonThemeLidia = new System.Windows.Forms.Button();
            this.buttonThemeRedV = new System.Windows.Forms.Button();
            this.buttonThemeDangerNum = new System.Windows.Forms.Button();
            this.buttonThemeAxAqua = new System.Windows.Forms.Button();
            this.buttonThemeLSingle = new System.Windows.Forms.Button();
            this.buttonThemeLoriate = new System.Windows.Forms.Button();
            this.buttonThemeBlack = new System.Windows.Forms.Button();
            this.buttonThemeNatural = new System.Windows.Forms.Button();
            this.buttonThemePurple = new System.Windows.Forms.Button();
            this.buttonThemeCoBoy = new System.Windows.Forms.Button();
            this.buttonThemeAliceBlue = new System.Windows.Forms.Button();
            this.buttonThemeSilverDark = new System.Windows.Forms.Button();
            this.buttonThemeDarkSky = new System.Windows.Forms.Button();
            this.buttonThemeLovy = new System.Windows.Forms.Button();
            this.buttonThemeSystem = new System.Windows.Forms.Button();
            this.buttonThemeAcideGreen = new System.Windows.Forms.Button();
            this.buttonThemeNivala = new System.Windows.Forms.Button();
            this.buttonThemeGTScroll = new System.Windows.Forms.Button();
            this.buttonThemeWindows = new System.Windows.Forms.Button();
            this.buttonPolice = new System.Windows.Forms.Button();
            this.buttonFermer = new System.Windows.Forms.Button();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.SuspendLayout();
            // 
            // buttonThemeDefault
            // 
            this.buttonThemeDefault.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonThemeDefault.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeDefault.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeDefault.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.buttonThemeDefault.Location = new System.Drawing.Point(12, 12);
            this.buttonThemeDefault.Name = "buttonThemeDefault";
            this.buttonThemeDefault.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeDefault.TabIndex = 0;
            this.buttonThemeDefault.Text = "Default";
            this.buttonThemeDefault.UseVisualStyleBackColor = false;
            this.buttonThemeDefault.Click += new System.EventHandler(this.buttonThemeDefault_Click);
            // 
            // buttonThemeOrit
            // 
            this.buttonThemeOrit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonThemeOrit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeOrit.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeOrit.ForeColor = System.Drawing.SystemColors.ControlLight;
            this.buttonThemeOrit.Location = new System.Drawing.Point(100, 12);
            this.buttonThemeOrit.Name = "buttonThemeOrit";
            this.buttonThemeOrit.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeOrit.TabIndex = 1;
            this.buttonThemeOrit.Text = "Orit";
            this.buttonThemeOrit.UseVisualStyleBackColor = false;
            this.buttonThemeOrit.Click += new System.EventHandler(this.buttonThemeOrit_Click);
            // 
            // buttonThemeGreenCyan
            // 
            this.buttonThemeGreenCyan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonThemeGreenCyan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeGreenCyan.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeGreenCyan.Location = new System.Drawing.Point(188, 12);
            this.buttonThemeGreenCyan.Name = "buttonThemeGreenCyan";
            this.buttonThemeGreenCyan.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeGreenCyan.TabIndex = 2;
            this.buttonThemeGreenCyan.Text = "GreenCyan";
            this.buttonThemeGreenCyan.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Location = new System.Drawing.Point(276, 12);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(82, 81);
            this.button4.TabIndex = 3;
            this.button4.Text = "BintYallow";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // buttonThemeAditoid
            // 
            this.buttonThemeAditoid.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonThemeAditoid.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeAditoid.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeAditoid.Location = new System.Drawing.Point(364, 12);
            this.buttonThemeAditoid.Name = "buttonThemeAditoid";
            this.buttonThemeAditoid.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeAditoid.TabIndex = 4;
            this.buttonThemeAditoid.Text = "Aditoïd";
            this.buttonThemeAditoid.UseVisualStyleBackColor = false;
            // 
            // buttonThemeLidia
            // 
            this.buttonThemeLidia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonThemeLidia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeLidia.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeLidia.Location = new System.Drawing.Point(452, 12);
            this.buttonThemeLidia.Name = "buttonThemeLidia";
            this.buttonThemeLidia.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeLidia.TabIndex = 5;
            this.buttonThemeLidia.Text = "Lidia";
            this.buttonThemeLidia.UseVisualStyleBackColor = false;
            // 
            // buttonThemeRedV
            // 
            this.buttonThemeRedV.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonThemeRedV.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeRedV.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeRedV.Location = new System.Drawing.Point(540, 12);
            this.buttonThemeRedV.Name = "buttonThemeRedV";
            this.buttonThemeRedV.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeRedV.TabIndex = 6;
            this.buttonThemeRedV.Text = "Red V";
            this.buttonThemeRedV.UseVisualStyleBackColor = false;
            // 
            // buttonThemeDangerNum
            // 
            this.buttonThemeDangerNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonThemeDangerNum.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeDangerNum.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeDangerNum.Location = new System.Drawing.Point(628, 12);
            this.buttonThemeDangerNum.Name = "buttonThemeDangerNum";
            this.buttonThemeDangerNum.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeDangerNum.TabIndex = 7;
            this.buttonThemeDangerNum.Text = "Danger Num";
            this.buttonThemeDangerNum.UseVisualStyleBackColor = false;
            // 
            // buttonThemeAxAqua
            // 
            this.buttonThemeAxAqua.BackColor = System.Drawing.Color.Aqua;
            this.buttonThemeAxAqua.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeAxAqua.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeAxAqua.Location = new System.Drawing.Point(12, 99);
            this.buttonThemeAxAqua.Name = "buttonThemeAxAqua";
            this.buttonThemeAxAqua.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeAxAqua.TabIndex = 8;
            this.buttonThemeAxAqua.Text = "Ax Aqua";
            this.buttonThemeAxAqua.UseVisualStyleBackColor = false;
            // 
            // buttonThemeLSingle
            // 
            this.buttonThemeLSingle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.buttonThemeLSingle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeLSingle.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeLSingle.Location = new System.Drawing.Point(100, 99);
            this.buttonThemeLSingle.Name = "buttonThemeLSingle";
            this.buttonThemeLSingle.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeLSingle.TabIndex = 9;
            this.buttonThemeLSingle.Text = "L Single";
            this.buttonThemeLSingle.UseVisualStyleBackColor = false;
            // 
            // buttonThemeLoriate
            // 
            this.buttonThemeLoriate.BackColor = System.Drawing.Color.Magenta;
            this.buttonThemeLoriate.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeLoriate.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeLoriate.Location = new System.Drawing.Point(188, 99);
            this.buttonThemeLoriate.Name = "buttonThemeLoriate";
            this.buttonThemeLoriate.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeLoriate.TabIndex = 10;
            this.buttonThemeLoriate.Text = "Loriate";
            this.buttonThemeLoriate.UseVisualStyleBackColor = false;
            // 
            // buttonThemeBlack
            // 
            this.buttonThemeBlack.BackColor = System.Drawing.Color.Black;
            this.buttonThemeBlack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeBlack.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeBlack.ForeColor = System.Drawing.SystemColors.Info;
            this.buttonThemeBlack.Location = new System.Drawing.Point(276, 99);
            this.buttonThemeBlack.Name = "buttonThemeBlack";
            this.buttonThemeBlack.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeBlack.TabIndex = 11;
            this.buttonThemeBlack.Text = "Black";
            this.buttonThemeBlack.UseVisualStyleBackColor = false;
            // 
            // buttonThemeNatural
            // 
            this.buttonThemeNatural.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonThemeNatural.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeNatural.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeNatural.Location = new System.Drawing.Point(364, 99);
            this.buttonThemeNatural.Name = "buttonThemeNatural";
            this.buttonThemeNatural.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeNatural.TabIndex = 12;
            this.buttonThemeNatural.Text = "Natural";
            this.buttonThemeNatural.UseVisualStyleBackColor = false;
            // 
            // buttonThemePurple
            // 
            this.buttonThemePurple.BackColor = System.Drawing.Color.Purple;
            this.buttonThemePurple.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemePurple.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemePurple.Location = new System.Drawing.Point(452, 99);
            this.buttonThemePurple.Name = "buttonThemePurple";
            this.buttonThemePurple.Size = new System.Drawing.Size(82, 81);
            this.buttonThemePurple.TabIndex = 13;
            this.buttonThemePurple.Text = "Purple";
            this.buttonThemePurple.UseVisualStyleBackColor = false;
            // 
            // buttonThemeCoBoy
            // 
            this.buttonThemeCoBoy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonThemeCoBoy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeCoBoy.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeCoBoy.Location = new System.Drawing.Point(540, 99);
            this.buttonThemeCoBoy.Name = "buttonThemeCoBoy";
            this.buttonThemeCoBoy.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeCoBoy.TabIndex = 14;
            this.buttonThemeCoBoy.Text = "Co Boy";
            this.buttonThemeCoBoy.UseVisualStyleBackColor = false;
            // 
            // buttonThemeAliceBlue
            // 
            this.buttonThemeAliceBlue.BackColor = System.Drawing.Color.AliceBlue;
            this.buttonThemeAliceBlue.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeAliceBlue.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeAliceBlue.Location = new System.Drawing.Point(628, 99);
            this.buttonThemeAliceBlue.Name = "buttonThemeAliceBlue";
            this.buttonThemeAliceBlue.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeAliceBlue.TabIndex = 15;
            this.buttonThemeAliceBlue.Text = "Alice Blue";
            this.buttonThemeAliceBlue.UseVisualStyleBackColor = false;
            // 
            // buttonThemeSilverDark
            // 
            this.buttonThemeSilverDark.BackColor = System.Drawing.Color.Silver;
            this.buttonThemeSilverDark.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeSilverDark.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeSilverDark.Location = new System.Drawing.Point(12, 187);
            this.buttonThemeSilverDark.Name = "buttonThemeSilverDark";
            this.buttonThemeSilverDark.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeSilverDark.TabIndex = 16;
            this.buttonThemeSilverDark.Text = "Silver Dark";
            this.buttonThemeSilverDark.UseVisualStyleBackColor = false;
            // 
            // buttonThemeDarkSky
            // 
            this.buttonThemeDarkSky.BackColor = System.Drawing.Color.Blue;
            this.buttonThemeDarkSky.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeDarkSky.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeDarkSky.Location = new System.Drawing.Point(100, 186);
            this.buttonThemeDarkSky.Name = "buttonThemeDarkSky";
            this.buttonThemeDarkSky.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeDarkSky.TabIndex = 17;
            this.buttonThemeDarkSky.Text = "Dark Sky";
            this.buttonThemeDarkSky.UseVisualStyleBackColor = false;
            // 
            // buttonThemeLovy
            // 
            this.buttonThemeLovy.BackColor = System.Drawing.Color.BlueViolet;
            this.buttonThemeLovy.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeLovy.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeLovy.Location = new System.Drawing.Point(188, 186);
            this.buttonThemeLovy.Name = "buttonThemeLovy";
            this.buttonThemeLovy.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeLovy.TabIndex = 18;
            this.buttonThemeLovy.Text = "Lovy";
            this.buttonThemeLovy.UseVisualStyleBackColor = false;
            // 
            // buttonThemeSystem
            // 
            this.buttonThemeSystem.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.buttonThemeSystem.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeSystem.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeSystem.Location = new System.Drawing.Point(276, 186);
            this.buttonThemeSystem.Name = "buttonThemeSystem";
            this.buttonThemeSystem.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeSystem.TabIndex = 19;
            this.buttonThemeSystem.Text = "System";
            this.buttonThemeSystem.UseVisualStyleBackColor = false;
            // 
            // buttonThemeAcideGreen
            // 
            this.buttonThemeAcideGreen.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonThemeAcideGreen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeAcideGreen.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeAcideGreen.Location = new System.Drawing.Point(364, 186);
            this.buttonThemeAcideGreen.Name = "buttonThemeAcideGreen";
            this.buttonThemeAcideGreen.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeAcideGreen.TabIndex = 20;
            this.buttonThemeAcideGreen.Text = "Acide Green";
            this.buttonThemeAcideGreen.UseVisualStyleBackColor = false;
            // 
            // buttonThemeNivala
            // 
            this.buttonThemeNivala.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.buttonThemeNivala.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeNivala.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeNivala.Location = new System.Drawing.Point(452, 186);
            this.buttonThemeNivala.Name = "buttonThemeNivala";
            this.buttonThemeNivala.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeNivala.TabIndex = 21;
            this.buttonThemeNivala.Text = "Nivala";
            this.buttonThemeNivala.UseVisualStyleBackColor = false;
            // 
            // buttonThemeGTScroll
            // 
            this.buttonThemeGTScroll.BackColor = System.Drawing.Color.MediumSlateBlue;
            this.buttonThemeGTScroll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeGTScroll.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeGTScroll.Location = new System.Drawing.Point(540, 186);
            this.buttonThemeGTScroll.Name = "buttonThemeGTScroll";
            this.buttonThemeGTScroll.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeGTScroll.TabIndex = 22;
            this.buttonThemeGTScroll.Text = "GT Scroll";
            this.buttonThemeGTScroll.UseVisualStyleBackColor = false;
            // 
            // buttonThemeWindows
            // 
            this.buttonThemeWindows.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonThemeWindows.Font = new System.Drawing.Font("Arial Rounded MT Bold", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonThemeWindows.Location = new System.Drawing.Point(628, 186);
            this.buttonThemeWindows.Name = "buttonThemeWindows";
            this.buttonThemeWindows.Size = new System.Drawing.Size(82, 81);
            this.buttonThemeWindows.TabIndex = 23;
            this.buttonThemeWindows.Text = "Windows";
            this.buttonThemeWindows.UseVisualStyleBackColor = true;
            // 
            // buttonPolice
            // 
            this.buttonPolice.BackColor = System.Drawing.Color.Navy;
            this.buttonPolice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPolice.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonPolice.Location = new System.Drawing.Point(12, 274);
            this.buttonPolice.Name = "buttonPolice";
            this.buttonPolice.Size = new System.Drawing.Size(113, 28);
            this.buttonPolice.TabIndex = 24;
            this.buttonPolice.Text = "Police";
            this.buttonPolice.UseVisualStyleBackColor = false;
            // 
            // buttonFermer
            // 
            this.buttonFermer.BackColor = System.Drawing.Color.Navy;
            this.buttonFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFermer.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonFermer.Location = new System.Drawing.Point(634, 276);
            this.buttonFermer.Name = "buttonFermer";
            this.buttonFermer.Size = new System.Drawing.Size(82, 29);
            this.buttonFermer.TabIndex = 23;
            this.buttonFermer.Text = "Fermer";
            this.buttonFermer.UseVisualStyleBackColor = false;
            // 
            // fontDialog
            // 
            this.fontDialog.Color = System.Drawing.SystemColors.ControlText;
            // 
            // AMGTProprieteForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Navy;
            this.ClientSize = new System.Drawing.Size(728, 315);
            this.Controls.Add(this.buttonFermer);
            this.Controls.Add(this.buttonPolice);
            this.Controls.Add(this.buttonThemeWindows);
            this.Controls.Add(this.buttonThemeGTScroll);
            this.Controls.Add(this.buttonThemeNivala);
            this.Controls.Add(this.buttonThemeAcideGreen);
            this.Controls.Add(this.buttonThemeSystem);
            this.Controls.Add(this.buttonThemeLovy);
            this.Controls.Add(this.buttonThemeDarkSky);
            this.Controls.Add(this.buttonThemeSilverDark);
            this.Controls.Add(this.buttonThemeAliceBlue);
            this.Controls.Add(this.buttonThemeCoBoy);
            this.Controls.Add(this.buttonThemePurple);
            this.Controls.Add(this.buttonThemeNatural);
            this.Controls.Add(this.buttonThemeBlack);
            this.Controls.Add(this.buttonThemeLoriate);
            this.Controls.Add(this.buttonThemeLSingle);
            this.Controls.Add(this.buttonThemeAxAqua);
            this.Controls.Add(this.buttonThemeDangerNum);
            this.Controls.Add(this.buttonThemeRedV);
            this.Controls.Add(this.buttonThemeLidia);
            this.Controls.Add(this.buttonThemeAditoid);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.buttonThemeGreenCyan);
            this.Controls.Add(this.buttonThemeOrit);
            this.Controls.Add(this.buttonThemeDefault);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "AMGTProprieteForm";
            this.Opacity = 0.85D;
            this.Text = "AMGT Propriete";
            this.TransparencyKey = System.Drawing.Color.Navy;
            this.Load += new System.EventHandler(this.AMGTProprieteForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonThemeDefault;
        private System.Windows.Forms.Button buttonThemeOrit;
        private System.Windows.Forms.Button buttonThemeGreenCyan;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button buttonThemeAditoid;
        private System.Windows.Forms.Button buttonThemeLidia;
        private System.Windows.Forms.Button buttonThemeRedV;
        private System.Windows.Forms.Button buttonThemeDangerNum;
        private System.Windows.Forms.Button buttonThemeAxAqua;
        private System.Windows.Forms.Button buttonThemeLSingle;
        private System.Windows.Forms.Button buttonThemeLoriate;
        private System.Windows.Forms.Button buttonThemeBlack;
        private System.Windows.Forms.Button buttonThemeNatural;
        private System.Windows.Forms.Button buttonThemePurple;
        private System.Windows.Forms.Button buttonThemeCoBoy;
        private System.Windows.Forms.Button buttonThemeAliceBlue;
        private System.Windows.Forms.Button buttonThemeSilverDark;
        private System.Windows.Forms.Button buttonThemeDarkSky;
        private System.Windows.Forms.Button buttonThemeLovy;
        private System.Windows.Forms.Button buttonThemeSystem;
        private System.Windows.Forms.Button buttonThemeAcideGreen;
        private System.Windows.Forms.Button buttonThemeNivala;
        private System.Windows.Forms.Button buttonThemeGTScroll;
        private System.Windows.Forms.Button buttonThemeWindows;
        private System.Windows.Forms.Button buttonPolice;
        private System.Windows.Forms.Button buttonFermer;
        private System.Windows.Forms.FontDialog fontDialog;

    }
}