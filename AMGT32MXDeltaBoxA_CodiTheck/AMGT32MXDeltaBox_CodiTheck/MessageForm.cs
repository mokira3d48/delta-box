﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public partial class MessageForm : Form
    {
        public MessageForm()
        {
            InitializeComponent();
            OK = false;
            Cancel = false;
        }
        public bool OK { get; set; }
        public bool Cancel { get; set; }

        private void MessageForm_Load(object sender, EventArgs e)
        {
            if (Title.Width <= Message.Width)
            {
                Rectangle.Size = new Size(Message.Width + 20, Message.Height + 20);
                Rectangle2.Size = new Size(Rectangle.Width - 10, Rectangle.Height - 10);
                this.Size = new Size(Rectangle.Width + 40, Rectangle.Height + 40);
            }
            else
            {
                Rectangle.Size = new Size(Title.Width + 20, Message.Height + 20);
                Rectangle2.Size = new Size(Rectangle.Width - 10, Rectangle.Height - 10);
                this.Size = new Size(Rectangle.Width + 50, Rectangle.Height + 50);
            }
        }

        private void Rectangle_Click(object sender, EventArgs e)
        {
            OK = true;
            this.Close();
        }

        private void MessageForm_Click(object sender, EventArgs e)
        {
            Cancel = true;
            this.Close();
        }

        private void Message_Click(object sender, EventArgs e)
        {
            OK = true;
            this.Close();
        }

        private void keys(object sender, KeyEventArgs e)
        {
            KeyEven(e);
        }
        private void KeyEven(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OK = true;
                this.Close();
            }
            if (e.KeyCode == Keys.Escape)
            {
                Cancel = true;
                this.Close();
            }
        }

        private void keys(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                OK = true;
                this.Close();
            }
            if (e.KeyCode == Keys.Escape)
            {
                Cancel = true;
                this.Close();
            }
        }

        private void Title_Click(object sender, EventArgs e)
        {
            OK = true;
            this.Close();
        }

        private void Rectangle2_Click(object sender, EventArgs e)
        {
            Cancel = true;
            this.Close();
        }
    }
}
