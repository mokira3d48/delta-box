﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public partial class OptionAnsForm : Form
    {
        public OptionAnsForm(MXDeltaBoxForm MxDeltaBox)
        {
            InitializeComponent();
            this.DeltaBox = MxDeltaBox;
            labelFraction.Text = trackBarFraction.Value.ToString();
            trackBarFraction.Value = DeltaBox.NSousFormFraction;
            DeltaBox.OptionResultat.Enabled = false; 
        }
        private MXDeltaBoxForm DeltaBox;
        private void OptionAnsForm_Load(object sender, EventArgs e)
        {
            if (DeltaBox.NormalDisplayAns)
            {
                radioButtonFaiblePrecision.Enabled = false;
                radioButtonDoublePrecision.Enabled = false;
                trackBarFraction.Enabled = false;
            }
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            DeltaBox.OptionResultat.Enabled = true;
            this.Close();
        }

        private void OptionNormal_Click(object sender, EventArgs e)
        {
            OptionNormal_Click();
        }
        public void OptionNormal_Click()
        {
            DeltaBox.labelAns.Text = "Ans Normal";
            DeltaBox.NormalDisplayAns = true;
            DeltaBox.FractionRealDisplayAns = false;
            DeltaBox.FractionAngleDisplayAns = false;
            radioButtonFaiblePrecision.Enabled = false;
            radioButtonDoublePrecision.Enabled = false;
            trackBarFraction.Enabled = false;
        }

        private void OptionFrationReal_Click(object sender, EventArgs e)
        {
            OptionFrationReal_Click();
        }
        public void OptionFrationReal_Click()
        {
            DeltaBox.labelAns.Text = "Ans Real Ft";
            DeltaBox.NormalDisplayAns = false;
            DeltaBox.FractionRealDisplayAns = true;
            DeltaBox.FractionAngleDisplayAns = false;
            DeltaBox.FractionDoubleDisplayAns = false;
            DeltaBox.FractionFaibleDisplayAns = true;
            radioButtonFaiblePrecision.Enabled = true;
            radioButtonDoublePrecision.Enabled = true;
            radioButtonFaiblePrecision.Checked = true;
        }

        private void OptionFractionAngle_Click(object sender, EventArgs e)
        {
            OptionFractionAngle_Click();
        }
        public void OptionFractionAngle_Click()
        {
            DeltaBox.labelAns.Text = "Ans Angl Ft";
            DeltaBox.NormalDisplayAns = false;
            DeltaBox.FractionRealDisplayAns = false;
            DeltaBox.FractionAngleDisplayAns = true;
            DeltaBox.FractionDoubleDisplayAns = false;
            DeltaBox.FractionFaibleDisplayAns = true;
            radioButtonFaiblePrecision.Enabled = true;
            radioButtonDoublePrecision.Enabled = true;
            radioButtonFaiblePrecision.Checked = true;
        }

        private void radioButtonFaiblePrecision_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonFaiblePrecision_CheckedChanged();
        }
        public void radioButtonFaiblePrecision_CheckedChanged()
        {
            DeltaBox.FractionDoubleDisplayAns = false;
            DeltaBox.FractionFaibleDisplayAns = true;
            trackBarFraction.Enabled = false;
        }

        private void radioButtonDoublePrecision_CheckedChanged(object sender, EventArgs e)
        {
            radioButtonDoublePrecision_CheckedChanged();
        }
        public void radioButtonDoublePrecision_CheckedChanged()
        {
            if (radioButtonDoublePrecision.Checked)
            {
                DeltaBox.NSousFormFraction = trackBarFraction.Value;
                DeltaBox.FractionDoubleDisplayAns = true;
                DeltaBox.FractionFaibleDisplayAns = false;
                trackBarFraction.Enabled = true;
            }
        }

        private void trackBarFraction_ValueChanged(object sender, EventArgs e)
        {
            DeltaBox.NSousFormFraction = trackBarFraction.Value;
            labelFraction.Text = trackBarFraction.Value.ToString();
        }

        private void OptionAnsForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            DeltaBox.OptionResultat.Enabled = true;
        }
    }
}
