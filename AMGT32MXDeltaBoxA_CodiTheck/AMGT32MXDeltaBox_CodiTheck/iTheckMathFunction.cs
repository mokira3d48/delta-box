﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public class iTheckMathFunction
    {
        public static bool Deg { get; set; }
        public static bool Rad { get; set; }
        public static bool ModeFraction { get; set; }
        public static bool ModeFractionWithPi { get; set; }
        public static bool Error { get; set; }
        public static string MessageError { get; set; }
        public static bool TypeFloat(double x)
        {
            bool ok = false;
            string nbr = Convert.ToString(x);
            for (int i = 0; i < nbr.Length; i++) if ((nbr[i] == ',' || nbr[i] == '.')) ok = true;
            return ok;
        }
        public static string FConvertFraction(double y, int val)
        {
            double b = FExposant(10, val);
            string ans = string.Empty;
            double a = y * b;
            a = Math.Truncate(a);
            if ((a / FPGCD(a, b)) != 1 && (b / FPGCD(a, b)) != 1) ans = Convert.ToString((a / FPGCD(a, b)) + " / " + (b / FPGCD(a, b)));
            else if ((a / FPGCD(a, b)) == 1 && (b / FPGCD(a, b)) != 1) ans = Convert.ToString((a / FPGCD(a, b)) + " / " + (b / FPGCD(a, b)));
            else if ((a / FPGCD(a, b)) != 1 && (b / FPGCD(a, b)) == 1) ans = Convert.ToString((a / FPGCD(a, b)));
            else if ((a / FPGCD(a, b)) == 1 && (b / FPGCD(a, b)) == 1) ans = Convert.ToString((a / FPGCD(a, b)));
            return ans;
        }
        public static string FConvertFractionPI(double y, int val)
        {
            double b = FExposant(10, val);
            string ans = string.Empty;
            double a = (y / Math.PI) * b;
            a = Math.Truncate(a);
            if ((a / FPGCD(a, b)) != 1 && (b / FPGCD(a, b)) != 1) ans = Convert.ToString((a / FPGCD(a, b)) + "π / " + (b / FPGCD(a, b)));
            else if ((a / FPGCD(a, b)) != 1 && (b / FPGCD(a, b)) == 1) ans = Convert.ToString((a / FPGCD(a, b)) + "π");
            else if ((a / FPGCD(a, b)) == 1 && (b / FPGCD(a, b)) != 1) ans = Convert.ToString("π / " + (b / FPGCD(a, b)));
            else if ((a / FPGCD(a, b)) == 1 && (b / FPGCD(a, b)) == 1) ans = Convert.ToString("π");
            return ans;
        }
        public static double FFactorielle(double n)
        {//limite de facto est 170!
            double ans = 1;
            if (TypeFloat(n))
            {
                Error = true;
                MessageError = "Je ne peut pas calculer le factorielle d'un nombre à virgule.";
            }
            if (n > 170)
            {
                Error = true;
                MessageError = "la valeur de " + n + "! dépasse mes limites, C'est pour vous dire que\nla valeur de n est trop grande. Ma limite de valeur est n = 170.";
            }
            else
            {
                if (n < 0)
                {
                    n = -1 * n;
                    ans = -1;
                }
                double i = 1;
                while (i <= n)
                {
                    ans = ans * i;
                    i++;
                }
            }
            return ans;
        }
        ///
        public static double FCombinaison(double n, double k)
        {
            double ans = 0.0;
            if (TypeFloat(n) && !TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer la combinaison de k dans n,\nsi n est un nombre décimal.\nn = " + n + ", n doit être un nombre entier naturel.";
            }
            else if (TypeFloat(k) && !TypeFloat(n))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer la combinaison de k dans n,\nsi k est un nombre décimal.\n k = " + k + ", k doit être un nombre entier naturel.";
            }
            else if (TypeFloat(n) && TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer la combinaison de k dans n,\nsi k et n sont des nombres décimaux.\nn = " + n + " et k = " + k + ", n et k doivent être des nombres entiers naturels.";
            }
            else if (k < 0)
            {
                Error = true;
                MessageError = "le k de n C k est inférieur à 0, car k = " + k + ".\nk doit être suppérieur à 0.";
            }
            else if (k > n)
            {
                Error = true;
                MessageError = "La valeur de k est supérieur à celle de n\ndonc le calcule " + n + " C " + k + " n'est pas possible.\nLa valeur de k doit être inférieur à celle de n.";

            }
            else
            {
                ans = (FFactorielle(n)) / (FFactorielle(k) * FFactorielle(n - k));
                if (n < 0) ans = -1 * ans;
            }
            return ans;
        }
        ///
        public static double FArrangement(double n, double k)
        {
            double ans = 0;
            if (TypeFloat(n) && !TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer l'Arrangement de k dans n,\nsi n est un nombre décimal.\nn = " + n + ", n doit être un nombre entier naturel.";
            }
            else if (TypeFloat(k) && !TypeFloat(n))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer l'Arrangement de k dans n,\nsi k est un nombre décimal.\n k = " + k + ", k doit être un nombre entier naturel.";
            }
            else if (TypeFloat(n) && TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer l'Arrangement de k dans n \nsi k et n sont des nombres décimaux.\nn = " + n + " et k = " + k + ", n et k doivent être des nombres entiers naturels.";
            }
            else if (k < 0)
            {
                Error = true;
                MessageError = "le k de n A k est inférieur à 0, car k = " + k + ".\n k doit être suppérieur à 0.";
            }
            else if (k > n)
            {
                Error = true;
                MessageError = "La valeur de k est supérieur à celle de n\ndonc le calcule " + n + " A " + k + " n'est pas possible.\nLa valeur de k doit être inférieur à celle de n.";

            }
            else
            {
                ans = FFactorielle(Math.Abs(n)) / FFactorielle(Math.Abs(n) - k);
                if (n < 0) ans = -1 * ans;
            }
            return ans;
        }
        ///
        public static long FDiv(double a, double b)
        {
            long ans = 0;
            if (b == 0)
            {
                Error = true;
                MessageError = "Je ne peux pas calculer la division entière d'un nombre par 0.";
            }
            else ans = (long)(a / b);
            return ans;
        }
        ///
        public static long FMODE(double a, double b)
        {
            long ans = 0;
            if ((a < b && a > 0 && b > 0) || a > b && a < 0 && b < 0)
            {
                Error = true;
                MessageError = "Le reste de la division euclidienne de a par b doit être une valeur positive,\nce n'est pas le cas ici car " + a + " inférieur à " + b + ".\na doit être supérieur à b.";
            }
            else if (b == 0)
            {
                Error = true;
                MessageError = "Impossible pour moi d'éffectuer ce calcule car b est null dans cette division euclidienne.\nb doit être différent de 0 pour que a / b existe,\nce n'est pas le cas ici car. " + a + " / 0 = ∞";
            }
            else if (a < 0 && Math.Abs(a) < b && b > 0)
            {
                Error = true;
                MessageError = "Impossible pour moi d'éffectuer ce calcule car il y a embiguité  dans l'oppération posée.\nLa valeur de a est d'abord inférieur à 0, ce qui me laisse savoir que a et b sont de signes différentes\nDe plus la valeur absolue de a est toujour inférieur à la valeur de b donc je ne peux pas éffectuer ce calcule\nétant donnée q'un reste doit toujour être suppérieur ou égale à 0\na = " + a + " et b = " + b + ".";
            }
            else ans = (long)(a - (b * FDiv(a, b)));
            return ans;
        }
        ///
        public static double FPGCD(double a, double b)
        {
            double x = a, y = b, ans = 0.0;
            if (TypeFloat(a) && !TypeFloat(b))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PGCD d'un nombre décimal par un nombre entier,\n car a = " + a + ". a doit être un nombre entier. ";
            }
            else if (TypeFloat(b) && !TypeFloat(a))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PGCD d'un nombre entier par un nombre décimal,\n car b = " + b + ". b doit être un nombre entier. ";
            }
            else if (TypeFloat(a) && TypeFloat(b))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PGCD de deux nombres décimaux,\ncar a = " + a + " et b = " + b + ".\na et b doivent être des nombres entiers. ";
            }
            else if (a == 0 && b == 0)
            {
                Error = true;
                MessageError = " a et b ne peuvent pas être null, car ici a = " + a + " et b = " + b + ".\nIl faut que l'un d'entre eux soit différent de 0.";
            }
            else
            {
                if (x < 0) x = Math.Abs(x);
                if (y < 0) y = Math.Abs(y);
                if (y == 0 && x != 0)
                {
                    y = x;
                    x = 0;
                }
                if (x < y && x != 0)
                {
                    double x1 = x;
                    x = y;
                    y = x1;
                }
                long r = 1;
                while (r != 0)
                {
                    r = FMODE(x, y);
                    x = y;
                    y = r;
                }
                ans = x;
            }
            return ans;
        }
        ///
        public static double FPPCM(double a, double b)
        {
            double ans = 0.0;
            if (TypeFloat(a) && !TypeFloat(b))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PPCM d'un nombre décimal par un nombre entier,\n car a = " + a + ". a doit être un nombre entier. ";
            }
            else if (TypeFloat(b) && !TypeFloat(a))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PPCM d'un nombre entier par un nombre décimal,\n car b = " + b + ". b doit être un nombre entier. ";
            }
            else if (TypeFloat(a) && TypeFloat(b))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le PPCM de deux nombres décimaux,\ncar a = " + a + " et b = " + b + ".\na et b doivent être des nombres entiers. ";
            }
            else if (a == 0)
            {
                Error = true;
                MessageError = "a = " + a + ", a ne peux pas être égal à 0 car lorsqu'on se sert de la relation suivante:\nPGCD(a;b) x PPCM(a;b) = a x b, pour calculer le PPCM(a;b) on a:\nPPCM(" + a + ";" + b + ") = " + a + " x " + b + " / PGCD(" + a + ";" + b + ")\nOr PGCD(" + a + ";" + b + ") = 0 => PPCM(" + a + ";" + b + ") = 0 / 0\nJe ne sais pas faire 0 / 0. Voila pourquoi a doit être différent de 0";
            }
            else ans = a * b / FPGCD(a, b);
            return ans;
        }
        ///
        public static double FPUplet(double n, double k)
        {
            double ans = 0.0;
            if (TypeFloat(n) && !TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le p-uplet d'un nombre décimal par un nombre entier,\ncar n = " + n + ". n doit être un nombre entier.";
            }
            else if (TypeFloat(n) && !TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le p-uplet d'un nombre entier par un nombre décimal,\ncar k = " + k + ". k doit être un nombre entier.";
            }
            else if (TypeFloat(n) && TypeFloat(k))
            {
                Error = true;
                MessageError = "Je ne peux pas calculer le p-uplet de deux nombres décimaux,\ncar n = " + n + " et k = " + k + ".\nn et k doivent être des nombres entiers.";
            }
            else if (k < 0)
            {
                Error = true;
                MessageError = "le k de n P k est inférieur à 0, car k = " + k + ".\nk doit être suppérieur à 0.";
            }
            else if (n < 0)
            {
                Error = true;
                MessageError = "l' entier n de n P k est inférieur à 0, car n = " + n + ".\nJe ne peux pas faire ce calcule. n doit être suppérieur à 0.";
            }
            else ans = Math.Pow(n, k);
            return ans;
        }
        ///
        public static double FConvertDegToRad(double alpha)
        {
            double ans = (alpha * Math.PI) / 180;
            return ans;
        }
        ///
        public static double FLn(double x)
        {
            double ans = 0.0;
            if (x < 0)
            {
                Error = true;
                MessageError = "Cette opération n'est pas possible car " + x + " est strictement négatif\ndonc x n'est pas dans le domaine de définition de la fonction ln (]0 ; +∞[)";
            }
            else if (x == 0)
            {
                Error = true;
                MessageError = "Le résultat de cette opération n'existe pas dans lR.\nCar le résultat est -∞, limite quand x tand vers 0,\nest -∞";
            }
            else ans = Math.Log(x);
            return ans;
        }
        ///
        public static double FLog(double x)
        {
            double ans = 0.0;
            if (x < 0)
            {
                Error = true;
                MessageError = "Cette opération n'est pas possible car " + x + " est négatif\ndonc x n'est pas dans le domaine de définition de la fonction log (]0 ; +∞[)";
            }
            else if (x == 0)
            {
                Error = true;
                MessageError = "Le résultat de cette opération n'existe pas dans lR.\nCar le résultat est -∞, limite quand x tand vers 0,\nest -∞";
            }
            else ans = Math.Log10(x);
            return ans;
        }
        ///
        public static double FSqrt(double x)
        {
            double ans = 0.0;
            if (x < 0)
            {
                Error = true;
                MessageError = "Cette opération n'est pas possible car " + x + " est négatif\ndonc x n'est pas dans le domaine de définition de la fonction racine carrée ([0 ; +∞[)\n Autrement dit √" + x + " n'existe pas dans lR";
            }
            else ans = Math.Sqrt(x);
            return ans;
        }
        ///
        public static double FExposant(double y, double x)
        {
            double ans = 0.0;
            if (x == 0 && y == 0)
            {
                Error = true;
                MessageError = "0 ^ 0 est une indétermination, autrement dit: impossible de calculer 0 ^ 0.\nx ou y doit être différent de 0.";
            }
            else if (y < 0 && TypeFloat(x / 2)) ans = -1 * Math.Pow(Math.Abs(y), x);
            else if (y < 0 && !TypeFloat(x / 2)) ans = Math.Pow(Math.Abs(y), x);
            else if (y > 0) ans = Math.Pow(y, x);
            return ans;
        }
        ///
        public static double FConvertAngle(double x, char ch)
        {
            double ans = 0.0;
            if (ch == 'd')
            {
                ans = (x * 180) / Math.PI;
            }
            else if (ch == 'r')
            {
                ans = (x * Math.PI) / 180;
            }
            return ans;
        }
        ///
        public static double FSin(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (TypeFloat(x / Math.PI)) ans = Math.Sin(x);
                else ans = 0.0;
            }
            else if (Deg)
            {
                if (TypeFloat(x / 180)) ans = Math.Sin(FConvertDegToRad(x));
                else ans = 0.0;
            }
            return ans;
        }
        ///
        public static double FCos(double x)
        {
            MessageError = string.Empty;
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (TypeFloat((x - (Math.PI / 2)) / Math.PI)) ans = Math.Cos(x);
                else ans = 0.0;
            }
            else if (Deg)
            {
                if (TypeFloat((x - 90) / 180)) ans = Math.Cos(FConvertDegToRad(x));
                else ans = 0.0;
            }
            return ans;
        }
        ///
        public static double FTan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (!TypeFloat((x - (Math.PI / 2)) / Math.PI))
                {
                    Error = true;
                    MessageError = "Impossible de calculer tan(" + x + ") car " + x + " = (π / 2) + kπ\navec k = " + ((x - (Math.PI / 2)) / Math.PI) + " ∈ Z. Or la fonction tangente est définie sur lR \\ {(π / 2) + kπ, k ∈ Z}.\nD'où x ∉ lR \\ {" + x + "}.";
                }
                else if (!TypeFloat((x / Math.PI))) ans = 0;
                else ans = Math.Tan(x);
            }
            else if (Deg)
            {
                if (!TypeFloat((x - 90) / 180))
                {
                    Error = true;
                    MessageError = "Impossible de calculer tan(" + x + ") car " + x + " = 90 + 180k\navec k = " + ((x - 90) / 180) + " ∈ Z. Or la fonction tangente est définie sur lR \\ { 90 + 180k, k ∈ Z}.\nD'où x ∉ lR \\ {" + x + "}.";
                }
                else if (!TypeFloat((x / 180))) ans = 0;
                else ans = Math.Tan(FConvertDegToRad(x));
            }
            return ans;
        }
        ///
        public static double FCotan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {//
                Deg = false;
                if (!TypeFloat((x / Math.PI)))
                {
                    Error = true;
                    MessageError = "Impossible de calculer cotan(" + x + ") car " + x + " = kπ\navec k = " + (x / Math.PI) + " ∈ Z. Or la fonction cotangente est définie sur lR \\ {kπ, k ∈ Z}.\nD'où x ∉ lR \\ {" + x + "}.";
                }
                else if (!TypeFloat((x - (Math.PI / 2)) / Math.PI)) ans = 0;
                else ans = Math.Cos(x) / Math.Sin(x);
            }
            else if (Deg)
            {//
                if (!TypeFloat((x / 180)))
                {
                    Error = true;
                    MessageError = "Impossible de calculer cotan(" + x + ") car " + x + " = 180k\navec k = " + (x / 180) + " ∈ Z. Or la fonction cotangente est définie sur lR \\ {180k, k ∈ Z}.\nD'où x ∉ lR \\ {" + x + "}.";
                }
                else if (!TypeFloat((x - 90) / 180)) ans = 0;
                else ans = Math.Cos(FConvertDegToRad(x)) / Math.Sin(FConvertDegToRad(x));
            }
            return ans;
        }
        ///
        public static double FArcsin(double x)
        {
            double ans = 0.0;
            if (x < -1 || x > 1)
            {
                Error = true;
                MessageError = "Impossible de calculer arcsin(" + x + "),\ncar " + x + " n'appartient pas à l'ensemble de définition\nde la fonction arcsin:\n(]-1 ; 1[).";
            }
            else if (x == -1)
            {
                Error = true;
                MessageError = "Arcsin(-1) n'existe pas dans lR car le résultat est +∞.";
            }
            else if (x == 1)
            {
                Error = true;
                MessageError = "Arcsin(1) n'existe pas dans lR car le résultat -∞.";
            }
            else
            {
                if (Rad)
                {
                    Deg = false;
                    ans = Math.Asin(x);
                }
                else if (Deg) ans = FConvertAngle(Math.Asin(x), 'd');
            }
            return ans;
        }
        ///
        public static double FArccos(double x)
        {
            double ans = 0.0;
            if (x < -1 || x > 1)
            {
                Error = true;
                MessageError = "Impossible de calculer arcsin(" + x + "),\ncar " + x + " n'appartient pas à l'ensemble de définition\nde la fonction arccos:\n(]-1 ; 1[)";
            }
            else if (x == -1)
            {
                Error = true;
                MessageError = "Arccos(-1) n'existe pas dans lR car le résultat est -∞";
            }
            else if (x == 1)
            {
                Error = true;
                MessageError = "Arccos(1) n'existe pas dans lR car le résultat +∞";
            }
            else
            {
                if (Rad)
                {
                    Deg = false;
                    ans = Math.Acos(x);
                }
                else if (Deg) ans = FConvertAngle(Math.Acos(x), 'd');
            }
            return ans;
        }
        ///
        public static double FArctan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                ans = Math.Atan(x);
            }
            else if (Deg) ans = FConvertAngle(Math.Atan(x), 'd');
            return ans;
        }
        ///
        public static double FArccotan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (x != 0) ans = FArctan(1 / x);
                else ans = Math.PI / 2;
            }
            else if (Deg)
            {
                if (x != 0) ans = FArctan(1 / x);
                else ans = 90;
            }
            return ans;
        }
        ///
        public static double FSign(double x)
        {
            double ans = 0;
            if (x < 0) ans = -1;
            else if (x > 0) ans = 1;
            return ans;
        }
        ///
        public static double FSh(double x)
        {
            double ans = Math.Sinh(x);
            return ans;
        }
        ///
        public static double FCh(double x)
        {
            double ans = Math.Cosh(x);
            return ans;
        }
        ///
        public static double FTh(double x)
        {
            double ans = Math.Tanh(x);
            return ans;
        }
        ///
        public static double FCoth(double x)
        {
            double ans = 0.0;
            if (x == 0)
            {
                Error = true;
                MessageError = "Impossible de calculer coth(0) car 0 ∉ ]-∞ ; 0[U]0 ; +∞[";
            }
            else ans = FCh(x) / FSh(x);
            return ans;
        }
        ///
        public static double FArgsh(double x)
        {
            double ans = FLn(x + FSqrt(FExposant(x, 2) + 1));
            return ans;
        }
        ///
        public static double FArgch(double x)
        {
            double ans = 0.0;
            if (x < 1)
            {
                Error = true;
                MessageError = "Cette opération n'est pas possible car " + x + " n'est pas dans le domaine de définition\nde la fonction Argch ([1 ; +∞[). Sachant que sachant la fonction Argch est définie par:\n     Argch :[1 ; +∞[ ⟶ lR\n      x ⟼ Argch(x) = ln(x - √(x²-1))\nLorsqu'on essai de faire le calcule suivant :\nArgch(" + x + ") = Ln( " + x + " - √(" + x + "² - 1))\nle résultat de Argch(" + x + ") n'existe pas dans lR qui est l'ensemble d'arrivé de la fonction Argch";
            }
            else ans = FLn(x - FSqrt(FExposant(x, 2) - 1));
            return ans;
        }
        ///
        public static double FArgth(double x)
        {
            double ans = 0.0;
            if (x <= -1 || x >= 1)
            {
                Error = true;
                MessageError = "Cette opération n'est pas possible car " + x + " n'est pas dans le domaine de définition\nde la fonction Argth (]-1 ; 1[). Sachant que sachant la fonction Argch est définie par:\n     Argth :]-1 ; 1[---> lR\n    x ⟼ Argth(x) = (1 / 2)ln((1 + x)/(1 - x))\n Lorsqu'on essai de faire le calcule suivant :\nArgth(" + x + ") = (1 / 2)Ln((1 + " + x + ")/(1 - " + x + "))\nle résultat de Argth(" + x + ") n'existe pas dans lR qui est l'ensemble d'arrivé de la fonction Argth";
            }
            else ans = (1 / 2) * FLn((1 + x) / (1 - x));
            return ans;
        }
        ///Dérivée de fonction
        public static double FdLn(double x)
        {
            double ans = 0.0;
            if (x == 0)
            {
                Error = true;
                MessageError = "Je ne peux pas faire 1 / 0.\nDe plus 0 n'apartient pas au domaine de dérivabilité de la fonction Logarithme népérien.";
            }
            else ans = 1 / x;
            return ans;
        }
        ///
        public static double FdLog(double x)
        {
            double ans = 0.0;
            if (x == 0)
            {
                Error = true;
                MessageError = "Je ne peux pas faire 1 / 0.\nDe plus 0 n'apartient pas au domaine de dérivabilité de la fonction Logarithme décimal.\nDonc x doit être différent de 0.";
            }
            else ans = 1 / (x * FLn(10));
            return ans;
        }
        ///
        public static double FdSqrt(double x)
        {
            double ans = 0.0;
            if (x < 0)
            {
                Error = true;
                MessageError = "Je ne peux par faire la racine carrée d'un nombre négatif\nx = " + x + " n'appartient pas à l'ensemble de définition de la fonction dérivée\nde la fonction racine carrée (]0 ; +∞[).";
            }
            else if (x == 0)
            {
                Error = true;
                MessageError = "Je ne peux pas faire 1 / 0. Car en remplacant x dans la formule dans la dérivée\n de racine carrée on a: 1 / 2√" + x + ".\nCe qui nous donne 1 / 0, je ne peux donc pas calculer 1 / 0. Par conséquent x doit être différent de 0.";
            }
            else ans = 1 / (2 * FSqrt(x));
            return ans;
        }
        ///
        public static double FdSin(double x)
        {
            double ans = FCos(x);
            return ans;
        }
        ///
        public static double FdCos(double x)
        {
            double ans = -1 * FSin(x);
            return ans;
        }
        ///
        public static double FdTan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (!TypeFloat((x - (Math.PI / 2) / Math.PI)))
                {
                    Error = true;
                    MessageError = "Je ne peux pas calculer la dérivée de la tan au point d'abscise x = " + x + ",\ncar " + x + " = (π / 2) + kπ  avec k = " + ((x - (Math.PI / 2)) / Math.PI) + ". Or la fonction tan est dérivable en tous point de lR \\ {(π / 2) + kπ, k ∈ Z}.\nDonc la dérivée de tan au point d'abscise x = " + x + " est ∞. Cela n'existe pas dans lR.";
                }
                else ans = 1 / FExposant(FCos(x), 2);
            }
            else if (Deg)
            {
                if (!TypeFloat((x - 90) / 180))
                {
                    Error = true;
                    MessageError = "Je ne peux pas calculer la dérivée de la tan au point d'abscise x = " + x + ",\ncar " + x + " = 90 + 180k  avec k = " + ((x - 90) / 180) + ". Or la fonction tan est dérivable en tous point de lR \\ {90 + 180k, k ∈ Z}.\nDonc la dérivée de tan au point d'abscise x = " + x + " est ∞. Cela n'existe pas dans lR.";
                }
                else ans = 1 / FExposant(FCos(x), 2);
            }
            return ans;
        }
        ///
        public static double FdCotan(double x)
        {
            double ans = 0.0;
            if (Rad)
            {
                Deg = false;
                if (!TypeFloat(x / Math.PI))
                {
                    Error = true;
                    MessageError = "Je ne peux pas calculer la dérivée de cotan au point d'abscise x = " + x + ",\ncar " + x + " = kπ  avec k = " + (x / Math.PI) + ". Or la fonction cotan est dérivable en tous point de lR \\ {kπ, k ∈ Z}.\nDonc la dérivée de cotan au point d'abscise x = " + x + " est ∞. Cela n'existe pas dans lR.";
                }
                else ans = -1 / FExposant(FSin(x), 2);
            }
            else if (Deg)
            {
                if (!TypeFloat(x / 180))
                {
                    Error = true;
                    MessageError = "Je ne peux pas calculer la dérivée de cotan au point d'abscise x = " + x + ",\ncar " + x + " = 180k  avec k = " + (x / 180) + ". Or la fonction cotan est dérivable en tous point de lR \\ {180k, k ∈ Z}.\nDonc la dérivée de cotan au point d'abscise x = " + x + " est ∞. Cela n'existe pas dans lR.";
                }
                else ans = -1 / FExposant(FSin(x), 2);
            }
            return ans;
        }
        ///
        public static double FdArcsin(double x)
        {
            double ans = 0.0;
            if (x < -1 || x > 1)
            {
                Error = true;
                MessageError = "Impossible de calculer arcsin'(" + x + ")\ncar " + x + " n'appartient pas à l'ensemble de dérivabilité\nde la fonction arcsin (]-1 ; 1[).";
            }
            else if (x == -1)
            {
                Error = true;
                MessageError = "Le résultat de arcsin'(" + x + ") est +∞ et +∞ ∉ lR (]-∞ ; +∞[).";
            }
            else if (x == 1)
            {
                Error = true;
                MessageError = "Le résultat de arcsin'(" + x + ") est +∞ et +∞ ∉ lR (]-∞ ; +∞[).";
            }
            else ans = 1 / (FSqrt(1 - FExposant(x, 2)));
            return ans;
        }
        ///
        public static double FdArccos(double x)
        {
            double ans = 0.0;
            if (x < -1 || x > 1)
            {
                Error = true;
                MessageError = "Impossible de calculer arccos'(" + x + ")\ncar " + x + " n'appartient pas à l'ensemble de dérivabilité\nde la fonction arccos (]-1 ; 1[).";
            }
            else if (x == -1)
            {
                Error = true;
                MessageError = "Le résultat de arccos'(" + x + ") est -∞ et -∞ ∉ lR (]-∞ ; +∞[).";
            }
            else if (x == 1)
            {
                Error = true;
                MessageError = "Le résultat de arccos'(" + x + ") est -∞ et -∞ ∉ lR (]-∞ ; +∞[).";
            }
            else ans = -1 / (FSqrt(1 - FExposant(x, 2)));
            return ans;
        }
        ///
        public static double FdArctan(double x)
        {
            double ans = 1 / (1 + FExposant(x, 2));
            return ans;
        }
        ///
        public static double FdArccotan(double x)
        {
            double ans = -1 / (1 + FExposant(x, 2));
            return ans;
        }
        ///
        public static double FdSh(double x)
        {
            double ans = FCh(x);
            return ans;
        }
        ///
        public static double FdCh(double x)
        {
            double ans = FSh(x);
            return ans;
        }
        ///
        public static double FdTh(double x)
        {
            double ans = 1 / FExposant(FCh(x), 2);
            return ans;
        }
        ///
        public static double FdCoth(double x)
        {
            double ans = 0.0;
            if (x == 0)
            {
                Error = true;
                MessageError = "Non! Non! La dériée de la fonction cotangente hyperbolique\nn'est pas n'est définie en 0.\nAutrement dit, coth n'est pas dérivable en 0.\nDons x doit être différent de 0.";
            }
            else ans = -1 / FExposant(FSh(x), 2);
            return ans;
        }
        ///
        public static double FdArgsh(double x)
        {
            double ans = 1 / FSqrt(1 + FExposant(x, 2));
            return ans;
        }
        ///
        public static double FdArgch(double x)
        {
            double ans = 0.0;
            if (x >= -1 && x <= 1)
            {
                Error = true;
                MessageError = "Non! non! impossible de calculer la dérivée de argch en " + x + " car " + x + " n'appartient pas\nau domaine de dérivabilité de la fonction argch (]-∞ ; -1[U]1 ; +∞[).";
            }
            else ans = 1 / FSqrt(FExposant(x, 2) - 1);
            return ans;
        }
        ///
        public static double FdArgth(double x)
        {
            double ans = 0.0;
            if (x == -1 || x == 1)
            {
                Error = true;
                MessageError = "Non! non! impossible de calculer la dérivée de argth en " + x + " car " + x + " n'appartient pas\nau domaine de dérivabilité de la fonction argth (lR \\ {-1,1}).";
            }
            else ans = 1 / (1 - FExposant(x, 2));
            return ans;
        }
    }
}
