﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public class iTheckMathCalculator
    {
        private string[] g1 = new string[500];
        private string[] g = new string[500];
        //public bool Rad { get; set; }
        public bool Deg { get; set; }
        private string Operation;
        private string[] f1 = new string[500];
        private string[] f = new string[500];
        private double[] t = new double[250];
        private char[] s = new char[500];
        private char[] p = new char[500];
        private char[] locked = new char[500];
        private double[] t1 = new double[500];
        private char[] s1 = new char[500];
        private char[] p1 = new char[500];
        private char[] locked1 = new char[500];
        private int n = 0;
        private char a = (char)0;
        private double ans = 0.0;
        public bool AnsFractionDoublePrecision { get; set; }
        public bool AnsFractionFaiblePrecision { get; set; }
        public bool AnsFractionPI { get; set; }
        public bool AnsFraction { get; set; }
        public int AnsFractionNPrecision { get; set; }
        public bool AnsNormal { get; set; }

        public iTheckMathCalculator(string data)
        {
            Operation = TrimSpace(data);
            Deg = false;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string TrimSpace(string str)
        {
            string ans = string.Empty;
            for (int i = 0; i < str.Length; i++) if (str[i] != (char)32) ans += str[i];
            return ans;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Calculate()
        {
            string num = string.Empty;
            int i, k = -1, y = -1;
            string data = Operation;
            if (data.Length == 0) goto fin;
            i = data.Length;
            for (int d = 0; d < f1.Length; d++) f1[d] = string.Empty;
            for (int d = 0; d < g1.Length; d++) g1[d] = string.Empty;
            try
            {
                while (i > 0)
                {
                    i -= 1;
                    Console.WriteLine("i= " + i + "\n");
                    if (data[i] >= '0' && data[i] <= '9') num = data[i] + num;
                    if (data[i] == ',' || data[i] == '.') num = "," + num;
                    if (data[i] == '+' && num != string.Empty)
                    {
                        if (num != string.Empty)
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;

                            if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x') s1[k] = data[i - 1];
                            else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²') s1[k] = '+';
                            else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E') g1[k] = "" + data[i - 1];
                            else if (data[i - 1] >= 'a' && data[i - 1] <= 'z' && data[i - 1] != 'x') continue;
                            else if (data[i - 1] == '(' || data[i - 1] == '[') continue;

                            else break;
                        }
                    }
                    else if (data[i] == '-')
                    {
                        if (num != string.Empty)
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;
                            if (i > 0)
                            {

                                if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                                {
                                    s1[k] = 'x';
                                    k++;
                                    t1[k] = -1;
                                    s1[k] = data[i - 1];
                                }
                                else if (((data[i - 1] >= 'a' && data[i - 1] <= 'z') && data[i - 1] != 'x') || data[i - 1] == '√')
                                {
                                    t1[k] = -1 * t1[k];
                                }
                                else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E')
                                {
                                    t1[k] = -1 * t1[k];
                                    g1[k] = Convert.ToString(data[i - 1]);
                                }
                                else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == '!' || data[i - 1] == 'e' || data[i - 1] == 'π' || data[i - 1] == '²')
                                {
                                    s1[k] = 'x';
                                    k++;
                                    t1[k] = -1;
                                    s1[k] = '+';
                                }
                                else if (data[i - 1] == '(' || data[i - 1] == '[')
                                {
                                    s1[k] = 'x';
                                    k++;
                                    t1[k] = -1;

                                    continue;
                                }
                                else if ((data[i - 1] >= 'a' && data[i - 1] <= 'z' && data[i - 1] != 'x') || data[i - 1] == '\'')
                                {
                                    t1[k] = -1 * t1[k];
                                    continue;
                                }
                            }
                            else
                            {
                                s1[k] = 'x';
                                k++;
                                t1[k] = -1;
                                break;
                            }

                        }
                    }
                    else if (data[i] == '(' || data[i] == '[')
                    {
                        if (num != string.Empty)
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;
                        }
                        k++;
                        p1[k] = data[i];

                        locked1[k] = '$';
                        if (i > 0)
                        {
                            if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                            {
                                s1[k] = data[i - 1];
                                t1[k] = 1;
                            }
                            else if (data[i - 1] == '-')
                            {
                                if (i > 1)
                                {
                                    if (data[i - 2] == '/' || data[i - 2] == '*' || data[i - 2] == 'x' || data[i - 2] == '+')
                                    {
                                        s1[k] = 'x';
                                        k++;
                                        t1[k] = -1;
                                        s1[k] = data[i - 2];
                                    }
                                    else if (data[i - 2] == '(' || data[i - 2] == '[')
                                    {
                                        s1[k] = 'x';
                                        k++;
                                        t1[k] = -1;
                                    }
                                    else if (data[i - 2] == ')' || data[i - 2] == ']' || data[i - 2] == '!' || data[i - 2] == 'e' || data[i - 2] == 'π' || (data[i - 2] >= '0' && data[i - 2] <= '9' || data[i - 2] == '²'))
                                    {
                                        s1[k] = 'x';
                                        k++;
                                        t1[k] = -1;
                                        s1[k] = '+';
                                    }
                                    else if (data[i - 2] == 'C' || data[i - 2] == 'A' || data[i - 2] == 'P' || data[i - 2] == '^' || data[i - 2] == 'E')
                                    {
                                        t1[k] = -1;
                                        g1[k] = "" + data[i - 2];
                                    }

                                }
                                else
                                {
                                    s1[k] = 'x';
                                    k++;
                                    t1[k] = -1;
                                }
                            }
                            else if ((data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == 'π' || data[i - 1] == '!' || data[i - 1] == '²' || data[i - 1] == ')' || data[i - 1] == ']')
                            {
                                s1[k] = 'x';
                            }
                            else if (data[i - 1] == '(' || data[i - 1] == '[') continue;
                            else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E') g1[k] = "" + data[i - 1];
                        }
                        else break;
                    }
                    else if ((data[i] >= 'a' && data[i] <= 'z' && data[i] != 'x') || data[i] == '\'' || data[i] == 'π' || data[i] == '√' || data[i] == 'µ') // fonction
                    {
                        if (num != string.Empty)
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;
                        }
                        if (data[i] == 'e') { k++; t1[k] = Math.E; goto a; }
                        if (data[i] == 'π') { k++; t1[k] = Math.PI; goto a; }
                        if (data[i] == '√') { k++; f1[k] = "√"; goto a; }
                        if (data[i] == 'µ') { k++; f1[k] = "µ"; goto a; }
                        else { g1[k] = string.Empty; k++; f1[k] = string.Empty; }

                        if (i > 0)
                        {
                            if (data.Substring(i - 1, 2) == "ln") { f1[k] = "ln"; }
                            if (data.Substring(i - 1, 2) == "sh") { f1[k] = "sh"; }
                            if (data.Substring(i - 1, 2) == "ch") { f1[k] = "ch"; }
                            if (data.Substring(i - 1, 2) == "th") { f1[k] = "th"; }
                        }
                        if (i > 1)
                        {
                            if (data.Substring(i - 2, 3) == "sin") { f1[k] = "sin"; }
                            if (data.Substring(i - 2, 3) == "cos") { f1[k] = "cos"; }
                            if (data.Substring(i - 2, 3) == "tan") { f1[k] = "tan"; }
                            if (data.Substring(i - 2, 3) == "log") { f1[k] = "log"; }
                            if (data.Substring(i - 2, 3) == "ln'") { f1[k] = "ln'"; }
                            if (data.Substring(i - 2, 3) == "ch'") { f1[k] = "ch'"; }
                            if (data.Substring(i - 2, 3) == "sh'") { f1[k] = "sh'"; }
                            if (data.Substring(i - 2, 3) == "th'") { f1[k] = "th'"; }
                            if (data.Substring(i - 2, 3) == "deg") { f1[k] = "deg"; }
                            if (data.Substring(i - 2, 3) == "rad") { f1[k] = "rad"; }
                            if (data.Substring(i - 2, 3) == "div") { k--; g1[k] = "div"; }
                        }
                        if (i > 2)
                        {
                            if (data.Substring(i - 3, 4) == "sqrt") { f1[k] = "sqrt"; }
                            if (data.Substring(i - 3, 4) == "coth") { f1[k] = "coth"; }
                            if (data.Substring(i - 3, 4) == "sign") { f1[k] = "sign"; }
                            if (data.Substring(i - 3, 4) == "sin'") { f1[k] = "sin'"; }
                            if (data.Substring(i - 3, 4) == "cos'") { f1[k] = "cos'"; }
                            if (data.Substring(i - 3, 4) == "tan'") { f1[k] = "tan'"; }
                            if (data.Substring(i - 3, 4) == "log'") { f1[k] = "log'"; }
                            if (data.Substring(i - 3, 4) == "pgcd") { k--; g1[k] = "pgcd"; }
                            if (data.Substring(i - 3, 4) == "ppcm") { k--; g1[k] = "ppcm"; }
                            if (data.Substring(i - 3, 4) == "rest") { k--; g1[k] = "rest"; }
                        }
                        if (i > 3)
                        {
                            if (data.Substring(i - 4, 5) == "argsh") { f1[k] = "argsh"; }
                            if (data.Substring(i - 4, 5) == "argch") { f1[k] = "argch"; }
                            if (data.Substring(i - 4, 5) == "argth") { f1[k] = "argth"; }
                            if (data.Substring(i - 4, 5) == "cotan") { f1[k] = "cotan"; }
                            if (data.Substring(i - 4, 5) == "sqrt'") { f1[k] = "sqrt'"; }
                            if (data.Substring(i - 4, 5) == "coth'") { f1[k] = "coth'"; }
                        }
                        if (i > 4)
                        {
                            if (data.Substring(i - 5, 6) == "arcsin") { f1[k] = "arcsin"; }
                            if (data.Substring(i - 5, 6) == "arccos") { f1[k] = "arccos"; }
                            if (data.Substring(i - 5, 6) == "arctan") { f1[k] = "arctan"; }
                            if (data.Substring(i - 5, 6) == "argsh'") { f1[k] = "argsh'"; }
                            if (data.Substring(i - 5, 6) == "argch'") { f1[k] = "argch'"; }
                            if (data.Substring(i - 5, 6) == "argth'") { f1[k] = "argth'"; }
                            if (data.Substring(i - 5, 6) == "cotan'") { f1[k] = "cotan'"; }
                        }
                        if (i > 5)
                        {
                            if (data.Substring(i - 6, 7) == "arccotg") { f1[k] = "arccotg"; }
                            if (data.Substring(i - 6, 7) == "arcsin'") { f1[k] = "arcsin'"; }
                            if (data.Substring(i - 6, 7) == "arccos'") { f1[k] = "arccos'"; }
                            if (data.Substring(i - 6, 7) == "arctan'") { f1[k] = "arctan'"; }
                        }
                        if (i > 6)
                        {
                            if (data.Substring(i - 7, 8) == "arccotg'") { f1[k] = "arccotg'"; }
                        }
                        if (f1[k].Length != 0)
                        {
                            try
                            {
                                i -= f1[k].Length - 1;
                            }
                            catch { }
                            locked1[k] = '$';
                        }
                        if (g1[k].Length != 0)
                        {
                            try
                            {
                                i -= g1[k].Length - 1;
                            }
                            catch { }
                            continue;
                            // locked1[k] = '$';
                        }
                    a:
                        if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) locked1[k] = '$';
                        if (i > 0)
                        {
                            if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                            {
                                s1[k] = data[i - 1];
                                if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                            }
                            else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E')
                            {
                                g1[k] = "" + data[i - 1];
                                if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                            }
                            else if (data[i - 1] == '-')
                            {
                                if (i > 1)
                                {
                                    if (data[i - 2] == '/' || data[i - 2] == '*' || data[i - 2] == 'x' || data[i - 2] == '+')
                                    {
                                        if (t1[k] != Math.Exp(1) && t1[k] != Math.PI)
                                        {
                                            t1[k] = -1;
                                            s1[k] = data[i - 2];
                                        }
                                        else
                                        {
                                            t1[k] = -1 * t1[k];
                                            s1[k] = data[i - 2];
                                        }
                                    }
                                    else if (data[i - 2] == ')' || data[i - 2] == ']' || data[i - 2] == '!' || data[i - 2] == 'e' || data[i - 2] == 'π' || (data[i - 2] >= '0' && data[i - 2] <= '9' || data[i - 2] == '²'))
                                    {
                                        s1[k] = 'x';
                                        k++;
                                        t1[k] = -1;
                                        s1[k] = '+';
                                    }
                                    else if (data[i - 2] == 'C' || data[i - 2] == 'A' || data[i - 2] == 'P' || data[i - 2] == '^' || data[i - 2] == 'E')
                                    {
                                        g1[k] = "" + data[i - 2];
                                        if (t1[k] != Math.Exp(1) && t1[k] != Math.PI)
                                        {
                                            t1[k] = -1;
                                        }
                                        else
                                        {
                                            t1[k] = -1 * t1[k];
                                        }
                                    }
                                    else if (data[i - 2] == '(' || data[i - 2] == '[')
                                    {
                                        s1[k] = 'x';
                                        k++;
                                        t1[k] = -1;
                                        continue;
                                    }

                                }
                                else
                                {
                                    s1[k] = 'x';
                                    k++;
                                    t1[k] = -1;
                                }
                            }
                            else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²')
                            {
                                if (t1[k] == Math.PI) s1[k] = 'm';
                                else s1[k] = 'x';
                                if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                            }
                            else if (data[i - 1] == '(' || data[i - 1] == '[')
                            {
                                if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                                continue;
                            }
                            else if ((data[i - 1] >= 'a' && data[i - 1] <= 'z') && data[i - 1] != 'x') if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                        }
                        else
                        {
                            if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                        }
                    }
                    else if (data[i] == ')' || data[i] == ']')
                    {
                        k++;
                        p1[k] = data[i];
                        locked1[k] = '$';
                    }
                    else if (i == 0 && data[i] != '+' && data[i] != '-' && data[i] != '(' && data[i] != '[')
                    {
                        if (num != string.Empty)
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;
                        }
                    }
                    else if (data[i] == '^')
                    {
                        if (i > 0)
                        {
                            if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                            {
                                k++;
                                t1[k] = double.Parse(num);
                                num = string.Empty;
                                g1[k] = "" + data[i];
                            }
                        }
                    }
                    else if (data[i] == 'C' || data[i] == 'A' || data[i] == 'P' || data[i] == 'E')
                    {
                        if (i > 0)
                        {
                            if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                            {
                                k++;
                                t1[k] = double.Parse(num);
                                num = string.Empty;
                                g1[k] = "" + data[i];
                            }
                        }
                    }
                    else if (data[i] == '*' || data[i] == 'x')
                    {
                        if (i > 0)
                        {
                            if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                            {
                                k++;
                                t1[k] = double.Parse(num);
                                num = string.Empty;
                                s1[k] = data[i];
                            }
                        }
                    }
                    else if (data[i] == '/')
                    {
                        if (i > 0)
                        {
                            if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                            {
                                k++;
                                t1[k] = double.Parse(num);
                                num = string.Empty;
                                s1[k] = '/';
                            }
                        }
                    }
                    else if (data[i] == '!' || data[i] == '²')
                    {
                        if (num == string.Empty)
                        {
                            k++;
                            f1[k] = "" + data[i];
                            locked1[k] = '$';
                        }
                        else
                        {
                            k++;
                            t1[k] = double.Parse(num);
                            num = string.Empty;
                            s1[k] = 'x';
                            k++;
                            locked1[k] = '$';
                            f1[k] = "" + data[i];
                        }
                    }
                }
            }
            catch { }
            for (i = 0; i < f.Length; i++) f[i] = string.Empty;
            for (i = 0; i < g.Length; i++) g[i] = string.Empty;
            n = k;
            y = -1;
            i = n;
            while (i >= 0)
            {
                y++;
                t[y] = t1[i];
                s[y] = s1[i];
                p[y] = p1[i];
                locked[y] = locked1[i];
                f[y] = f1[i];
                g[y] = g1[i];
                i--;
            }
            ans = 0.0;
            Console.WriteLine("Classement correct");
            Affiche();
            ParentheseCalculator();
            Console.WriteLine("parti 1");
            Affiche();
            FactorielleCalculator(0, n);
            FunctionIntegerCalculator(0, n);
            Console.WriteLine("parti 2");
            Affiche();
            MultiplicationDivisionCalculator0(0, n);
            FunctionRealCalculator(0, n);
            MultiplicationDivisionCalculator(0, n);
            Console.WriteLine("partie 3");
            Affiche();
            AdditionCalculator(0, n);
            ans = FinalAnswer();
        fin:
            string SAns = string.Empty;
            if (AnsNormal) SAns = Convert.ToString(ans);
            else if (AnsFraction && AnsFractionFaiblePrecision) SAns = iTheckMathFunction.FConvertFraction(ans, 2);
            else if (AnsFraction && AnsFractionDoublePrecision) SAns = iTheckMathFunction.FConvertFraction(ans, AnsFractionNPrecision);
            else if (AnsFractionPI && AnsFractionFaiblePrecision) SAns = iTheckMathFunction.FConvertFractionPI(ans, 2);
            else if (AnsFractionPI && AnsFractionDoublePrecision) SAns = iTheckMathFunction.FConvertFractionPI(ans, AnsFractionNPrecision);
            return SAns;
        }
        /// <summary>
        /// 
        /// </summary>
        /// 
        private void FactorielleCalculator(int val1, int val2)
        {
            int i, y;
            for (i = val1; i <= val2; i++)
            {
                if (f[i] == "!" || f[i] == "²")
                {
                    y = i - 1;
                    while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > 0) y--;
                    if (p[y] != ')' && p[y] != ']')
                    {
                        if (f[i] == "!") t[y] = iTheckMathFunction.FFactorielle(t[y]);
                        if (f[i] == "²") t[y] = iTheckMathFunction.FExposant(t[y], 2);
                        f[i] = string.Empty;
                        locked[i] = '$';
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void FunctionIntegerCalculator(int val1, int val2)
        {
            int i, y;
            for (i = val1; i <= val2; i++)
            {
                if ((g[i] == "^" || g[i] == "ppcm" || g[i] == "pgcd" || g[i] == "rest" || g[i] == "C" || g[i] == "A" || g[i] == "P" || g[i] == "div" || g[i] == "E") && locked[i] != '$')
                {
                    if (g[i] == "^")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FExposant(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "pgcd")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FPGCD(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "ppcm")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FPPCM(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "P")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FPUplet(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "A")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FArrangement(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "C")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FCombinaison(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "div")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FDiv(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "rest")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = iTheckMathFunction.FMODE(t[y], t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                    else if (g[i] == "E")
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && p[y] != ']' && y > val1) y--;
                        if (p[y] != ')' && p[y] != ']')
                        {
                            t[y] = t[y] * iTheckMathFunction.FExposant(10, t[i]);
                            t[i] = 0.0;
                            g[i] = string.Empty;
                            locked[i] = '$';
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void FunctionRealCalculator(int val1, int val2)
        {
            int i;
            for (i = val2; i >= val1; i--)
            {
                if ((f[i] == "sign" || f[i] == "ln" || f[i] == "ln'" || f[i] == "sqrt" || f[i] == "sqrt'" || f[i] == "arcsin" || f[i] == "arcsin'" || f[i] == "arccos" || f[i] == "arccos'" || f[i] == "deg" || f[i] == "rad" || f[i] == "arctan" || f[i] == "arctan'" || f[i] == "arccotg" || f[i] == "arccotg'" || f[i] == "argsh" || f[i] == "argsh'" || f[i] == "argch" || f[i] == "argch'" || f[i] == "argth" || f[i] == "argth'" || f[i] == "√" || f[i] == "µ" || f[i] == "sin" || f[i] == "sin'" || f[i] == "cos" || f[i] == "cos'" || f[i] == "tan" || f[i] == "tan'" || f[i] == "cotan" || f[i] == "cotan'" || f[i] == "sh" || f[i] == "sh'" || f[i] == "ch" || f[i] == "ch'" || f[i] == "th" || f[i] == "th'" || f[i] == "coth" || f[i] == "coth'" || f[i] == "log" || f[i] == "log'") && locked[i + 1] != '$')
                {
                    if (f[i] == "sign")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FSign(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FSign(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i + 1] = '$';
                        locked[i] = a;
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "ln")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FLn(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FLn(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i + 1] = '$';
                        locked[i] = a;
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "ln'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdLn(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdLn(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i + 1] = '$';
                        locked[i] = a;
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sin")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FSin(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FSin(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sin'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdSin(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdSin(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "cos")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FCos(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FCos(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "cos'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdCos(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdCos(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "tan")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FTan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FTan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "tan'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdTan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdTan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "cotan")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FCotan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FCotan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "cotan'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdCotan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdCotan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sh")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FSh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FSh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sh'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdSh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdSh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "ch")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FCh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FCh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "ch'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdCh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdCh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "th")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FTh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FTh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "th'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdTh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdTh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sqrt")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FSqrt(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FSqrt(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "sqrt'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdSqrt(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdSqrt(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "log")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FLog(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FLog(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "log'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdLog(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdLog(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "√" || f[i] == "µ")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FSqrt(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FSqrt(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argsh")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArgsh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArgsh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argsh'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArgsh(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArgsh(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argch")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArgch(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArgch(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argch'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArgch(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArgch(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argth")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArgth(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArgth(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "argth'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArgth(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArgth(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arcsin")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArcsin(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArcsin(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arcsin'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArcsin(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArcsin(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arccos")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArccos(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArccos(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arccos'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArccos(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArccos(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arctan")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArctan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArctan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arctan'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArctan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArctan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arccotg")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FArccotan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FArccotan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "arccotg'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdArccotan(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdArccotan(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "coth")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FCoth(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FCoth(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "coth'")
                    {
                        if (t[i] != -1) t[i] = iTheckMathFunction.FdCoth(t[i + 1]);
                        else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FdCoth(t[i + 1]);
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                    else if (f[i] == "deg" || f[i] == "rad")
                    {
                        if (f[i] == "deg")
                        {
                            if (t[i] != -1) t[i] = iTheckMathFunction.FConvertAngle(t[i + 1], 'd');
                            else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FConvertAngle(t[i + 1], 'd');
                        }
                        else if (f[i] == "rad")
                        {
                            if (t[i] != -1) t[i] = iTheckMathFunction.FConvertAngle(t[i + 1], 'r');
                            else if (t[i] == -1) t[i] = -1 * iTheckMathFunction.FConvertAngle(t[i + 1], 'r');
                        }
                        f[i] = string.Empty;
                        locked[i] = a;
                        locked[i + 1] = '$';
                        t[i + 1] = 0.0;
                    }
                }
            }
            int h = val1;
            int h1 = val2;
            FunctionIntegerCalculator(h, h1);
        }
        /// <summary>
        /// 
        /// </summary>
        private void MultiplicationDivisionCalculator(int val1, int val2)
        {
            int i, y;
            for (i = val2; i >= val1; i--)
            {
                if ((s[i] == 'x' || s[i] == '/' || s[i] == '*') && locked[i] != '$')
                {
                    if (s[i] == 'x' || s[i] == '*')
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && y > val1) y--;
                        Console.Write(y);
                        if (p[y] != ')')
                        {
                            t[y] = t[y] * t[i];
                            t[i] = 0.0;
                            s[i] = a;
                            locked[i] = '$';
                        }
                    }
                    else if (s[i] == '/')
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && y > val1) y--;
                        if (p[y] != ')')
                        {
                            t[y] = t[y] / t[i];
                            t[i] = 0.0;
                            s[i] = a;
                            locked[i] = '$';
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        private void MultiplicationDivisionCalculator0(int val1, int val2)
        {
            int i, y;
            for (i = val2; i >= val1; i--)
            {
                if ((s[i] == 'm') && locked[i] != '$')
                {
                    if (s[i] == 'm')
                    {
                        y = i - 1;
                        while (locked[y] == '$' && p[y] != ')' && y > val1) y--;
                        Console.Write(y);
                        if (p[y] != ')')
                        {
                            t[y] = t[y] * t[i];
                            t[i] = 0.0;
                            s[i] = a;
                            locked[i] = '$';
                        }
                    }
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// 
        private void ParentheseCalculator()
        {
            double an;
            int i, k;
            for (i = n; i >= 0; i--)
            {
                if (p[i] == '(' || p[i] == '[')
                {
                    k = i + 1;
                    locked[i] = a;
                    while (p[k] != ')' && p[k] != ']' && k < n) k++;
                    an = t[i];
                    t[i] = 0.0;
                    FactorielleCalculator(i + 1, k);
                    FunctionIntegerCalculator(i + 1, k);
                    MultiplicationDivisionCalculator0(i + 1, k);
                    FunctionRealCalculator(i + 1, k);
                    MultiplicationDivisionCalculator(i + 1, k);
                    AdditionCalculator(i, k);
                    p[i] = a;   //effacer la parenthèse
                    if (p[k] == ']' && t[i] < 0) t[i] = -1 * t[i];
                    p[k] = a;
                    if (an < 0)
                    {
                        t[i] = -1 * t[i];
                    }
                }
            }
        }
        private void AdditionCalculator(int val1, int val2)
        {
            int k, i;
            k = val1;
            while (t[k] == 0 && k < val2) k++;
            t[val1] = t[k];
            if (val1 != k)
            {
                t[k] = 0.0;
                locked[k] = '$';
            }
            for (i = k + 1; i <= val2; i++)
            {
                if (s[i] == '+')
                {
                    t[val1] = t[val1] + t[i];
                    t[i] = 0.0;
                    locked[i] = '$';
                    s[i] = a;
                }
            }
            Console.WriteLine(t[val1]);
        }
        private double FinalAnswer()
        {
            int k;
            k = 0;
            while (t[k] == 0 && k < n) k++;
            Console.WriteLine(k + " " + t[k]);
            return t[k];
        }
        /// <summary>
        /// 
        /// </summary>
        public void Affiche()
        {
            for (int i = 0; i <= n; i++)
            {
                Console.WriteLine();
                Console.WriteLine("t(" + i + ")= " + t[i]);
                Console.WriteLine("s(" + i + ")= " + s[i]);
                Console.WriteLine("p(" + i + ")= " + p[i]);
                Console.WriteLine("locked(" + (i) + ")= " + locked[i]);
                Console.WriteLine("f(" + i + ")= " + f[i]);
                Console.WriteLine("g(" + i + ")= " + g[i]);
            }
        }
    }
}
