﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public class ClassPassageOperation
    {
        public bool ErrorSyntaxe { get; set; }
        public string MessageError { get; set; }
        private List<string> Mxg = new List<string>();
        private string Operation;
        public ClassPassageOperation(string Str)
        {
            Operation = TrimSpace(Str);
        }
        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="data"></param>
        public string TrimSpace(string str)
        {
            string ans = string.Empty;
            for (int i = 0; i < str.Length; i++) if (str[i] != (char)32) ans += str[i];
            return ans;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        private void FErrorParenthese(string data)
        {
            int ouvert = 0, fermer = 0;
            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == '(') ouvert++;
                if (data[i] == ')') fermer++;
            }
            if (ouvert < fermer)
            {
                ErrorSyntaxe = true;
                if ((fermer - ouvert) == 1) Mxg.Add("Il y a une parenthèse fermée qui n'a jamais été ouverte");
                else if ((fermer - ouvert) > 1) Mxg.Add("Il y a " + (fermer - ouvert) + " parenthèses fermées qui n'ont jamain été ouvertes");
            }
            else if (ouvert > fermer)
            {
                ErrorSyntaxe = true;
                if ((ouvert - fermer) == 1) Mxg.Add("Il y a une parenthèse ouverte qui n'est pas été fermée");
                else if ((ouvert - fermer) > 1) Mxg.Add("Il y a " + (ouvert - fermer) + " parenthèses ouvertes qui n'ont pas été fermées");
            }
        }
        ///
        private void FErrorSigne(string data)
        {
            for (int i = 0; i < data.Length; i++)
            {
                if (i < data.Length - 1)
                {
                    if (data[i] == '+' && data[i + 1] == '+')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"++\"\nPour moi on dirait une incrémentation en programmation");
                    }
                    if (data[i] == '+' && (data[i + 1] == '*' || data[i + 1] == 'x'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"+x\".\nVous avez peut être oublier de préciser quelque chose entre + et x.");
                    }
                    if (data[i] == '+' && data[i + 1] == '/')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"+/\".\nVous avez peut être oublier de préciser quelque chose entre les opérateurs + et /.");
                    }
                    if (data[i] == '+' && data[i + 1] == ')')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"+)\".\nCela ne me signifie rien donc arrangez ça. Vous avez peut être oublier de préciser quelque chose.");
                    }
                    if (data[i] == '-' && (data[i + 1] == '*' || data[i + 1] == 'x'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-x\".\nVous avez peut être oublier de préciser quelque chose entre - et x \nDeplus je tiens à vous rappeler qu'il ne s'agit pas de la résolution d'une équation.");
                    }
                    if (data[i] == '-' && data[i + 1] == '+')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-+\".\nIl faut revoir votre Machin!.");
                    }
                    if (data[i] == '-' && data[i + 1] == '/')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-/\".\nVous avez peut être oublier de préciser quelque chose entre - et /.");
                    }
                    if (data[i] == '-' && data[i + 1] == '!')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-!\".\nVous avez peut être oublier de préciser quelque chose entre - et !.");
                    }
                    if (data[i] == '-' && data[i + 1] == '²')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-²\".\nVous avez peut être oublier de préciser quelque chose entre - et ².\nSinon ça n'a pas de sens.");
                    }
                    if (data[i] == '-' && data[i + 1] == ')')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"-)\".\nVous avez peut être oublier de préciser quelque chose.");
                    }
                    if (data[i] == '+' && data[i + 1] == '!')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"+!\".\nVous avez peut être oublier de préciser quelque chose entre + et !.");
                    }
                    if (data[i] == '+' && data[i + 1] == '²')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"+²\".\nVous avez peut être oublier de préciser quelque chose entre + et ².\n l'opérateur \"+\" n'est pas quantifiable, autrement dit \"+\" n'est pas un nombre.");
                    }
                    if ((data[i] == 'x' || data[i] == '*') && data[i + 1] == '/')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"x/\".\nVous avez peut être oublier de préciser quelque chose entre x et /.\nCes deux signe ne peuvent pas se suivre");
                    }
                    if ((data[i] == 'x' || data[i] == '*') && data[i + 1] == '²')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"x²\".\nVous avez peut être oublier de préciser quelque chose entre x et ²");
                    }
                    if ((data[i] == 'x' || data[i] == '*') && data[i + 1] == ')')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"x)\".La parenthèse fermée n'est un nombre.\nVous avez peut être oublier de préciser quelque chose entre x et )");
                    }
                    if (data[i] == '-' && data[i + 1] == '-')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"--\".\nOn dirait une décrémentation en programmation. Je n'est aucune option pour \"--\".\nDonc je vous suggère de mettre une seul fois ce signe.");
                    }
                    if ((data[i] == 'x' || data[i] == '*') && (data[i + 1] == 'x' || data[i + 1] == '*'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"xx\".\nVous avez peut être oublier de préciser quelque chose entre ces deux simbole identiques.");
                    }
                    if ((data[i] == 'x' || data[i] == '*') && data[i + 1] == '!')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"x!\".\nMoi je lit factorielle de x. Donc si je comprend, Vous prennez \"x\" pour une variale.\nOr ici \"x\" est un multiplicateur.\nVous devez donc corriger cela.");
                    }
                    if (data[i] == '/' && (data[i + 1] == '*' || data[i + 1] == 'x'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"/x\".\nVous avez peut être oublier de préciser quelque chose entre / et x");
                    }
                    if (data[i] == '/' && data[i + 1] == '/')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"//\".On dirait un commentaire en programmation\nVous avez peut être oublier de préciser quelque chose entre ces deux signes identiques.");
                    }
                    if (data[i] == '/' && data[i + 1] == ')')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous avez mal posé votre opération, je ne comprend pas le signe \"/)\".\nVous avez peut être oublier de préciser quelque chose entre / et )");
                    }
                    if (data[i] == '(' && (data[i + 1] == '*' || data[i + 1] == 'x'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("J'attend un nombre ou... entre \"(\" et \"x\" avant de continuer le salle boulo");
                    }
                    if (data[i] == '(' && data[i + 1] == '/')
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("J'attend un nombre entre ( et / avant de continuer");
                    }
                    if (data[i] == ')' && (data[i + 1] >= '0' && data[i + 1] <= '9'))
                    {
                        ErrorSyntaxe = true;
                        Mxg.Add("Vous ne pouvez pas mettre un nombre après une parenthèse \")" + data[i + 1] + "\"");
                    }

                }
                if (data.Length == 1 && (data[i] == '+' || data[i] == '-' || data[i] == '*' || data[i] == 'x' || data[i] == '/' || data[i] == ')' || data[i] == '(' || data[i] == '!' || data[i] == '²' || data[i] == 'E' || data[i] == '√' || data[i] == '^'))
                {
                    ErrorSyntaxe = true;
                    Mxg.Add("Je n'appelle pas sa opération une Opération");
                }
                if (i == data.Length - 1 && (data[i] == '+' || data[i] == '-' || data[i] == '*' || data[i] == 'x' || data[i] == '/' || data[i] == '^' || data[i] == '(' || data[i] == 'E' || data[i] == '√'))
                {
                    ErrorSyntaxe = true;
                    Mxg.Add("Je suppose que vous n'aviez pas fini de poser votre opération\nCar je n'est encore rien compris.");
                }
            }
        }
        ///
        private void FunctionCorrect(string data)
        {
            int i = -1;
            while (i < data.Length - 1)
            {
                i++;
                if (data[i] >= 'A' && data[i] <= 'Z')
                {
                    if (data[i] == 'A') { continue; }
                    if (data[i] == 'C') { continue; }
                    if (data[i] == 'P') { continue; }
                    if (data[i] == 'E') { continue; }
                }
                else if ((data[i] >= 'a' && data[i] <= 'z') && data[i] != 'x')
                {
                    if (data[i] == 'e') { continue; }
                    if (i < data.Length - 1)
                    {
                        if (data.Substring(i, 2) == "ln") { i += 1; continue; }
                        if (data.Substring(i, 2) == "sh") { i += 1; continue; }
                        if (data.Substring(i, 2) == "ch") { i += 1; continue; }
                        if (data.Substring(i, 2) == "th") { i += 1; continue; }
                    }
                    if (i < data.Length - 2)
                    {
                        if (data.Substring(i, 3) == "sin") { i += 2; continue; }
                        if (data.Substring(i, 3) == "cos") { i += 2; continue; }
                        if (data.Substring(i, 3) == "tan") { i += 2; continue; }
                        if (data.Substring(i, 3) == "log") { i += 2; continue; }
                        if (data.Substring(i, 3) == "ln'") { i += 2; continue; }
                        if (data.Substring(i, 3) == "ch'") { i += 2; continue; }
                        if (data.Substring(i, 3) == "sh'") { i += 2; continue; }
                        if (data.Substring(i, 3) == "th'") { i += 2; continue; }
                        if (data.Substring(i, 3) == "deg") { i += 2; continue; }
                        if (data.Substring(i, 3) == "rad") { i += 2; continue; }
                        if (data.Substring(i, 3) == "div") { i += 2; continue; }
                    }
                    if (i < data.Length - 3)
                    {
                        if (data.Substring(i, 4) == "sqrt") { i += 3; continue; }
                        if (data.Substring(i, 4) == "coth") { i += 3; continue; }
                        if (data.Substring(i, 4) == "sign") { i += 3; continue; }
                        if (data.Substring(i, 4) == "sin'") { i += 3; continue; }
                        if (data.Substring(i, 4) == "cos'") { i += 3; continue; }
                        if (data.Substring(i, 4) == "tan'") { i += 3; continue; }
                        if (data.Substring(i, 4) == "log'") { i += 3; continue; }
                        if (data.Substring(i, 4) == "pgcd") { i += 3; continue; }
                        if (data.Substring(i, 4) == "ppcm") { i += 3; continue; }
                        if (data.Substring(i, 4) == "rest") { i += 3; continue; }
                    }
                    if (i < data.Length - 4)
                    {
                        if (data.Substring(i, 5) == "argsh") { i += 4; continue; }
                        if (data.Substring(i, 5) == "argch") { i += 4; continue; }
                        if (data.Substring(i, 5) == "argth") { i += 4; continue; }
                        if (data.Substring(i, 5) == "cotan") { i += 4; continue; }
                        if (data.Substring(i, 5) == "sqrt'") { i += 4; continue; }
                        if (data.Substring(i, 5) == "coth'") { i += 4; continue; }
                    }
                    if (i < data.Length - 5)
                    {
                        if (data.Substring(i, 6) == "arcsin") { i += 5; continue; }
                        if (data.Substring(i, 6) == "arccos") { i += 5; continue; }
                        if (data.Substring(i, 6) == "arctan") { i += 5; continue; }
                        if (data.Substring(i, 6) == "argsh'") { i += 5; continue; }
                        if (data.Substring(i, 6) == "argch'") { i += 5; continue; }
                        if (data.Substring(i, 6) == "argth'") { i += 5; continue; }
                        if (data.Substring(i, 6) == "cotan'") { i += 5; continue; }
                    }
                    if (i < data.Length - 6)
                    {
                        if (data.Substring(i, 7) == "arccotg") { i += 6; continue; }
                        if (data.Substring(i, 7) == "arcsin'") { i += 6; continue; }
                        if (data.Substring(i, 7) == "arccos'") { i += 6; continue; }
                        if (data.Substring(i, 7) == "arctan'") { i += 6; continue; }
                    }
                    if (i < data.Length - 7)
                    {
                        if (data.Substring(i, 8) == "arccotg'") { i += 7; continue; }
                    }
                    Mxg.Add("Il y a une fonction dans votre opération que je ne reconnait pas.\nDonc essayez de revoir l'opération que vous m'avez poser.");
                    continue;
                }
                else if (data[i] >= '0' && data[i] <= '9') continue;
                else if (data[i] != '/' && data[i] != '\'' && data[i] != 'x' && data[i] != '*' && data[i] != '-' && data[i] != '+' && data[i] != '√' && data[i] != 'π' && data[i] != '!' && data[i] != '(' && data[i] != ')' && data[i] != '[' && data[i] != ']' && data[i] != '^' && data[i] != '²' && data[i] != '.' && data[i] != ',') Mxg.Add("Ooooh je ne connait pas encore le symbole mathématique\nque vous avez utilisé dans votre l'opération.\nDonc essayez de voir. Il s'agit de ça: " + data[i] + "");
            }
        }
        ///
        private void OperationVerify(string data)
        {
            /* string num = string.Empty;
             string[] g1 = new string[500];
             double[] t1 = new double[500];
             char[] s1 = new char[500];
             char[] p1 = new char[500];
             char[] locked1 = new char[500];
             string[] f1 = new string[500];
             int i, k = -1;
             i = data.Length;
             for (int d = 0; d < f1.Length; d++) f1[d] = string.Empty;
             for (int d = 0; d < g1.Length; d++) g1[d] = string.Empty;
             while (i > 0)
             {
                 i -= 1;
                 Console.WriteLine("i= " + i + "\n");
                 if (data[i] >= '0' && data[i] <= '9') num = data[i] + num;
                 if (data[i] == ',' || data[i] == '.') num = ',' + num;
                 if (data[i] == '+' && num != string.Empty)
                 {
                     if (num != string.Empty)
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;

                         if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x') s1[k] = data[i - 1];
                         else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²') s1[k] = '+';
                         else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E') g1[k] = "" + data[i - 1];
                         else if (data[i - 1] >= 'a' && data[i - 1] <= 'z' && data[i - 1] != 'x') continue;
                         else if (data[i - 1] == '(' || data[i - 1] == '[') continue;

                         else break;
                     }
                 }
                 else if (data[i] == '-')
                 {
                     if (num != string.Empty)
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;
                         if (i > 0)
                         {

                             if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                             {
                                 s1[k] = 'x';
                                 k++;
                                 t1[k] = -1;
                                 s1[k] = data[i - 1];
                             }
                             else if (((data[i - 1] >= 'a' && data[i - 1] <= 'z') && data[i - 1] != 'x') || data[i - 1] == '√')
                             {
                                 t1[k] = -1 * t1[k];
                             }
                             else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E')
                             {
                                 t1[k] = -1 * t1[k];
                                 g1[k] = Convert.ToString(data[i - 1]);
                             }
                             else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == '!' || data[i - 1] == 'e' || data[i - 1] == 'π' || data[i - 1] == '²')
                             {
                                 s1[k] = 'x';
                                 k++;
                                 t1[k] = -1;
                                 s1[k] = '+';
                             }
                             else if (data[i - 1] == '(' || data[i - 1] == '[')
                             {
                                 s1[k] = 'x';
                                 k++;
                                 t1[k] = -1;

                                 continue;
                             }
                             else if (data[i - 1] >= 'a' && data[i - 1] <= 'z' && data[i - 1] != 'x')
                             {
                                 t1[k] = -1 * t1[k];
                                 continue;
                             }
                         }
                         else
                         {
                             s1[k] = 'x';
                             k++;
                             t1[k] = -1;
                             break;
                         }

                     }
                 }
                 else if (data[i] == '(' || data[i] == '[')
                 {
                     if (num != string.Empty)
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;
                     }
                     k++;
                     p1[k] = data[i];

                     locked1[k] = '$';
                     if (i > 0)
                     {
                         if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                         {
                             s1[k] = data[i - 1];
                             t1[k] = 1;
                         }
                         else if (data[i - 1] == '-')
                         {
                             if (i > 1)
                             {
                                 if (data[i - 2] == '/' || data[i - 2] == '*' || data[i - 2] == 'x' || data[i - 2] == '+')
                                 {
                                     s1[k] = 'x';
                                     k++;
                                     t1[k] = -1;
                                     s1[k] = data[i - 2];
                                 }
                                 else if (data[i - 2] == '(' || data[i - 2] == '[')
                                 {
                                     s1[k] = 'x';
                                     k++;
                                     t1[k] = -1;
                                 }
                                 else if (data[i - 2] == ')' || data[i - 2] == ']' || data[i - 2] == '!' || data[i - 2] == 'e' || data[i - 2] == 'π' || (data[i - 2] >= '0' && data[i - 2] <= '9' || data[i - 2] == '²'))
                                 {
                                     s1[k] = 'x';
                                     k++;
                                     t1[k] = -1;
                                     s1[k] = '+';
                                 }
                                 else if (data[i - 2] == 'C' || data[i - 2] == 'A' || data[i - 2] == 'P' || data[i - 2] == '^' || data[i - 2] == 'E')
                                 {
                                     t1[k] = -1;
                                     g1[k] = "" + data[i - 2];
                                 }

                             }
                             else
                             {
                                 s1[k] = 'x';
                                 k++;
                                 t1[k] = -1;
                             }
                         }
                         else if ((data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == 'π' || data[i - 1] == '!' || data[i - 1] == '²' || data[i - 1] == ')' || data[i - 1] == ']')
                         {
                             s1[k] = 'x';
                         }
                         else if (data[i - 1] == '(' || data[i - 1] == '[') continue;
                         else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E') g1[k] = "" + data[i - 1];
                     }
                     else break;
                 }
                 else if ((data[i] >= 'a' && data[i] <= 'z' && data[i] != 'x') || data[i] == '\'' || data[i] == 'π' || data[i] == '√' || data[i] == 'µ') // fonction
                 {
                     if (num != string.Empty)
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;
                     }
                     if (data[i] == 'e') { k++; t1[k] = Math.E; goto a; }
                     if (data[i] == 'π') { k++; t1[k] = Math.PI; goto a; }
                     if (data[i] == '√') { k++; f1[k] = "√"; goto a; }
                     if (data[i] == 'µ') { k++; f1[k] = "µ"; goto a; }
                     else { g1[k] = string.Empty; k++; f1[k] = string.Empty; }

                     if (i > 0)
                     {
                         if (data.Substring(i - 1, 2) == "ln") { f1[k] = "ln"; }
                         if (data.Substring(i - 1, 2) == "sh") { f1[k] = "sh"; }
                         if (data.Substring(i - 1, 2) == "ch") { f1[k] = "ch"; }
                         if (data.Substring(i - 1, 2) == "th") { f1[k] = "th"; }
                     }
                     if (i > 1)
                     {
                         if (data.Substring(i - 2, 3) == "sin") { f1[k] = "sin"; }
                         if (data.Substring(i - 2, 3) == "cos") { f1[k] = "cos"; }
                         if (data.Substring(i - 2, 3) == "tan") { f1[k] = "tan"; }
                         if (data.Substring(i - 2, 3) == "log") { f1[k] = "log"; }
                         if (data.Substring(i - 2, 3) == "ln'") { f1[k] = "ln'"; }
                         if (data.Substring(i - 2, 3) == "ch'") { f1[k] = "ch'"; }
                         if (data.Substring(i - 2, 3) == "sh'") { f1[k] = "sh'"; }
                         if (data.Substring(i - 2, 3) == "th'") { f1[k] = "th'"; }
                         if (data.Substring(i - 2, 3) == "deg") { f1[k] = "deg"; }
                         if (data.Substring(i - 2, 3) == "rad") { f1[k] = "rad"; }
                         if (data.Substring(i - 2, 3) == "div") { k--; g1[k] = "div"; }
                     }
                     if (i > 2)
                     {
                         if (data.Substring(i - 3, 4) == "sqrt") { f1[k] = "sqrt"; }
                         if (data.Substring(i - 3, 4) == "coth") { f1[k] = "coth"; }
                         if (data.Substring(i - 3, 4) == "sign") { f1[k] = "sign"; }
                         if (data.Substring(i - 3, 4) == "sin'") { f1[k] = "sin'"; }
                         if (data.Substring(i - 3, 4) == "cos'") { f1[k] = "cos'"; }
                         if (data.Substring(i - 3, 4) == "tan'") { f1[k] = "tan'"; }
                         if (data.Substring(i - 3, 4) == "log'") { f1[k] = "log'"; }
                         if (data.Substring(i - 3, 4) == "pgcd") { k--; g1[k] = "pgcd"; }
                         if (data.Substring(i - 3, 4) == "ppcm") { k--; g1[k] = "ppcm"; }
                         if (data.Substring(i - 3, 4) == "rest") { k--; g1[k] = "rest"; }
                     }
                     if (i > 3)
                     {
                         if (data.Substring(i - 4, 5) == "argsh") { f1[k] = "argsh"; }
                         if (data.Substring(i - 4, 5) == "argch") { f1[k] = "argch"; }
                         if (data.Substring(i - 4, 5) == "argth") { f1[k] = "argth"; }
                         if (data.Substring(i - 4, 5) == "cotan") { f1[k] = "cotan"; }
                         if (data.Substring(i - 4, 5) == "sqrt'") { f1[k] = "sqrt'"; }
                         if (data.Substring(i - 4, 5) == "coth'") { f1[k] = "coth'"; }
                     }
                     if (i > 4)
                     {
                         if (data.Substring(i - 5, 6) == "arcsin") { f1[k] = "arcsin"; }
                         if (data.Substring(i - 5, 6) == "arccos") { f1[k] = "arccos"; }
                         if (data.Substring(i - 5, 6) == "arctan") { f1[k] = "arctan"; }
                         if (data.Substring(i - 5, 6) == "argsh'") { f1[k] = "argsh'"; }
                         if (data.Substring(i - 5, 6) == "argch'") { f1[k] = "argch'"; }
                         if (data.Substring(i - 5, 6) == "argth'") { f1[k] = "argth'"; }
                         if (data.Substring(i - 5, 6) == "cotan'") { f1[k] = "cotan'"; }
                     }
                     if (i > 5)
                     {
                         if (data.Substring(i - 6, 7) == "arccotg") { f1[k] = "arccotg"; }
                         if (data.Substring(i - 6, 7) == "arcsin'") { f1[k] = "arcsin'"; }
                         if (data.Substring(i - 6, 7) == "arccos'") { f1[k] = "arccos'"; }
                         if (data.Substring(i - 6, 7) == "arctan'") { f1[k] = "arctan'"; }
                     }
                     if (i > 6)
                     {
                         if (data.Substring(i - 7, 8) == "arccotg'") { f1[k] = "arccotg'"; }
                     }
                     if (f1[k].Length != 0)
                     {
                         try
                         {
                             i -= f1[k].Length - 1;
                         }
                         catch { }
                         locked1[k] = '$';
                     }
                     if (g1[k].Length != 0)
                     {
                         try
                         {
                             i -= g1[k].Length - 1;
                         }
                         catch { }
                         continue;
                         // locked1[k] = '$';
                     }
                 a:
                     if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) locked1[k] = '$';
                     if (i > 0)
                     {
                         if (data[i - 1] == '/' || data[i - 1] == '*' || data[i - 1] == 'x' || data[i - 1] == '+')
                         {
                             s1[k] = data[i - 1];
                             if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                         }
                         else if (data[i - 1] == 'C' || data[i - 1] == 'A' || data[i - 1] == 'P' || data[i - 1] == '^' || data[i - 1] == 'E')
                         {
                             g1[k] = "" + data[i - 1];
                             if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                         }
                         else if (data[i - 1] == '-')
                         {
                             if (i > 1)
                             {
                                 if (data[i - 2] == '/' || data[i - 2] == '*' || data[i - 2] == 'x' || data[i - 2] == '+')
                                 {
                                     if (t1[k] != Math.Exp(1) && t1[k] != Math.PI)
                                     {
                                         t1[k] = -1;
                                         s1[k] = data[i - 2];
                                     }
                                     else
                                     {
                                         t1[k] = -1 * t1[k];
                                         s1[k] = data[i - 2];
                                     }
                                 }
                                 else if (data[i - 2] == ')' || data[i - 2] == ']' || data[i - 2] == '!' || data[i - 2] == 'e' || data[i - 2] == 'π' || (data[i - 2] >= '0' && data[i - 2] <= '9' || data[i - 2] == '²'))
                                 {
                                     s1[k] = 'x';
                                     k++;
                                     t1[k] = -1;
                                     s1[k] = '+';
                                 }
                                 else if (data[i - 2] == 'C' || data[i - 2] == 'A' || data[i - 2] == 'P' || data[i - 2] == '^' || data[i - 2] == 'E')
                                 {
                                     g1[k] = "" + data[i - 2];
                                     if (t1[k] != Math.Exp(1) && t1[k] != Math.PI)
                                     {
                                         t1[k] = -1;
                                     }
                                     else
                                     {
                                         t1[k] = -1 * t1[k];
                                     }
                                 }
                                 else if (data[i - 2] == '(' || data[i - 2] == '[')
                                 {
                                     s1[k] = 'x';
                                     k++;
                                     t1[k] = -1;
                                     continue;
                                 }

                             }
                             else
                             {
                                 s1[k] = 'x';
                                 k++;
                                 t1[k] = -1;
                             }
                         }
                         else if (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²')
                         {
                             if (t1[k] == Math.PI) s1[k] = 'm';
                             else s1[k] = 'x';
                             if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                         }
                         else if (data[i - 1] == '(' || data[i - 1] == '[')
                         {
                             if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                             continue;
                         }
                         else if ((data[i - 1] >= 'a' && data[i - 1] <= 'z') && data[i - 1] != 'x') if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                     }
                     else
                     {
                         if (t1[k] != Math.Exp(1) && t1[k] != Math.PI) t1[k] = 1;
                     }
                 }
                 else if (data[i] == ')' || data[i] == ']')
                 {
                     k++;
                     p1[k] = data[i];
                     locked1[k] = '$';
                 }
                 else if (i == 0 && data[i] != '+' && data[i] != '-' && data[i] != '(' && data[i] != '[')
                 {
                     if (num != string.Empty)
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;
                     }
                 }
                 else if (data[i] == '^')
                 {
                     if (i > 0)
                     {
                         if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                         {
                             k++;
                             t1[k] = double.Parse(num);
                             num = string.Empty;
                             g1[k] = "" + data[i];
                         }
                     }
                 }
                 else if (data[i] == 'C' || data[i] == 'A' || data[i] == 'P' || data[i] == 'E')
                 {
                     if (i > 0)
                     {
                         if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                         {
                             k++;
                             t1[k] = double.Parse(num);
                             num = string.Empty;
                             g1[k] = "" + data[i];
                         }
                     }
                 }
                 else if (data[i] == '*' || data[i] == 'x')
                 {
                     if (i > 0)
                     {
                         if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                         {
                             k++;
                             t1[k] = double.Parse(num);
                             num = string.Empty;
                             s1[k] = data[i];
                         }
                     }
                 }
                 else if (data[i] == '/')
                 {
                     if (i > 0)
                     {
                         if (num != string.Empty && (data[i - 1] == ')' || data[i - 1] == ']' || (data[i - 1] >= '0' && data[i - 1] <= '9') || data[i - 1] == 'e' || (data[i - 1] >= 'a' && data[i - 1] <= 'z') || data[i - 1] == '!' || data[i - 1] == 'π' || data[i - 1] == '²'))
                         {
                             k++;
                             t1[k] = double.Parse(num);
                             num = string.Empty;
                             s1[k] = '/';
                         }
                     }
                 }
                 else if (data[i] == '!' || data[i] == '²')
                 {
                     if (num == string.Empty)
                     {
                         k++;
                         f1[k] = "" + data[i];
                         locked1[k] = '$';
                     }
                     else
                     {
                         k++;
                         t1[k] = double.Parse(num);
                         num = string.Empty;
                         s1[k] = 'x';
                         k++;
                         locked1[k] = '$';
                         f1[k] = "" + data[i];
                     }
                 }
             }
            
             int n = k;
             for (i = 0; i <= n; i++)
             {
                 if(i > 0)
                 {
                     if ((g1[i] != string.Empty && (p1[i - 1] != ')' && s1[i] != '+' && (t1[i - 1] < '0' || t1[i - 1] > '9'))) || (g1[i] != string.Empty && (p1[i] != '(' && (t1[i] < '0' || t1[i] > '9'))))
                     {
                         ErrorSyntaxe = true;
                         Mxg.Add("Vous avez mal utilisé la fonction \""+ g1[i] +"\" dans votre oppérationn.");
                     }
                 }
             }*/
        }
        ///
        public string Verify()
        {
            FErrorParenthese(Operation);
            FErrorSigne(Operation);
            FunctionCorrect(Operation);
            OperationVerify(Operation);
            string mxg = string.Empty;
            if (Mxg.Count != 0)
            {
                if (Mxg.Count == 1)
                {
                    mxg = "Vous avez commit une erreurs de saisie\n";
                    ErrorSyntaxe = true;
                    mxg += Mxg[0];
                }
                else
                {
                    mxg = "Vous avez commit " + Mxg.Count + " erreurs de saisie\n";
                    ErrorSyntaxe = true;
                    for (int i = 0; i < Mxg.Count; i++)
                    {
                        mxg += "Erreur n° " + (i + 1) + "\n" + Mxg[i] + "\n";
                    }
                }
            }
            return mxg;
        }
    }
}
