﻿namespace AMGT32MXDeltaBox_CodiTheck
{
    partial class MXDeltaBoxForm
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MXDeltaBoxForm));
            this.textBoxPoseOperation = new System.Windows.Forms.TextBox();
            this.textBoxAns = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.buttonVirgule = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape7 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape6 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape4 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShapeFonction = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShapeOperateur = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShapeNum = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShapeGlobal = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.labelPaveNumeric = new System.Windows.Forms.Label();
            this.buttonDivision = new System.Windows.Forms.Button();
            this.buttonMultiplication = new System.Windows.Forms.Button();
            this.buttonSoustration = new System.Windows.Forms.Button();
            this.buttonAddition = new System.Windows.Forms.Button();
            this.buttonEgale = new System.Windows.Forms.Button();
            this.labelOpérateur = new System.Windows.Forms.Label();
            this.labelFonction = new System.Windows.Forms.Label();
            this.buttonOff = new System.Windows.Forms.Button();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonFonctionDerivee = new System.Windows.Forms.Button();
            this.buttonDRG = new System.Windows.Forms.Button();
            this.buttonCalculer = new System.Windows.Forms.Button();
            this.labelModeAngle = new System.Windows.Forms.Label();
            this.labelModeFonction = new System.Windows.Forms.Label();
            this.buttonDelete = new System.Windows.Forms.Button();
            this.buttonPuissance10 = new System.Windows.Forms.Button();
            this.buttonPUplet = new System.Windows.Forms.Button();
            this.buttonArrangement = new System.Windows.Forms.Button();
            this.buttonCombinaison = new System.Windows.Forms.Button();
            this.buttonPGCD = new System.Windows.Forms.Button();
            this.buttonPPCM = new System.Windows.Forms.Button();
            this.buttonDiv = new System.Windows.Forms.Button();
            this.buttonReste = new System.Windows.Forms.Button();
            this.buttonSqrt = new System.Windows.Forms.Button();
            this.buttonArctan = new System.Windows.Forms.Button();
            this.buttonArccos = new System.Windows.Forms.Button();
            this.buttonArcsin = new System.Windows.Forms.Button();
            this.buttonValeurAbs = new System.Windows.Forms.Button();
            this.buttonParenthese = new System.Windows.Forms.Button();
            this.buttonExposant = new System.Windows.Forms.Button();
            this.buttonTan = new System.Windows.Forms.Button();
            this.buttonTh = new System.Windows.Forms.Button();
            this.buttonCh = new System.Windows.Forms.Button();
            this.buttonSin = new System.Windows.Forms.Button();
            this.buttonLn = new System.Windows.Forms.Button();
            this.buttonExp = new System.Windows.Forms.Button();
            this.buttonFactorielle = new System.Windows.Forms.Button();
            this.buttonPi = new System.Windows.Forms.Button();
            this.buttonSh = new System.Windows.Forms.Button();
            this.buttonCos = new System.Windows.Forms.Button();
            this.buttonLog = new System.Windows.Forms.Button();
            this.labelInfo = new System.Windows.Forms.Label();
            this.labelOpp = new System.Windows.Forms.Label();
            this.buttonParenthese1 = new System.Windows.Forms.Button();
            this.buttonParenthese2 = new System.Windows.Forms.Button();
            this.buttonCursorRight = new System.Windows.Forms.Button();
            this.buttonCursorLeft = new System.Windows.Forms.Button();
            this.labelCursorPosition = new System.Windows.Forms.Label();
            this.labelGauche = new System.Windows.Forms.Label();
            this.buttonArgch = new System.Windows.Forms.Button();
            this.buttonArgth = new System.Windows.Forms.Button();
            this.buttonArgsh = new System.Windows.Forms.Button();
            this.buttonArccotan = new System.Windows.Forms.Button();
            this.buttonCotan = new System.Windows.Forms.Button();
            this.buttonSign = new System.Windows.Forms.Button();
            this.buttonDeg = new System.Windows.Forms.Button();
            this.buttonRad = new System.Windows.Forms.Button();
            this.buttonVAbs2 = new System.Windows.Forms.Button();
            this.buttonVAbs1 = new System.Windows.Forms.Button();
            this.buttonGauche = new System.Windows.Forms.ToolStripButton();
            this.buttonDroite = new System.Windows.Forms.ToolStripButton();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.OutilMenuOption = new System.Windows.Forms.ToolStripSplitButton();
            this.OptionResultat = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionUsingInfo = new System.Windows.Forms.ToolStripMenuItem();
            this.labelAns = new System.Windows.Forms.Label();
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.buttonCoth = new System.Windows.Forms.Button();
            this.toolStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // textBoxPoseOperation
            // 
            this.textBoxPoseOperation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBoxPoseOperation.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxPoseOperation.Font = new System.Drawing.Font("Cambria", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxPoseOperation.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.textBoxPoseOperation.Location = new System.Drawing.Point(39, 35);
            this.textBoxPoseOperation.Name = "textBoxPoseOperation";
            this.textBoxPoseOperation.Size = new System.Drawing.Size(500, 23);
            this.textBoxPoseOperation.TabIndex = 0;
            this.textBoxPoseOperation.TextChanged += new System.EventHandler(this.textBoxPoseOperation_TextChanged);
            this.textBoxPoseOperation.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // textBoxAns
            // 
            this.textBoxAns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.textBoxAns.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxAns.Font = new System.Drawing.Font("Cambria", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxAns.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.textBoxAns.Location = new System.Drawing.Point(39, 58);
            this.textBoxAns.Name = "textBoxAns";
            this.textBoxAns.Size = new System.Drawing.Size(500, 23);
            this.textBoxAns.TabIndex = 1;
            this.textBoxAns.Text = "0";
            this.textBoxAns.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxAns.Click += new System.EventHandler(this.textBoxAns_Click);
            this.textBoxAns.DoubleClick += new System.EventHandler(this.textBoxAns_DoubleClick);
            this.textBoxAns.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Black;
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button8.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button8.Location = new System.Drawing.Point(565, 121);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(33, 33);
            this.button8.TabIndex = 9;
            this.button8.Tag = "Chiffre 8";
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            this.button8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button8.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button8.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonVirgule
            // 
            this.buttonVirgule.BackColor = System.Drawing.Color.Black;
            this.buttonVirgule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVirgule.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonVirgule.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVirgule.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.buttonVirgule.Location = new System.Drawing.Point(604, 232);
            this.buttonVirgule.Name = "buttonVirgule";
            this.buttonVirgule.Size = new System.Drawing.Size(33, 33);
            this.buttonVirgule.TabIndex = 12;
            this.buttonVirgule.Tag = "Virgule (pour un réel)";
            this.buttonVirgule.Text = ",";
            this.buttonVirgule.UseVisualStyleBackColor = false;
            this.buttonVirgule.Click += new System.EventHandler(this.buttonVirgule_Click);
            this.buttonVirgule.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonVirgule.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonVirgule.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.Color.Black;
            this.button0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button0.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button0.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button0.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button0.Location = new System.Drawing.Point(526, 232);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(72, 33);
            this.button0.TabIndex = 13;
            this.button0.Tag = "Chiffre 0 (null)";
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            this.button0.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button0.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button0.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.Black;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button3.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button3.Location = new System.Drawing.Point(604, 193);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(33, 33);
            this.button3.TabIndex = 14;
            this.button3.Tag = "Chiffre 3";
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button3.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button3.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Black;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button6.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button6.Location = new System.Drawing.Point(604, 157);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(33, 33);
            this.button6.TabIndex = 15;
            this.button6.Tag = "Chiffre 6";
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            this.button6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button6.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button6.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Black;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button9.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button9.Location = new System.Drawing.Point(604, 121);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(33, 33);
            this.button9.TabIndex = 16;
            this.button9.Tag = "Chiffre 9";
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            this.button9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button9.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button9.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button2.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button2.Location = new System.Drawing.Point(565, 193);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(33, 33);
            this.button2.TabIndex = 17;
            this.button2.Tag = "Chiffre 2";
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button2.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button2.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Black;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button5.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button5.Location = new System.Drawing.Point(565, 157);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(33, 33);
            this.button5.TabIndex = 18;
            this.button5.Tag = "Chiffre 5";
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            this.button5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button5.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button5.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button1.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button1.Location = new System.Drawing.Point(526, 193);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(33, 33);
            this.button1.TabIndex = 19;
            this.button1.Tag = "Chiffre 1";
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            this.button1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button1.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button1.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Black;
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button4.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button4.Location = new System.Drawing.Point(526, 157);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(33, 33);
            this.button4.TabIndex = 20;
            this.button4.Tag = "Chiffre 4";
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button4.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button4.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Black;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button7.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.LightSkyBlue;
            this.button7.Location = new System.Drawing.Point(526, 121);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(33, 33);
            this.button7.TabIndex = 21;
            this.button7.Tag = "Chiffre 7";
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            this.button7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.button7.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.button7.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape7,
            this.rectangleShape6,
            this.rectangleShape5,
            this.rectangleShape4,
            this.rectangleShapeFonction,
            this.rectangleShapeOperateur,
            this.rectangleShapeNum,
            this.rectangleShapeGlobal});
            this.shapeContainer1.Size = new System.Drawing.Size(694, 457);
            this.shapeContainer1.TabIndex = 22;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape7
            // 
            this.rectangleShape7.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShape7.Location = new System.Drawing.Point(273, 105);
            this.rectangleShape7.Name = "rectangleShape7";
            this.rectangleShape7.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShape7.Size = new System.Drawing.Size(75, 299);
            this.rectangleShape7.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShape7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShape6
            // 
            this.rectangleShape6.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShape6.Location = new System.Drawing.Point(198, 112);
            this.rectangleShape6.Name = "rectangleShape6";
            this.rectangleShape6.Size = new System.Drawing.Size(75, 292);
            this.rectangleShape6.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShape6.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShape5.Location = new System.Drawing.Point(121, 105);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShape5.Size = new System.Drawing.Size(77, 268);
            this.rectangleShape5.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShape5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShape4
            // 
            this.rectangleShape4.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShape4.Location = new System.Drawing.Point(32, 112);
            this.rectangleShape4.Name = "rectangleShape4";
            this.rectangleShape4.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShape4.Size = new System.Drawing.Size(89, 292);
            this.rectangleShape4.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShape4.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShapeFonction
            // 
            this.rectangleShapeFonction.BackColor = System.Drawing.Color.Gray;
            this.rectangleShapeFonction.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangleShapeFonction.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShapeFonction.Location = new System.Drawing.Point(32, 105);
            this.rectangleShapeFonction.Name = "rectangleShapeFonction";
            this.rectangleShapeFonction.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShapeFonction.Size = new System.Drawing.Size(383, 313);
            this.rectangleShapeFonction.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShapeFonction.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShapeOperateur
            // 
            this.rectangleShapeOperateur.BackColor = System.Drawing.Color.Gray;
            this.rectangleShapeOperateur.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangleShapeOperateur.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShapeOperateur.Location = new System.Drawing.Point(415, 112);
            this.rectangleShapeOperateur.Name = "rectangleShapeOperateur";
            this.rectangleShapeOperateur.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShapeOperateur.Size = new System.Drawing.Size(101, 162);
            this.rectangleShapeOperateur.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShapeOperateur.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShapeNum
            // 
            this.rectangleShapeNum.BackColor = System.Drawing.Color.Gray;
            this.rectangleShapeNum.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangleShapeNum.BorderColor = System.Drawing.SystemColors.ControlLightLight;
            this.rectangleShapeNum.Location = new System.Drawing.Point(516, 112);
            this.rectangleShapeNum.Name = "rectangleShapeNum";
            this.rectangleShapeNum.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShapeNum.Size = new System.Drawing.Size(132, 162);
            this.rectangleShapeNum.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShapeNum.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // rectangleShapeGlobal
            // 
            this.rectangleShapeGlobal.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.rectangleShapeGlobal.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque;
            this.rectangleShapeGlobal.BorderColor = System.Drawing.Color.Transparent;
            this.rectangleShapeGlobal.Location = new System.Drawing.Point(13, 11);
            this.rectangleShapeGlobal.Name = "rectangleShapeGlobal";
            this.rectangleShapeGlobal.SelectionColor = System.Drawing.Color.Transparent;
            this.rectangleShapeGlobal.Size = new System.Drawing.Size(666, 433);
            this.rectangleShapeGlobal.Tag = "455";
            this.rectangleShapeGlobal.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.rectangleShapeGlobal.DoubleClick += new System.EventHandler(this.rectangleShapeGlobal_DoubleClick);
            this.rectangleShapeGlobal.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            // 
            // labelPaveNumeric
            // 
            this.labelPaveNumeric.AutoSize = true;
            this.labelPaveNumeric.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelPaveNumeric.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPaveNumeric.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelPaveNumeric.Location = new System.Drawing.Point(530, 103);
            this.labelPaveNumeric.Name = "labelPaveNumeric";
            this.labelPaveNumeric.Size = new System.Drawing.Size(94, 14);
            this.labelPaveNumeric.TabIndex = 36;
            this.labelPaveNumeric.Text = "Pavé Numérique";
            // 
            // buttonDivision
            // 
            this.buttonDivision.BackColor = System.Drawing.Color.Teal;
            this.buttonDivision.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDivision.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDivision.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDivision.ForeColor = System.Drawing.Color.White;
            this.buttonDivision.Location = new System.Drawing.Point(431, 121);
            this.buttonDivision.Name = "buttonDivision";
            this.buttonDivision.Size = new System.Drawing.Size(33, 33);
            this.buttonDivision.TabIndex = 37;
            this.buttonDivision.Tag = "Division réelle (rapport)";
            this.buttonDivision.Text = "/";
            this.buttonDivision.UseVisualStyleBackColor = false;
            this.buttonDivision.Click += new System.EventHandler(this.buttonDivision_Click);
            this.buttonDivision.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonDivision.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDivision.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonMultiplication
            // 
            this.buttonMultiplication.BackColor = System.Drawing.Color.Teal;
            this.buttonMultiplication.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonMultiplication.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonMultiplication.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMultiplication.ForeColor = System.Drawing.Color.White;
            this.buttonMultiplication.Location = new System.Drawing.Point(431, 157);
            this.buttonMultiplication.Name = "buttonMultiplication";
            this.buttonMultiplication.Size = new System.Drawing.Size(33, 33);
            this.buttonMultiplication.TabIndex = 38;
            this.buttonMultiplication.Tag = "Multiplication (produit)";
            this.buttonMultiplication.Text = "x";
            this.buttonMultiplication.UseVisualStyleBackColor = false;
            this.buttonMultiplication.Click += new System.EventHandler(this.buttonMultiplication_Click);
            this.buttonMultiplication.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonMultiplication.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonMultiplication.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonSoustration
            // 
            this.buttonSoustration.BackColor = System.Drawing.Color.Teal;
            this.buttonSoustration.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSoustration.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSoustration.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSoustration.ForeColor = System.Drawing.Color.White;
            this.buttonSoustration.Location = new System.Drawing.Point(431, 193);
            this.buttonSoustration.Name = "buttonSoustration";
            this.buttonSoustration.Size = new System.Drawing.Size(33, 33);
            this.buttonSoustration.TabIndex = 39;
            this.buttonSoustration.Tag = "Soustraction (différence)";
            this.buttonSoustration.Text = "-";
            this.buttonSoustration.UseVisualStyleBackColor = false;
            this.buttonSoustration.Click += new System.EventHandler(this.buttonSoustration_Click);
            this.buttonSoustration.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonSoustration.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonSoustration.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonAddition
            // 
            this.buttonAddition.BackColor = System.Drawing.Color.Teal;
            this.buttonAddition.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonAddition.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonAddition.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonAddition.ForeColor = System.Drawing.Color.White;
            this.buttonAddition.Location = new System.Drawing.Point(431, 230);
            this.buttonAddition.Name = "buttonAddition";
            this.buttonAddition.Size = new System.Drawing.Size(72, 33);
            this.buttonAddition.TabIndex = 40;
            this.buttonAddition.Tag = "Addition (somme)";
            this.buttonAddition.Text = "+";
            this.buttonAddition.UseVisualStyleBackColor = false;
            this.buttonAddition.Click += new System.EventHandler(this.buttonAddition_Click);
            this.buttonAddition.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonAddition.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonAddition.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonEgale
            // 
            this.buttonEgale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonEgale.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonEgale.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonEgale.Font = new System.Drawing.Font("Cambria", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEgale.ForeColor = System.Drawing.Color.White;
            this.buttonEgale.Location = new System.Drawing.Point(470, 121);
            this.buttonEgale.Name = "buttonEgale";
            this.buttonEgale.Size = new System.Drawing.Size(33, 105);
            this.buttonEgale.TabIndex = 41;
            this.buttonEgale.Tag = "Egale (Résultat)";
            this.buttonEgale.Text = "=";
            this.buttonEgale.UseVisualStyleBackColor = false;
            this.buttonEgale.Click += new System.EventHandler(this.buttonEgale_Click);
            this.buttonEgale.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonEgale.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonEgale.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // labelOpérateur
            // 
            this.labelOpérateur.AutoSize = true;
            this.labelOpérateur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelOpérateur.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpérateur.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelOpérateur.Location = new System.Drawing.Point(434, 103);
            this.labelOpérateur.Name = "labelOpérateur";
            this.labelOpérateur.Size = new System.Drawing.Size(66, 14);
            this.labelOpérateur.TabIndex = 42;
            this.labelOpérateur.Text = "Opérateurs";
            // 
            // labelFonction
            // 
            this.labelFonction.AutoSize = true;
            this.labelFonction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelFonction.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelFonction.ForeColor = System.Drawing.Color.White;
            this.labelFonction.Location = new System.Drawing.Point(155, 101);
            this.labelFonction.Name = "labelFonction";
            this.labelFonction.Size = new System.Drawing.Size(140, 14);
            this.labelFonction.TabIndex = 43;
            this.labelFonction.Text = "Fonctions Mathématiques";
            // 
            // buttonOff
            // 
            this.buttonOff.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buttonOff.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonOff.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonOff.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOff.ForeColor = System.Drawing.Color.Lavender;
            this.buttonOff.Location = new System.Drawing.Point(604, 32);
            this.buttonOff.Name = "buttonOff";
            this.buttonOff.Size = new System.Drawing.Size(59, 26);
            this.buttonOff.TabIndex = 56;
            this.buttonOff.Tag = "Eteindre la calculatrice (Ferme l\'application)";
            this.buttonOff.Text = "OFF";
            this.buttonOff.UseVisualStyleBackColor = false;
            this.buttonOff.Click += new System.EventHandler(this.buttonOff_Click);
            this.buttonOff.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonOff.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonOff.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonReset
            // 
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.buttonReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReset.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReset.ForeColor = System.Drawing.Color.Lavender;
            this.buttonReset.Location = new System.Drawing.Point(545, 32);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(59, 26);
            this.buttonReset.TabIndex = 57;
            this.buttonReset.Tag = "Effacer tout";
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            this.buttonReset.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonReset.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonReset.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonFonctionDerivee
            // 
            this.buttonFonctionDerivee.BackColor = System.Drawing.Color.Black;
            this.buttonFonctionDerivee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFonctionDerivee.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonFonctionDerivee.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFonctionDerivee.ForeColor = System.Drawing.Color.Lavender;
            this.buttonFonctionDerivee.Location = new System.Drawing.Point(347, 246);
            this.buttonFonctionDerivee.Name = "buttonFonctionDerivee";
            this.buttonFonctionDerivee.Size = new System.Drawing.Size(69, 26);
            this.buttonFonctionDerivee.TabIndex = 58;
            this.buttonFonctionDerivee.Tag = "Utiliser les fontions dérivées";
            this.buttonFonctionDerivee.Text = "f \'(x)";
            this.buttonFonctionDerivee.UseVisualStyleBackColor = false;
            this.buttonFonctionDerivee.Click += new System.EventHandler(this.buttonFonctionDerivee_Click);
            this.buttonFonctionDerivee.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonFonctionDerivee.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonFonctionDerivee.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonDRG
            // 
            this.buttonDRG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.buttonDRG.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDRG.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDRG.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDRG.ForeColor = System.Drawing.Color.Black;
            this.buttonDRG.Location = new System.Drawing.Point(604, 58);
            this.buttonDRG.Name = "buttonDRG";
            this.buttonDRG.Size = new System.Drawing.Size(59, 24);
            this.buttonDRG.TabIndex = 59;
            this.buttonDRG.Tag = "Définir l\'unité de l\'angle (Degré/Radian)";
            this.buttonDRG.Text = "DR";
            this.buttonDRG.UseVisualStyleBackColor = false;
            this.buttonDRG.Click += new System.EventHandler(this.buttonDRG_Click);
            this.buttonDRG.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonDRG.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDRG.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCalculer
            // 
            this.buttonCalculer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.buttonCalculer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCalculer.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCalculer.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCalculer.ForeColor = System.Drawing.Color.White;
            this.buttonCalculer.Location = new System.Drawing.Point(545, 58);
            this.buttonCalculer.Name = "buttonCalculer";
            this.buttonCalculer.Size = new System.Drawing.Size(59, 24);
            this.buttonCalculer.TabIndex = 60;
            this.buttonCalculer.Tag = "Obtenir le résultat de l\'opération";
            this.buttonCalculer.Text = "Calculer";
            this.buttonCalculer.UseVisualStyleBackColor = false;
            this.buttonCalculer.Click += new System.EventHandler(this.buttonCalculer_Click);
            this.buttonCalculer.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonCalculer.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCalculer.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // labelModeAngle
            // 
            this.labelModeAngle.AutoSize = true;
            this.labelModeAngle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelModeAngle.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModeAngle.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelModeAngle.Location = new System.Drawing.Point(345, 18);
            this.labelModeAngle.Name = "labelModeAngle";
            this.labelModeAngle.Size = new System.Drawing.Size(29, 12);
            this.labelModeAngle.TabIndex = 61;
            this.labelModeAngle.Text = "RAD";
            // 
            // labelModeFonction
            // 
            this.labelModeFonction.AutoSize = true;
            this.labelModeFonction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelModeFonction.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelModeFonction.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelModeFonction.Location = new System.Drawing.Point(156, 18);
            this.labelModeFonction.Name = "labelModeFonction";
            this.labelModeFonction.Size = new System.Drawing.Size(109, 12);
            this.labelModeFonction.TabIndex = 62;
            this.labelModeFonction.Text = "Mode Fonction";
            // 
            // buttonDelete
            // 
            this.buttonDelete.BackColor = System.Drawing.Color.Purple;
            this.buttonDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDelete.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDelete.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDelete.ForeColor = System.Drawing.Color.Lavender;
            this.buttonDelete.Location = new System.Drawing.Point(347, 214);
            this.buttonDelete.Name = "buttonDelete";
            this.buttonDelete.Size = new System.Drawing.Size(69, 26);
            this.buttonDelete.TabIndex = 66;
            this.buttonDelete.Tag = "Supprimer";
            this.buttonDelete.Text = "Delete";
            this.buttonDelete.UseVisualStyleBackColor = false;
            this.buttonDelete.Click += new System.EventHandler(this.buttonDelete_Click);
            this.buttonDelete.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonDelete.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDelete.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonPuissance10
            // 
            this.buttonPuissance10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPuissance10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPuissance10.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPuissance10.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPuissance10.ForeColor = System.Drawing.Color.Lavender;
            this.buttonPuissance10.Location = new System.Drawing.Point(273, 278);
            this.buttonPuissance10.Name = "buttonPuissance10";
            this.buttonPuissance10.Size = new System.Drawing.Size(70, 26);
            this.buttonPuissance10.TabIndex = 93;
            this.buttonPuissance10.Tag = "x.10 puissance y ";
            this.buttonPuissance10.Text = "E";
            this.buttonPuissance10.UseVisualStyleBackColor = false;
            this.buttonPuissance10.Click += new System.EventHandler(this.buttonPuissance10_Click);
            this.buttonPuissance10.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonPuissance10.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonPuissance10.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonPUplet
            // 
            this.buttonPUplet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPUplet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPUplet.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPUplet.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPUplet.ForeColor = System.Drawing.Color.Lavender;
            this.buttonPUplet.Location = new System.Drawing.Point(121, 278);
            this.buttonPUplet.Name = "buttonPUplet";
            this.buttonPUplet.Size = new System.Drawing.Size(70, 26);
            this.buttonPUplet.TabIndex = 92;
            this.buttonPUplet.Tag = "P-Uplet";
            this.buttonPUplet.Text = "n P k";
            this.buttonPUplet.UseVisualStyleBackColor = false;
            this.buttonPUplet.Click += new System.EventHandler(this.buttonPUplet_Click);
            this.buttonPUplet.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonPUplet.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonPUplet.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArrangement
            // 
            this.buttonArrangement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArrangement.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArrangement.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArrangement.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArrangement.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArrangement.Location = new System.Drawing.Point(121, 246);
            this.buttonArrangement.Name = "buttonArrangement";
            this.buttonArrangement.Size = new System.Drawing.Size(70, 26);
            this.buttonArrangement.TabIndex = 91;
            this.buttonArrangement.Tag = "Arrangement de k dans n";
            this.buttonArrangement.Text = "n A k";
            this.buttonArrangement.UseVisualStyleBackColor = false;
            this.buttonArrangement.Click += new System.EventHandler(this.buttonArrangement_Click);
            this.buttonArrangement.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonArrangement.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArrangement.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCombinaison
            // 
            this.buttonCombinaison.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCombinaison.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCombinaison.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCombinaison.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCombinaison.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCombinaison.Location = new System.Drawing.Point(121, 214);
            this.buttonCombinaison.Name = "buttonCombinaison";
            this.buttonCombinaison.Size = new System.Drawing.Size(70, 26);
            this.buttonCombinaison.TabIndex = 90;
            this.buttonCombinaison.Tag = "Combinaison de k dans n";
            this.buttonCombinaison.Text = "n C k";
            this.buttonCombinaison.UseVisualStyleBackColor = false;
            this.buttonCombinaison.Click += new System.EventHandler(this.buttonCombinaison_Click);
            this.buttonCombinaison.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonCombinaison.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCombinaison.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonPGCD
            // 
            this.buttonPGCD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPGCD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPGCD.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPGCD.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPGCD.ForeColor = System.Drawing.Color.Lavender;
            this.buttonPGCD.Location = new System.Drawing.Point(42, 342);
            this.buttonPGCD.Name = "buttonPGCD";
            this.buttonPGCD.Size = new System.Drawing.Size(70, 26);
            this.buttonPGCD.TabIndex = 89;
            this.buttonPGCD.Tag = "Plus Grand Commun Diviseur";
            this.buttonPGCD.Text = "a pgcd b";
            this.buttonPGCD.UseVisualStyleBackColor = false;
            this.buttonPGCD.Click += new System.EventHandler(this.buttonPGCD_Click);
            this.buttonPGCD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonPGCD.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonPGCD.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonPPCM
            // 
            this.buttonPPCM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPPCM.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPPCM.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPPCM.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPPCM.ForeColor = System.Drawing.Color.Lavender;
            this.buttonPPCM.Location = new System.Drawing.Point(42, 374);
            this.buttonPPCM.Name = "buttonPPCM";
            this.buttonPPCM.Size = new System.Drawing.Size(70, 26);
            this.buttonPPCM.TabIndex = 88;
            this.buttonPPCM.Tag = "Plus Petit Commun Multiple";
            this.buttonPPCM.Text = "a ppcm b";
            this.buttonPPCM.UseVisualStyleBackColor = false;
            this.buttonPPCM.Click += new System.EventHandler(this.buttonPPCM_Click);
            this.buttonPPCM.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonPPCM.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonPPCM.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonDiv
            // 
            this.buttonDiv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonDiv.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDiv.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDiv.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDiv.ForeColor = System.Drawing.Color.Lavender;
            this.buttonDiv.Location = new System.Drawing.Point(121, 310);
            this.buttonDiv.Name = "buttonDiv";
            this.buttonDiv.Size = new System.Drawing.Size(70, 26);
            this.buttonDiv.TabIndex = 87;
            this.buttonDiv.Tag = "Division entière";
            this.buttonDiv.Text = "a div b";
            this.buttonDiv.UseVisualStyleBackColor = false;
            this.buttonDiv.Click += new System.EventHandler(this.buttonDiv_Click);
            this.buttonDiv.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonDiv.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDiv.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonReste
            // 
            this.buttonReste.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonReste.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonReste.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonReste.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonReste.ForeColor = System.Drawing.Color.Lavender;
            this.buttonReste.Location = new System.Drawing.Point(121, 342);
            this.buttonReste.Name = "buttonReste";
            this.buttonReste.Size = new System.Drawing.Size(70, 26);
            this.buttonReste.TabIndex = 86;
            this.buttonReste.Tag = "Le reste d\'une division euclidienne";
            this.buttonReste.Text = "a rest b";
            this.buttonReste.UseVisualStyleBackColor = false;
            this.buttonReste.Click += new System.EventHandler(this.buttonReste_Click);
            this.buttonReste.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonReste.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonReste.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonSqrt
            // 
            this.buttonSqrt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonSqrt.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSqrt.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSqrt.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSqrt.ForeColor = System.Drawing.Color.Lavender;
            this.buttonSqrt.Location = new System.Drawing.Point(273, 246);
            this.buttonSqrt.Name = "buttonSqrt";
            this.buttonSqrt.Size = new System.Drawing.Size(70, 26);
            this.buttonSqrt.TabIndex = 85;
            this.buttonSqrt.Tag = "Racine carrée de x";
            this.buttonSqrt.Text = "√(x)";
            this.buttonSqrt.UseVisualStyleBackColor = false;
            this.buttonSqrt.Click += new System.EventHandler(this.buttonSqrt_Click);
            this.buttonSqrt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonSqrt.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonSqrt.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArctan
            // 
            this.buttonArctan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArctan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArctan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArctan.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArctan.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArctan.Location = new System.Drawing.Point(197, 342);
            this.buttonArctan.Name = "buttonArctan";
            this.buttonArctan.Size = new System.Drawing.Size(70, 26);
            this.buttonArctan.TabIndex = 84;
            this.buttonArctan.Tag = "Réçiproque de tangente";
            this.buttonArctan.Text = "arctan(x)";
            this.buttonArctan.UseVisualStyleBackColor = false;
            this.buttonArctan.Click += new System.EventHandler(this.buttonArctan_Click);
            this.buttonArctan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonArctan.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArctan.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArccos
            // 
            this.buttonArccos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArccos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArccos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArccos.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArccos.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArccos.Location = new System.Drawing.Point(198, 310);
            this.buttonArccos.Name = "buttonArccos";
            this.buttonArccos.Size = new System.Drawing.Size(70, 26);
            this.buttonArccos.TabIndex = 83;
            this.buttonArccos.Tag = "Réçiproque de cosinus";
            this.buttonArccos.Text = "arccos(x)";
            this.buttonArccos.UseVisualStyleBackColor = false;
            this.buttonArccos.Click += new System.EventHandler(this.buttonArccos_Click);
            this.buttonArccos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonArccos.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArccos.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArcsin
            // 
            this.buttonArcsin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArcsin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArcsin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArcsin.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArcsin.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArcsin.Location = new System.Drawing.Point(198, 278);
            this.buttonArcsin.Name = "buttonArcsin";
            this.buttonArcsin.Size = new System.Drawing.Size(70, 26);
            this.buttonArcsin.TabIndex = 82;
            this.buttonArcsin.Tag = "Réçiproque de sinus";
            this.buttonArcsin.Text = "arcsin(x)";
            this.buttonArcsin.UseVisualStyleBackColor = false;
            this.buttonArcsin.Click += new System.EventHandler(this.buttonArcsin_Click);
            this.buttonArcsin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonArcsin.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArcsin.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonValeurAbs
            // 
            this.buttonValeurAbs.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonValeurAbs.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonValeurAbs.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonValeurAbs.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonValeurAbs.ForeColor = System.Drawing.Color.Lavender;
            this.buttonValeurAbs.Location = new System.Drawing.Point(273, 182);
            this.buttonValeurAbs.Name = "buttonValeurAbs";
            this.buttonValeurAbs.Size = new System.Drawing.Size(70, 26);
            this.buttonValeurAbs.TabIndex = 81;
            this.buttonValeurAbs.Tag = "Valeur absolue";
            this.buttonValeurAbs.Text = "[x]";
            this.buttonValeurAbs.UseVisualStyleBackColor = false;
            this.buttonValeurAbs.Click += new System.EventHandler(this.buttonValeurAbs_Click);
            this.buttonValeurAbs.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonValeurAbs.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonValeurAbs.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonParenthese
            // 
            this.buttonParenthese.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonParenthese.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonParenthese.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonParenthese.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonParenthese.ForeColor = System.Drawing.Color.Lavender;
            this.buttonParenthese.Location = new System.Drawing.Point(273, 118);
            this.buttonParenthese.Name = "buttonParenthese";
            this.buttonParenthese.Size = new System.Drawing.Size(70, 26);
            this.buttonParenthese.TabIndex = 80;
            this.buttonParenthese.Tag = "Parenthèses";
            this.buttonParenthese.Text = "(x)";
            this.buttonParenthese.UseVisualStyleBackColor = false;
            this.buttonParenthese.Click += new System.EventHandler(this.buttonParenthese_Click);
            this.buttonParenthese.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonParenthese.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonParenthese.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonExposant
            // 
            this.buttonExposant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonExposant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonExposant.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonExposant.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExposant.ForeColor = System.Drawing.Color.Lavender;
            this.buttonExposant.Location = new System.Drawing.Point(273, 150);
            this.buttonExposant.Name = "buttonExposant";
            this.buttonExposant.Size = new System.Drawing.Size(70, 26);
            this.buttonExposant.TabIndex = 79;
            this.buttonExposant.Tag = "y exposant x";
            this.buttonExposant.Text = "y^x";
            this.buttonExposant.UseVisualStyleBackColor = false;
            this.buttonExposant.Click += new System.EventHandler(this.buttonExposant_Click);
            this.buttonExposant.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonExposant.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonExposant.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonTan
            // 
            this.buttonTan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonTan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonTan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonTan.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTan.ForeColor = System.Drawing.Color.Lavender;
            this.buttonTan.Location = new System.Drawing.Point(197, 214);
            this.buttonTan.Name = "buttonTan";
            this.buttonTan.Size = new System.Drawing.Size(70, 26);
            this.buttonTan.TabIndex = 78;
            this.buttonTan.Tag = "Tangente";
            this.buttonTan.Text = "tan(x)";
            this.buttonTan.UseVisualStyleBackColor = false;
            this.buttonTan.Click += new System.EventHandler(this.buttonTan_Click);
            this.buttonTan.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonTan.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonTan.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonTh
            // 
            this.buttonTh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonTh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonTh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonTh.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonTh.ForeColor = System.Drawing.Color.Lavender;
            this.buttonTh.Location = new System.Drawing.Point(42, 182);
            this.buttonTh.Name = "buttonTh";
            this.buttonTh.Size = new System.Drawing.Size(70, 26);
            this.buttonTh.TabIndex = 77;
            this.buttonTh.Tag = "Tangente hyperbolique";
            this.buttonTh.Text = "th(x)";
            this.buttonTh.UseVisualStyleBackColor = false;
            this.buttonTh.Click += new System.EventHandler(this.buttonTh_Click);
            this.buttonTh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonTh.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonTh.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCh
            // 
            this.buttonCh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCh.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCh.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCh.Location = new System.Drawing.Point(42, 150);
            this.buttonCh.Name = "buttonCh";
            this.buttonCh.Size = new System.Drawing.Size(70, 26);
            this.buttonCh.TabIndex = 76;
            this.buttonCh.Tag = "Cosinus hrperbolique";
            this.buttonCh.Text = "ch(x)";
            this.buttonCh.UseVisualStyleBackColor = false;
            this.buttonCh.Click += new System.EventHandler(this.buttonCh_Click);
            this.buttonCh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonCh.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCh.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonSin
            // 
            this.buttonSin.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonSin.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSin.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSin.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSin.ForeColor = System.Drawing.Color.Lavender;
            this.buttonSin.Location = new System.Drawing.Point(197, 150);
            this.buttonSin.Name = "buttonSin";
            this.buttonSin.Size = new System.Drawing.Size(70, 26);
            this.buttonSin.TabIndex = 75;
            this.buttonSin.Tag = "Sinus";
            this.buttonSin.Text = "sin(x)";
            this.buttonSin.UseVisualStyleBackColor = false;
            this.buttonSin.Click += new System.EventHandler(this.buttonSin_Click);
            this.buttonSin.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonSin.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonSin.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonLn
            // 
            this.buttonLn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonLn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLn.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLn.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLn.ForeColor = System.Drawing.Color.Lavender;
            this.buttonLn.Location = new System.Drawing.Point(121, 150);
            this.buttonLn.Name = "buttonLn";
            this.buttonLn.Size = new System.Drawing.Size(70, 26);
            this.buttonLn.TabIndex = 74;
            this.buttonLn.Tag = "Logarithme Népérien";
            this.buttonLn.Text = "ln(x)";
            this.buttonLn.UseVisualStyleBackColor = false;
            this.buttonLn.Click += new System.EventHandler(this.buttonLn_Click);
            this.buttonLn.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonLn.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonLn.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonExp
            // 
            this.buttonExp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonExp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonExp.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonExp.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonExp.ForeColor = System.Drawing.Color.Lavender;
            this.buttonExp.Location = new System.Drawing.Point(121, 118);
            this.buttonExp.Name = "buttonExp";
            this.buttonExp.Size = new System.Drawing.Size(70, 26);
            this.buttonExp.TabIndex = 72;
            this.buttonExp.Tag = "Valeur exponancielle (2,71828182845905)";
            this.buttonExp.Text = "e";
            this.buttonExp.UseVisualStyleBackColor = false;
            this.buttonExp.Click += new System.EventHandler(this.buttonExp2_Click);
            this.buttonExp.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonExp.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonExp.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonFactorielle
            // 
            this.buttonFactorielle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonFactorielle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonFactorielle.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonFactorielle.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonFactorielle.ForeColor = System.Drawing.Color.Lavender;
            this.buttonFactorielle.Location = new System.Drawing.Point(273, 214);
            this.buttonFactorielle.Name = "buttonFactorielle";
            this.buttonFactorielle.Size = new System.Drawing.Size(70, 26);
            this.buttonFactorielle.TabIndex = 71;
            this.buttonFactorielle.Tag = "Factorielle de n";
            this.buttonFactorielle.Text = "n!";
            this.buttonFactorielle.UseVisualStyleBackColor = false;
            this.buttonFactorielle.Click += new System.EventHandler(this.buttonFactorielle_Click);
            this.buttonFactorielle.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonFactorielle.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonFactorielle.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonPi
            // 
            this.buttonPi.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonPi.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonPi.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonPi.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonPi.ForeColor = System.Drawing.Color.Lavender;
            this.buttonPi.Location = new System.Drawing.Point(197, 118);
            this.buttonPi.Name = "buttonPi";
            this.buttonPi.Size = new System.Drawing.Size(70, 26);
            this.buttonPi.TabIndex = 70;
            this.buttonPi.Tag = "Valeur Circonférencielle π = 3,14159265358979";
            this.buttonPi.Text = "π";
            this.buttonPi.UseVisualStyleBackColor = false;
            this.buttonPi.Click += new System.EventHandler(this.buttonPi_Click);
            this.buttonPi.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonPi.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonPi.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonSh
            // 
            this.buttonSh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonSh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSh.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSh.ForeColor = System.Drawing.Color.Lavender;
            this.buttonSh.Location = new System.Drawing.Point(42, 118);
            this.buttonSh.Name = "buttonSh";
            this.buttonSh.Size = new System.Drawing.Size(70, 26);
            this.buttonSh.TabIndex = 69;
            this.buttonSh.Tag = "Sinus hyperbolique";
            this.buttonSh.Text = "sh(x)";
            this.buttonSh.UseVisualStyleBackColor = false;
            this.buttonSh.Click += new System.EventHandler(this.buttonSh_Click);
            this.buttonSh.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonSh.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonSh.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCos
            // 
            this.buttonCos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCos.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCos.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCos.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCos.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCos.Location = new System.Drawing.Point(197, 182);
            this.buttonCos.Name = "buttonCos";
            this.buttonCos.Size = new System.Drawing.Size(70, 26);
            this.buttonCos.TabIndex = 68;
            this.buttonCos.Tag = "Cosinus";
            this.buttonCos.Text = "cos(x)";
            this.buttonCos.UseVisualStyleBackColor = false;
            this.buttonCos.Click += new System.EventHandler(this.buttonCos_Click);
            this.buttonCos.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonCos.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCos.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonLog
            // 
            this.buttonLog.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonLog.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonLog.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonLog.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLog.ForeColor = System.Drawing.Color.Lavender;
            this.buttonLog.Location = new System.Drawing.Point(121, 182);
            this.buttonLog.Name = "buttonLog";
            this.buttonLog.Size = new System.Drawing.Size(70, 26);
            this.buttonLog.TabIndex = 67;
            this.buttonLog.Tag = "Logarithme décimal";
            this.buttonLog.Text = "log(x)";
            this.buttonLog.UseVisualStyleBackColor = false;
            this.buttonLog.Click += new System.EventHandler(this.buttonLog_Click);
            this.buttonLog.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.buttonLog.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonLog.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // labelInfo
            // 
            this.labelInfo.AutoSize = true;
            this.labelInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelInfo.Font = new System.Drawing.Font("Cambria", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.labelInfo.Location = new System.Drawing.Point(69, 85);
            this.labelInfo.Name = "labelInfo";
            this.labelInfo.Size = new System.Drawing.Size(32, 14);
            this.labelInfo.TabIndex = 94;
            this.labelInfo.Text = "label";
            // 
            // labelOpp
            // 
            this.labelOpp.AutoSize = true;
            this.labelOpp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelOpp.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOpp.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelOpp.Location = new System.Drawing.Point(40, 18);
            this.labelOpp.Name = "labelOpp";
            this.labelOpp.Size = new System.Drawing.Size(45, 12);
            this.labelOpp.TabIndex = 95;
            this.labelOpp.Text = "label";
            // 
            // buttonParenthese1
            // 
            this.buttonParenthese1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonParenthese1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonParenthese1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonParenthese1.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonParenthese1.ForeColor = System.Drawing.Color.Lavender;
            this.buttonParenthese1.Location = new System.Drawing.Point(347, 150);
            this.buttonParenthese1.Name = "buttonParenthese1";
            this.buttonParenthese1.Size = new System.Drawing.Size(31, 26);
            this.buttonParenthese1.TabIndex = 96;
            this.buttonParenthese1.Tag = "Parenthèses";
            this.buttonParenthese1.Text = "(";
            this.buttonParenthese1.UseVisualStyleBackColor = false;
            this.buttonParenthese1.Click += new System.EventHandler(this.buttonParenthese1_Click);
            this.buttonParenthese1.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonParenthese1.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonParenthese2
            // 
            this.buttonParenthese2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonParenthese2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonParenthese2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonParenthese2.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonParenthese2.ForeColor = System.Drawing.Color.Lavender;
            this.buttonParenthese2.Location = new System.Drawing.Point(384, 150);
            this.buttonParenthese2.Name = "buttonParenthese2";
            this.buttonParenthese2.Size = new System.Drawing.Size(32, 26);
            this.buttonParenthese2.TabIndex = 97;
            this.buttonParenthese2.Tag = "Parenthèses";
            this.buttonParenthese2.Text = ")";
            this.buttonParenthese2.UseVisualStyleBackColor = false;
            this.buttonParenthese2.Click += new System.EventHandler(this.buttonParenthese2_Click);
            this.buttonParenthese2.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonParenthese2.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCursorRight
            // 
            this.buttonCursorRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCursorRight.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCursorRight.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCursorRight.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCursorRight.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCursorRight.Location = new System.Drawing.Point(384, 118);
            this.buttonCursorRight.Name = "buttonCursorRight";
            this.buttonCursorRight.Size = new System.Drawing.Size(32, 26);
            this.buttonCursorRight.TabIndex = 99;
            this.buttonCursorRight.Tag = "Déplace le curseur vers la droite";
            this.buttonCursorRight.Text = "=>";
            this.buttonCursorRight.UseVisualStyleBackColor = false;
            this.buttonCursorRight.Click += new System.EventHandler(this.buttonCursorRight_Click);
            this.buttonCursorRight.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCursorRight.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCursorLeft
            // 
            this.buttonCursorLeft.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCursorLeft.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCursorLeft.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCursorLeft.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCursorLeft.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCursorLeft.Location = new System.Drawing.Point(347, 118);
            this.buttonCursorLeft.Name = "buttonCursorLeft";
            this.buttonCursorLeft.Size = new System.Drawing.Size(31, 26);
            this.buttonCursorLeft.TabIndex = 98;
            this.buttonCursorLeft.Tag = "Déplcer le curseur vers la gauche";
            this.buttonCursorLeft.Text = "<=";
            this.buttonCursorLeft.UseVisualStyleBackColor = false;
            this.buttonCursorLeft.Click += new System.EventHandler(this.buttonCursorLeft_Click);
            this.buttonCursorLeft.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCursorLeft.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // labelCursorPosition
            // 
            this.labelCursorPosition.AutoSize = true;
            this.labelCursorPosition.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelCursorPosition.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCursorPosition.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelCursorPosition.Location = new System.Drawing.Point(429, 18);
            this.labelCursorPosition.Name = "labelCursorPosition";
            this.labelCursorPosition.Size = new System.Drawing.Size(45, 12);
            this.labelCursorPosition.TabIndex = 100;
            this.labelCursorPosition.Text = "label";
            // 
            // labelGauche
            // 
            this.labelGauche.AutoSize = true;
            this.labelGauche.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelGauche.Cursor = System.Windows.Forms.Cursors.Hand;
            this.labelGauche.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGauche.ForeColor = System.Drawing.Color.White;
            this.labelGauche.Location = new System.Drawing.Point(15, 40);
            this.labelGauche.Name = "labelGauche";
            this.labelGauche.Size = new System.Drawing.Size(21, 12);
            this.labelGauche.TabIndex = 101;
            this.labelGauche.Tag = "Ramène le curseur au debut ou à la fin de l\'opération";
            this.labelGauche.Text = "<>";
            this.labelGauche.Click += new System.EventHandler(this.labelGauche_Click);
            this.labelGauche.DoubleClick += new System.EventHandler(this.labelGauche_DoubleClick);
            this.labelGauche.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.labelGauche.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArgch
            // 
            this.buttonArgch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArgch.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArgch.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArgch.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArgch.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArgch.Location = new System.Drawing.Point(42, 278);
            this.buttonArgch.Name = "buttonArgch";
            this.buttonArgch.Size = new System.Drawing.Size(70, 26);
            this.buttonArgch.TabIndex = 102;
            this.buttonArgch.Tag = "Argument cosinus hyperbolique";
            this.buttonArgch.Text = "argch(x)";
            this.buttonArgch.UseVisualStyleBackColor = false;
            this.buttonArgch.Click += new System.EventHandler(this.buttonArgch_Click);
            this.buttonArgch.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArgch.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArgth
            // 
            this.buttonArgth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArgth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArgth.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArgth.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArgth.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArgth.Location = new System.Drawing.Point(42, 310);
            this.buttonArgth.Name = "buttonArgth";
            this.buttonArgth.Size = new System.Drawing.Size(70, 26);
            this.buttonArgth.TabIndex = 103;
            this.buttonArgth.Tag = "Argument tangente hyperbolique";
            this.buttonArgth.Text = "argth(x)";
            this.buttonArgth.UseVisualStyleBackColor = false;
            this.buttonArgth.Click += new System.EventHandler(this.buttonArgth_Click);
            this.buttonArgth.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArgth.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArgsh
            // 
            this.buttonArgsh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArgsh.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArgsh.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArgsh.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArgsh.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArgsh.Location = new System.Drawing.Point(42, 246);
            this.buttonArgsh.Name = "buttonArgsh";
            this.buttonArgsh.Size = new System.Drawing.Size(70, 26);
            this.buttonArgsh.TabIndex = 104;
            this.buttonArgsh.Tag = "Argument sinus hyperbolique";
            this.buttonArgsh.Text = "argsh(x)";
            this.buttonArgsh.UseVisualStyleBackColor = false;
            this.buttonArgsh.Click += new System.EventHandler(this.buttonArgsh_Click);
            this.buttonArgsh.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArgsh.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonArccotan
            // 
            this.buttonArccotan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonArccotan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonArccotan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonArccotan.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonArccotan.ForeColor = System.Drawing.Color.Lavender;
            this.buttonArccotan.Location = new System.Drawing.Point(198, 374);
            this.buttonArccotan.Name = "buttonArccotan";
            this.buttonArccotan.Size = new System.Drawing.Size(70, 26);
            this.buttonArccotan.TabIndex = 105;
            this.buttonArccotan.Tag = "Réçiproque de cotangente";
            this.buttonArccotan.Text = "arccotg(x)";
            this.buttonArccotan.UseVisualStyleBackColor = false;
            this.buttonArccotan.Click += new System.EventHandler(this.buttonArccotan_Click);
            this.buttonArccotan.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonArccotan.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonCotan
            // 
            this.buttonCotan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCotan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCotan.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCotan.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCotan.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCotan.Location = new System.Drawing.Point(198, 246);
            this.buttonCotan.Name = "buttonCotan";
            this.buttonCotan.Size = new System.Drawing.Size(70, 26);
            this.buttonCotan.TabIndex = 106;
            this.buttonCotan.Tag = "Cotangente";
            this.buttonCotan.Text = "cotan(x)";
            this.buttonCotan.UseVisualStyleBackColor = false;
            this.buttonCotan.Click += new System.EventHandler(this.buttonCotan_Click);
            this.buttonCotan.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCotan.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonSign
            // 
            this.buttonSign.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonSign.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSign.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonSign.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSign.ForeColor = System.Drawing.Color.Lavender;
            this.buttonSign.Location = new System.Drawing.Point(273, 310);
            this.buttonSign.Name = "buttonSign";
            this.buttonSign.Size = new System.Drawing.Size(70, 26);
            this.buttonSign.TabIndex = 107;
            this.buttonSign.Tag = "retourne la valeur \"-1\" ou \"1\" si x est négatif ou positif ";
            this.buttonSign.Text = "sign(x)";
            this.buttonSign.UseVisualStyleBackColor = false;
            this.buttonSign.Click += new System.EventHandler(this.buttonSign_Click);
            this.buttonSign.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonSign.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonDeg
            // 
            this.buttonDeg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonDeg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonDeg.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonDeg.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonDeg.ForeColor = System.Drawing.Color.Lavender;
            this.buttonDeg.Location = new System.Drawing.Point(273, 342);
            this.buttonDeg.Name = "buttonDeg";
            this.buttonDeg.Size = new System.Drawing.Size(70, 26);
            this.buttonDeg.TabIndex = 108;
            this.buttonDeg.Tag = "Convertie x en degré";
            this.buttonDeg.Text = "deg(x)";
            this.buttonDeg.UseVisualStyleBackColor = false;
            this.buttonDeg.Click += new System.EventHandler(this.buttonDeg_Click);
            this.buttonDeg.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDeg.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonRad
            // 
            this.buttonRad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonRad.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonRad.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonRad.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRad.ForeColor = System.Drawing.Color.Lavender;
            this.buttonRad.Location = new System.Drawing.Point(273, 374);
            this.buttonRad.Name = "buttonRad";
            this.buttonRad.Size = new System.Drawing.Size(70, 26);
            this.buttonRad.TabIndex = 109;
            this.buttonRad.Tag = "Convertie x en radian";
            this.buttonRad.Text = "rad(x)";
            this.buttonRad.UseVisualStyleBackColor = false;
            this.buttonRad.Click += new System.EventHandler(this.buttonRad_Click);
            this.buttonRad.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonRad.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonVAbs2
            // 
            this.buttonVAbs2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonVAbs2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVAbs2.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonVAbs2.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVAbs2.ForeColor = System.Drawing.Color.Lavender;
            this.buttonVAbs2.Location = new System.Drawing.Point(384, 182);
            this.buttonVAbs2.Name = "buttonVAbs2";
            this.buttonVAbs2.Size = new System.Drawing.Size(32, 26);
            this.buttonVAbs2.TabIndex = 111;
            this.buttonVAbs2.Tag = "Symbole valeur absolue";
            this.buttonVAbs2.Text = "]";
            this.buttonVAbs2.UseVisualStyleBackColor = false;
            this.buttonVAbs2.Click += new System.EventHandler(this.buttonVAbs2_Click);
            this.buttonVAbs2.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonVAbs2.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonVAbs1
            // 
            this.buttonVAbs1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonVAbs1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonVAbs1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonVAbs1.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonVAbs1.ForeColor = System.Drawing.Color.Lavender;
            this.buttonVAbs1.Location = new System.Drawing.Point(347, 182);
            this.buttonVAbs1.Name = "buttonVAbs1";
            this.buttonVAbs1.Size = new System.Drawing.Size(31, 26);
            this.buttonVAbs1.TabIndex = 110;
            this.buttonVAbs1.Tag = "Symbole valeur absolue";
            this.buttonVAbs1.Text = "[";
            this.buttonVAbs1.UseVisualStyleBackColor = false;
            this.buttonVAbs1.Click += new System.EventHandler(this.buttonVAbs1_Click);
            this.buttonVAbs1.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonVAbs1.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonGauche
            // 
            this.buttonGauche.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonGauche.Image = ((System.Drawing.Image)(resources.GetObject("buttonGauche.Image")));
            this.buttonGauche.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonGauche.Name = "buttonGauche";
            this.buttonGauche.Size = new System.Drawing.Size(23, 22);
            this.buttonGauche.Tag = "Calcule précédent";
            this.buttonGauche.Text = "Précédent";
            this.buttonGauche.Click += new System.EventHandler(this.buttonGauche_Click);
            this.buttonGauche.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonGauche.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // buttonDroite
            // 
            this.buttonDroite.BackColor = System.Drawing.Color.Black;
            this.buttonDroite.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.buttonDroite.Image = ((System.Drawing.Image)(resources.GetObject("buttonDroite.Image")));
            this.buttonDroite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonDroite.Name = "buttonDroite";
            this.buttonDroite.Size = new System.Drawing.Size(23, 22);
            this.buttonDroite.Tag = "Calcule suivant des calcules précédentes";
            this.buttonDroite.Text = "Suivant";
            this.buttonDroite.Click += new System.EventHandler(this.buttonDroite_Click);
            this.buttonDroite.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonDroite.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // toolStrip
            // 
            this.toolStrip.BackColor = System.Drawing.Color.Black;
            this.toolStrip.Dock = System.Windows.Forms.DockStyle.None;
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonGauche,
            this.buttonDroite,
            this.OutilMenuOption});
            this.toolStrip.Location = new System.Drawing.Point(483, 279);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip.Size = new System.Drawing.Size(90, 25);
            this.toolStrip.TabIndex = 64;
            this.toolStrip.Text = "toolStrip1";
            // 
            // OutilMenuOption
            // 
            this.OutilMenuOption.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.OutilMenuOption.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptionResultat,
            this.OptionUsingInfo});
            this.OutilMenuOption.Image = ((System.Drawing.Image)(resources.GetObject("OutilMenuOption.Image")));
            this.OutilMenuOption.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.OutilMenuOption.Name = "OutilMenuOption";
            this.OutilMenuOption.Size = new System.Drawing.Size(32, 22);
            this.OutilMenuOption.Text = "Option";
            this.OutilMenuOption.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.OutilMenuOption.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // OptionResultat
            // 
            this.OptionResultat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OptionResultat.ForeColor = System.Drawing.Color.Lavender;
            this.OptionResultat.Name = "OptionResultat";
            this.OptionResultat.Size = new System.Drawing.Size(211, 22);
            this.OptionResultat.Text = "Résultat";
            this.OptionResultat.Click += new System.EventHandler(this.OptionResultat_Click);
            // 
            // OptionUsingInfo
            // 
            this.OptionUsingInfo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.OptionUsingInfo.ForeColor = System.Drawing.SystemColors.Window;
            this.OptionUsingInfo.Name = "OptionUsingInfo";
            this.OptionUsingInfo.Size = new System.Drawing.Size(211, 22);
            this.OptionUsingInfo.Text = "Information sur l\'utilisation";
            this.OptionUsingInfo.Click += new System.EventHandler(this.OptionUsingInfo_Click);
            // 
            // labelAns
            // 
            this.labelAns.AutoSize = true;
            this.labelAns.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelAns.Font = new System.Drawing.Font("Lucida Console", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelAns.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.labelAns.Location = new System.Drawing.Point(430, 88);
            this.labelAns.Name = "labelAns";
            this.labelAns.Size = new System.Drawing.Size(85, 12);
            this.labelAns.TabIndex = 112;
            this.labelAns.Text = "Ans Normal";
            // 
            // pictureBox
            // 
            this.pictureBox.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.pictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.BackgroundImage")));
            this.pictureBox.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.ErrorImage")));
            this.pictureBox.InitialImage = ((System.Drawing.Image)(resources.GetObject("pictureBox.InitialImage")));
            this.pictureBox.Location = new System.Drawing.Point(470, 310);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(120, 121);
            this.pictureBox.TabIndex = 113;
            this.pictureBox.TabStop = false;
            this.pictureBox.DoubleClick += new System.EventHandler(this.rectangleShapeGlobal_DoubleClick);
            // 
            // buttonCoth
            // 
            this.buttonCoth.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.buttonCoth.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonCoth.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.buttonCoth.Font = new System.Drawing.Font("Cambria", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCoth.ForeColor = System.Drawing.Color.Lavender;
            this.buttonCoth.Location = new System.Drawing.Point(42, 214);
            this.buttonCoth.Name = "buttonCoth";
            this.buttonCoth.Size = new System.Drawing.Size(70, 26);
            this.buttonCoth.TabIndex = 114;
            this.buttonCoth.Tag = "Cotangente hyperbolique";
            this.buttonCoth.Text = "coth(x)";
            this.buttonCoth.UseVisualStyleBackColor = false;
            this.buttonCoth.Click += new System.EventHandler(this.buttonCoth_Click);
            this.buttonCoth.MouseEnter += new System.EventHandler(this.MooseEnterMethode);
            this.buttonCoth.MouseLeave += new System.EventHandler(this.MooseLeaveMethode);
            // 
            // MXDeltaBoxForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(694, 457);
            this.Controls.Add(this.buttonCoth);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.labelAns);
            this.Controls.Add(this.buttonVAbs2);
            this.Controls.Add(this.buttonVAbs1);
            this.Controls.Add(this.buttonRad);
            this.Controls.Add(this.buttonDeg);
            this.Controls.Add(this.buttonSign);
            this.Controls.Add(this.buttonCotan);
            this.Controls.Add(this.buttonArccotan);
            this.Controls.Add(this.buttonArgsh);
            this.Controls.Add(this.buttonArgth);
            this.Controls.Add(this.buttonArgch);
            this.Controls.Add(this.labelGauche);
            this.Controls.Add(this.labelCursorPosition);
            this.Controls.Add(this.buttonCursorRight);
            this.Controls.Add(this.buttonCursorLeft);
            this.Controls.Add(this.buttonParenthese2);
            this.Controls.Add(this.buttonParenthese1);
            this.Controls.Add(this.labelOpp);
            this.Controls.Add(this.labelInfo);
            this.Controls.Add(this.buttonPuissance10);
            this.Controls.Add(this.buttonPUplet);
            this.Controls.Add(this.buttonArrangement);
            this.Controls.Add(this.buttonCombinaison);
            this.Controls.Add(this.buttonPGCD);
            this.Controls.Add(this.buttonPPCM);
            this.Controls.Add(this.buttonDiv);
            this.Controls.Add(this.buttonReste);
            this.Controls.Add(this.buttonSqrt);
            this.Controls.Add(this.buttonArctan);
            this.Controls.Add(this.buttonArccos);
            this.Controls.Add(this.buttonArcsin);
            this.Controls.Add(this.buttonValeurAbs);
            this.Controls.Add(this.buttonParenthese);
            this.Controls.Add(this.buttonExposant);
            this.Controls.Add(this.buttonTan);
            this.Controls.Add(this.buttonTh);
            this.Controls.Add(this.buttonCh);
            this.Controls.Add(this.buttonSin);
            this.Controls.Add(this.buttonLn);
            this.Controls.Add(this.buttonExp);
            this.Controls.Add(this.buttonFactorielle);
            this.Controls.Add(this.buttonPi);
            this.Controls.Add(this.buttonSh);
            this.Controls.Add(this.buttonCos);
            this.Controls.Add(this.buttonLog);
            this.Controls.Add(this.buttonDelete);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.labelModeFonction);
            this.Controls.Add(this.labelModeAngle);
            this.Controls.Add(this.buttonCalculer);
            this.Controls.Add(this.buttonDRG);
            this.Controls.Add(this.buttonFonctionDerivee);
            this.Controls.Add(this.buttonReset);
            this.Controls.Add(this.buttonOff);
            this.Controls.Add(this.labelFonction);
            this.Controls.Add(this.labelOpérateur);
            this.Controls.Add(this.buttonEgale);
            this.Controls.Add(this.buttonAddition);
            this.Controls.Add(this.buttonSoustration);
            this.Controls.Add(this.buttonMultiplication);
            this.Controls.Add(this.buttonDivision);
            this.Controls.Add(this.labelPaveNumeric);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.buttonVirgule);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.textBoxAns);
            this.Controls.Add(this.textBoxPoseOperation);
            this.Controls.Add(this.shapeContainer1);
            this.ForeColor = System.Drawing.Color.Navy;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "MXDeltaBoxForm";
            this.Opacity = 0.96D;
            this.Text = "MX DeltaBox";
            this.TransparencyKey = System.Drawing.Color.Navy;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MXDeltaBoxForm_FormClosing);
            this.Load += new System.EventHandler(this.MXDeltaBoxForm_Load);
            this.Click += new System.EventHandler(this.rectangleShapeGlobal_Click);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.KeyDownMethode);
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape4;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape7;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape6;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        public System.Windows.Forms.Button buttonOff;
        public System.Windows.Forms.Button buttonReset;
        public System.Windows.Forms.Button buttonDRG;
        public System.Windows.Forms.Button buttonCalculer;
        public System.Windows.Forms.TextBox textBoxPoseOperation;
        public System.Windows.Forms.TextBox textBoxAns;
        public System.Windows.Forms.Button buttonDelete;
        public System.Windows.Forms.Button button8;
        public System.Windows.Forms.Button buttonVirgule;
        public System.Windows.Forms.Button button0;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button6;
        public System.Windows.Forms.Button button9;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button7;
        public System.Windows.Forms.Label labelPaveNumeric;
        public System.Windows.Forms.Button buttonDivision;
        public System.Windows.Forms.Button buttonMultiplication;
        public System.Windows.Forms.Button buttonSoustration;
        public System.Windows.Forms.Button buttonAddition;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShapeGlobal;
        public System.Windows.Forms.Button buttonEgale;
        public System.Windows.Forms.Label labelOpérateur;
        public System.Windows.Forms.Label labelFonction;
        public System.Windows.Forms.Button buttonFonctionDerivee;
        public System.Windows.Forms.Label labelModeAngle;
        public System.Windows.Forms.Label labelModeFonction;
        public System.Windows.Forms.Button buttonPuissance10;
        public System.Windows.Forms.Button buttonPUplet;
        public System.Windows.Forms.Button buttonArrangement;
        public System.Windows.Forms.Button buttonCombinaison;
        public System.Windows.Forms.Button buttonPGCD;
        public System.Windows.Forms.Button buttonPPCM;
        public System.Windows.Forms.Button buttonDiv;
        public System.Windows.Forms.Button buttonReste;
        public System.Windows.Forms.Button buttonSqrt;
        public System.Windows.Forms.Button buttonArctan;
        public System.Windows.Forms.Button buttonArccos;
        public System.Windows.Forms.Button buttonArcsin;
        public System.Windows.Forms.Button buttonValeurAbs;
        public System.Windows.Forms.Button buttonParenthese;
        public System.Windows.Forms.Button buttonExposant;
        public System.Windows.Forms.Button buttonTan;
        public System.Windows.Forms.Button buttonTh;
        public System.Windows.Forms.Button buttonCh;
        public System.Windows.Forms.Button buttonSin;
        public System.Windows.Forms.Button buttonLn;
        public System.Windows.Forms.Button buttonExp;
        public System.Windows.Forms.Button buttonFactorielle;
        public System.Windows.Forms.Button buttonPi;
        public System.Windows.Forms.Button buttonSh;
        public System.Windows.Forms.Button buttonCos;
        public System.Windows.Forms.Button buttonLog;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShapeFonction;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShapeOperateur;
        public Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShapeNum;
        public System.Windows.Forms.Label labelInfo;
        public System.Windows.Forms.Label labelOpp;
        public System.Windows.Forms.Button buttonParenthese1;
        public System.Windows.Forms.Button buttonParenthese2;
        public System.Windows.Forms.Button buttonCursorRight;
        public System.Windows.Forms.Button buttonCursorLeft;
        public System.Windows.Forms.Label labelCursorPosition;
        public System.Windows.Forms.Label labelGauche;
        public System.Windows.Forms.Button buttonArgch;
        public System.Windows.Forms.Button buttonArgth;
        public System.Windows.Forms.Button buttonArgsh;
        public System.Windows.Forms.Button buttonArccotan;
        public System.Windows.Forms.Button buttonCotan;
        public System.Windows.Forms.Button buttonSign;
        public System.Windows.Forms.Button buttonDeg;
        public System.Windows.Forms.Button buttonRad;
        public System.Windows.Forms.Button buttonVAbs2;
        public System.Windows.Forms.Button buttonVAbs1;
        public System.Windows.Forms.ToolStripButton buttonGauche;
        public System.Windows.Forms.ToolStripButton buttonDroite;
        public System.Windows.Forms.ToolStrip toolStrip;
        public System.Windows.Forms.ToolStripSplitButton OutilMenuOption;
        public System.Windows.Forms.ToolStripMenuItem OptionResultat;
        public System.Windows.Forms.Label labelAns;
        private System.Windows.Forms.PictureBox pictureBox;
        public System.Windows.Forms.Button buttonCoth;
        private System.Windows.Forms.ToolStripMenuItem OptionUsingInfo;
    }
}

