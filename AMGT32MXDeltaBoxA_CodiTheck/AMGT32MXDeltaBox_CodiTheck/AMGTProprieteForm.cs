﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public partial class AMGTProprieteForm : Form
    {
        public AMGTProprieteForm(MXDeltaBoxForm MxDeltaBox)
        {
            InitializeComponent();
            MxDelta = MxDeltaBox;
        }
        private MXDeltaBoxForm MxDelta;

        private void AMGTProprieteForm_Load(object sender, EventArgs e)
        {

        }

        private void buttonThemeDefault_Click(object sender, EventArgs e)
        {
            MxDelta.BackColor = Color.Navy;
            MxDelta.button0.BackColor = Color.Black;
            MxDelta.button1.BackColor = Color.Black;
            MxDelta.button2.BackColor = Color.Black;
            MxDelta.button3.BackColor = Color.Black;
            MxDelta.button4.BackColor = Color.Black;
            MxDelta.button5.BackColor = Color.Black;
            MxDelta.button6.BackColor = Color.Black;
            MxDelta.button7.BackColor = Color.Black;
            MxDelta.button8.BackColor = Color.Black;
            MxDelta.button9.BackColor = Color.Black;
            MxDelta.buttonVirgule.BackColor = Color.Black;
            MxDelta.buttonSh.BackColor = Color.FromArgb((byte)64,(byte)64,(byte)64);
            MxDelta.buttonCh.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonPPCM.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonPGCD.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonReste.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonDiv.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonExp.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonPi.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonLn.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonLog.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonCombinaison.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonArrangement.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonPUplet.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonSin.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonCos.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonTan.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonArctan.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonArcsin.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonArccos.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonExposant.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonTh.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonParenthese.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonValeurAbs.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonSqrt.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonPuissance10.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonFactorielle.BackColor = Color.FromArgb((byte)64, (byte)64, (byte)64);
            MxDelta.buttonFonctionDerivee.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.buttonDelete.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.buttonDRG.BackColor = Color.FromArgb((byte)192, (byte)192, (byte)0);
            MxDelta.buttonReset.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)192);
            MxDelta.buttonOff.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)0);
            MxDelta.buttonAddition.BackColor = Color.Teal;
            MxDelta.buttonSoustration.BackColor = Color.Teal;
            MxDelta.buttonMultiplication.BackColor = Color.Teal;
            MxDelta.buttonDivision.BackColor = Color.Teal;
            MxDelta.textBoxPoseOperation.BackColor = Color.FromArgb((byte)192, (byte)255, (byte)192);
            MxDelta.textBoxAns.BackColor = Color.FromArgb((byte)192, (byte)255, (byte)192);
            MxDelta.rectangleShapeFonction.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.rectangleShapeGlobal.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.rectangleShapeOperateur.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.rectangleShapeNum.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.labelModeFonction.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.labelModeAngle.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.labelFonction.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.labelOpérateur.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.labelPaveNumeric.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.toolStrip.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
        }

        private void buttonThemeOrit_Click(object sender, EventArgs e)
        {
            MxDelta.BackColor = Color.Navy;
            MxDelta.button0.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button1.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button2.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button3.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button4.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button5.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button6.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button7.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button8.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.button9.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.buttonVirgule.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.buttonSh.BackColor = Color.DarkGreen;
            MxDelta.buttonCh.BackColor = Color.DarkGreen;
            MxDelta.buttonPPCM.BackColor = Color.DarkGreen;
            MxDelta.buttonPGCD.BackColor = Color.DarkGreen;
            MxDelta.buttonReste.BackColor = Color.DarkGreen;
            MxDelta.buttonDiv.BackColor = Color.DarkGreen;
            MxDelta.buttonExp.BackColor = Color.DarkGreen;
            MxDelta.buttonPi.BackColor = Color.DarkGreen;
            MxDelta.buttonLn.BackColor = Color.DarkGreen;
            MxDelta.buttonLog.BackColor = Color.DarkGreen;
            MxDelta.buttonCombinaison.BackColor = Color.DarkGreen;
            MxDelta.buttonArrangement.BackColor = Color.DarkGreen;
            MxDelta.buttonPUplet.BackColor = Color.DarkGreen;
            MxDelta.buttonSin.BackColor = Color.DarkGreen;
            MxDelta.buttonCos.BackColor = Color.DarkGreen;
            MxDelta.buttonTan.BackColor = Color.DarkGreen;
            MxDelta.buttonArctan.BackColor = Color.DarkGreen;
            MxDelta.buttonArcsin.BackColor = Color.DarkGreen;
            MxDelta.buttonArccos.BackColor = Color.DarkGreen;
            MxDelta.buttonExposant.BackColor = Color.DarkGreen;
            MxDelta.buttonTh.BackColor = Color.DarkGreen;
            MxDelta.buttonParenthese.BackColor = Color.DarkGreen;
            MxDelta.buttonValeurAbs.BackColor = Color.DarkGreen;
            MxDelta.buttonSqrt.BackColor = Color.DarkGreen;
            MxDelta.buttonPuissance10.BackColor = Color.DarkGreen;
            MxDelta.buttonFactorielle.BackColor = Color.DarkGreen;
            MxDelta.buttonFonctionDerivee.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)192);
            MxDelta.buttonDelete.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)64);
            MxDelta.buttonDRG.BackColor = Color.FromArgb((byte)192, (byte)192, (byte)0);
            MxDelta.buttonReset.BackColor = Color.FromArgb((byte)0, (byte)0, (byte)192);
            MxDelta.buttonOff.BackColor = Color.FromArgb((byte)192, (byte)0, (byte)0);
            MxDelta.buttonAddition.BackColor = Color.MidnightBlue;
            MxDelta.buttonSoustration.BackColor = Color.MidnightBlue;
            MxDelta.buttonMultiplication.BackColor = Color.MidnightBlue;
            MxDelta.buttonDivision.BackColor = Color.MidnightBlue;
            MxDelta.buttonFonctionDerivee.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.textBoxPoseOperation.BackColor = Color.FromArgb((byte)192, (byte)255, (byte)192);
            MxDelta.textBoxAns.BackColor = Color.FromArgb((byte)192, (byte)255, (byte)192);
            MxDelta.rectangleShapeFonction.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.rectangleShapeGlobal.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.rectangleShapeOperateur.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.rectangleShapeNum.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.labelModeFonction.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.labelModeAngle.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.labelFonction.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.labelOpérateur.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.labelPaveNumeric.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
            MxDelta.toolStrip.BackColor = Color.FromArgb((byte)64, (byte)0, (byte)64);
        }
    }
}
