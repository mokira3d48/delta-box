﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace AMGT32MXDeltaBox_CodiTheck
{
    public partial class MXDeltaBoxForm : Form
    {
        public MXDeltaBoxForm()
        {
            InitializeComponent();
            labelInfo.Text = string.Empty;
            labelOpp.Text = string.Empty;
            iTheckMathFunction.Deg = false;
            iTheckMathFunction.Rad = true;
            labelCursorPosition.Text = "Ln " + textBoxPoseOperation.SelectionStart;
            NormalDisplayAns = true;
            rectangleShapeGlobal_DoubleClick1();
        }
        public bool FonctionDerive = false;
        int kFonctionDerive = 0;
        public static bool Deg = false;
        int kAngle = 0;
        private string textOperation;
        private int OAvant = -1, OApres;
        private bool SmallLength = false;
        public int NSousFormFraction { get; set; }
        public bool NormalDisplayAns { get; set; }
        public bool FractionRealDisplayAns { get; set; }
        public bool FractionAngleDisplayAns { get; set; }
        public bool FractionFaibleDisplayAns { get; set; }
        public bool FractionDoubleDisplayAns { get; set; }

        public string ParcourOperation(int indice)
        {
            iTheckClassMXDeltaBox.InitialiseData1();
            return iTheckClassMXDeltaBox.Operation[indice];
        }

        public void Calcule()
        {
            if (textBoxPoseOperation.TextLength > 0) if (textBoxPoseOperation.Text[textBoxPoseOperation.TextLength - 1] == '=') textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Delete(textBoxPoseOperation.Text);
            iTheckMathCalculator Calculatrice = new iTheckMathCalculator(textBoxPoseOperation.Text);
            Calculatrice.AnsFraction = FractionRealDisplayAns;
            Calculatrice.AnsFractionPI = FractionAngleDisplayAns;
            Calculatrice.AnsNormal = NormalDisplayAns;
            Calculatrice.AnsFractionNPrecision = NSousFormFraction;
            Calculatrice.AnsFractionDoublePrecision = FractionDoubleDisplayAns;
            Calculatrice.AnsFractionFaiblePrecision = FractionFaibleDisplayAns;
            string Ans = string.Empty;
            ClassPassageOperation Passage = new ClassPassageOperation(textBoxPoseOperation.Text);
            Ans = Passage.Verify();
            if (Passage.ErrorSyntaxe)
            {
                textBoxAns.Text = "Erreur de saisie";
                MessageForm Mxg = new MessageForm();
                Mxg.Rectangle2.BackColor = Color.Silver;
                Mxg.Title.BackColor = Color.Silver;
                Mxg.Title.Text = "Erreur de synthaxe mathématique";
                Mxg.Message.Text = Ans;
                Mxg.ShowDialog();
                Passage.ErrorSyntaxe = false;
                Passage.MessageError = string.Empty;
            }
            else
            {
                Ans = Calculatrice.Calculate();
                if (!iTheckMathFunction.Error)
                {
                    textBoxAns.Text = Ans;
                    if (textBoxPoseOperation.Text != string.Empty && textBoxPoseOperation.Text != textOperation)
                    {
                        textOperation = textBoxPoseOperation.Text;
                        if (!iTheckMathFunction.Error) iTheckClassMXDeltaBox.SaverOperation(textBoxPoseOperation.Text, Directory.GetCurrentDirectory() + @"\FileSaver.Arnold");
                    }
                    try
                    {
                        labelOpp.Text = string.Empty;
                        OApres = iTheckClassMXDeltaBox.TabOperationLength();
                        OAvant = -1;

                    }
                    catch { }
                }
                else
                {
                    iTheckMathFunction.Error = false;
                    textBoxAns.Text = "Erreur de Calcule";
                    MessageForm Mxg = new MessageForm();
                    Mxg.Title.Text = "Error";
                    Mxg.Message.Text = iTheckMathFunction.MessageError;
                    Mxg.ShowDialog();
                }
            }
        }
        public void Calcule2()
        {
            iTheckMathCalculator Calculatrice = new iTheckMathCalculator(textBoxPoseOperation.Text);
            Calculatrice.AnsFraction = FractionRealDisplayAns;
            Calculatrice.AnsFractionPI = FractionAngleDisplayAns;
            Calculatrice.AnsNormal = NormalDisplayAns;
            Calculatrice.AnsFractionNPrecision = NSousFormFraction;
            Calculatrice.AnsFractionDoublePrecision = FractionDoubleDisplayAns;
            Calculatrice.AnsFractionFaiblePrecision = FractionFaibleDisplayAns;
            textBoxAns.Text = Calculatrice.Calculate();
        }

        private void textBoxPoseOperation_TextChanged(object sender, EventArgs e)
        {
            labelCursorPosition.Text = "Ln " + textBoxPoseOperation.SelectionStart;
            if (textBoxPoseOperation.Text == "off") Application.Exit();
            if (textBoxPoseOperation.TextLength > 1)
            {
                if (textBoxPoseOperation.Text[textBoxPoseOperation.TextLength - 2] == '=')
                {
                    textBoxPoseOperation.Text = "" + textBoxPoseOperation.Text[textBoxPoseOperation.TextLength - 1];
                    textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
                }
            }
            if (textBoxPoseOperation.TextLength > 0)
            {
                if (textBoxPoseOperation.Text[textBoxPoseOperation.TextLength - 1] == '=')
                {
                    Calcule();
                    textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
                }
            }
        }

        private void MXDeltaBoxForm_Load(object sender, EventArgs e)
        {
            WellcomeForm Wellcome = new WellcomeForm();
            try
            {
                if (!File.Exists(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold"))
                {
                    BinaryWriter file = new BinaryWriter(new FileStream(Directory.GetCurrentDirectory() + @"\FileSaver.Arnold", FileMode.Create, FileAccess.Write));
                    file.Close();
                    Wellcome.Ttime = 5;
                    Wellcome.timer.Enabled = true;
                    Wellcome.ShowDialog();
                }
                else
                {
                    Wellcome.Ttime = 2;
                    Wellcome.timer.Enabled = true;
                    Wellcome.ShowDialog();
                }
                OApres = iTheckClassMXDeltaBox.TabOperationLength();
                NormalDisplayAns = true;
            }
            catch { }
        }

        private void buttonOff_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void buttonSh_Click(object sender, EventArgs e)
        {
            buttonSh_Click();
        }
        private void buttonSh_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sh", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 2, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sh'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
        }

        private void buttonCh_Click(object sender, EventArgs e)
        {
            buttonCh_Click();
        }
        private void buttonCh_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "ch", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 2, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "ch'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
        }

        private void buttonTh_Click(object sender, EventArgs e)
        {
            buttonTh_Click();
        }
        private void buttonTh_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "th", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 2, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "th'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
        }

        private void buttonPPCM_Click(object sender, EventArgs e)
        {
            buttonPPCM_Click();
        }
        private void buttonPPCM_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "ppcm", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 4, 0);
        }

        private void buttonPGCD_Click(object sender, EventArgs e)
        {
            buttonPGCD_Click();
        }
        private void buttonPGCD_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "pgcd", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 4, 0);
        }

        private void buttonReste_Click(object sender, EventArgs e)
        {
            buttonReste_Click();
        }
        private void buttonReste_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "rest", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 4, 0);
        }

        private void buttonDiv_Click(object sender, EventArgs e)
        {
            buttonDiv_Click();
        }
        private void buttonDiv_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "div", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 3, 0);
        }

        private void buttonExp2_Click(object sender, EventArgs e)
        {
            buttonExp2_Click();
        }
        private void buttonExp2_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "e", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonLn_Click(object sender, EventArgs e)
        {
            buttonLn_Click();
        }
        private void buttonLn_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "ln", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 2, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "ln'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
        }

        private void buttonLog_Click(object sender, EventArgs e)
        {
            buttonLog_Click();
        }
        private void buttonLog_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "log", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "log'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 4, 0);
            }
        }

        private void buttonCombinaison_Click(object sender, EventArgs e)
        {
            buttonCombinaison_Click();
        }
        private void buttonCombinaison_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "C", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonArrangement_Click(object sender, EventArgs e)
        {
            buttonArrangement_Click();
        }
        private void buttonArrangement_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "A", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonPUplet_Click(object sender, EventArgs e)
        {
            buttonPUplet_Click();
        }
        private void buttonPUplet_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "P", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonPi_Click(object sender, EventArgs e)
        {
            buttonPi_Click();
        }
        private void buttonPi_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "π", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonSin_Click(object sender, EventArgs e)
        {
            buttonSin_Click();
        }
        private void buttonSin_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sin", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sin'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 4, 0);
            }
        }

        private void buttonCos_Click(object sender, EventArgs e)
        {
            buttonCos_Click();
        }
        private void buttonCos_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "cos", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "cos'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 4, 0);
            }
        }

        private void buttonTan_Click(object sender, EventArgs e)
        {
            buttonTan_Click();
        }
        private void buttonTan_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "tan", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 3, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "tan'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 4, 0);
            }
        }

        private void buttonArcsin_Click(object sender, EventArgs e)
        {
            buttonArcsin_Click();
        }
        private void buttonArcsin_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arcsin", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arcsin'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 7, 0);
            }
        }

        private void buttonArccos_Click(object sender, EventArgs e)
        {
            buttonArccos_Click();
        }
        private void buttonArccos_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arccos", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arccos'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 7, 0);
            }
        }

        private void buttonArctan_Click(object sender, EventArgs e)
        {
            buttonArctan_Click();
        }
        private void buttonArctan_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arctan", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arctan'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 7, 0);
            }
        }

        private void buttonParenthese_Click(object sender, EventArgs e)
        {
            buttonParenthese_Click();
        }
        private void buttonParenthese_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, iTheckClassMXDeltaBox.FParenthese(textBoxPoseOperation.Text), textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 2, 0);
        }

        private void buttonExposant_Click(object sender, EventArgs e)
        {
            buttonExposant_Click();
        }
        private void buttonExposant_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "^", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonValeurAbs_Click(object sender, EventArgs e)
        {
            buttonValeurAbs_Click();
        }
        private void buttonValeurAbs_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, iTheckClassMXDeltaBox.FValeurAbs(textBoxPoseOperation.Text), textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 2, 0);
        }

        private void buttonFactorielle_Click(object sender, EventArgs e)
        {
            buttonFactorielle_Click();
        }
        private void buttonFactorielle_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "!", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonSqrt_Click(object sender, EventArgs e)
        {
            buttonSqrt_Click();
        }
        private void buttonSqrt_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "√", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 1, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sqrt'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
        }

        private void buttonPuissance10_Click(object sender, EventArgs e)
        {
            buttonPuissance10_Click();
        }
        private void buttonPuissance10_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "E", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            try
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.TouchDelete(textBoxPoseOperation.Text, textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind - 1, 0);
            }
            catch { }
        }

        private void buttonFonctionDerivee_Click(object sender, EventArgs e)
        {
            buttonFonctionDerivee_Click();
        }
        private void buttonFonctionDerivee_Click()
        {
            if (kFonctionDerive == 0)
            {
                this.FonctionDerive = true;
                buttonFonctionDerivee.Text = "f (x)";
                labelModeFonction.Text = "Mode Ft Dérivée";
                kFonctionDerive++;
            }
            else
            {
                this.FonctionDerive = false;
                buttonFonctionDerivee.Text = "f '(x)";
                labelModeFonction.Text = "Mode Fonction";
                kFonctionDerive = 0;
            }
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
        }

        private void buttonSoustration_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "-", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonDivision_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "/", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonMultiplication_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "x", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonAddition_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "+", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonEgale_Click(object sender, EventArgs e)
        {
            Calcule();
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "7", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "8", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "9", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "4", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "5", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "6", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "1", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "2", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "3", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void button0_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "0", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonVirgule_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, ",", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonDRG_Click(object sender, EventArgs e)
        {
            buttonDRG_Click();
        }
        private void buttonDRG_Click()
        {
            if (kAngle == 0)
            {
                iTheckMathFunction.Deg = true;
                iTheckMathFunction.Rad = false;
                labelModeAngle.Text = "DEG";
                kAngle++;
            }
            else
            {
                iTheckMathFunction.Deg = false;
                iTheckMathFunction.Rad = true;
                labelModeAngle.Text = "RAD";
                kAngle = 0;
            }
            Calcule();
            textBoxPoseOperation.Focus();
            //textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
        }
        private void buttonDRG_Click2()
        {
            if (kAngle == 0)
            {
                iTheckMathFunction.Deg = true;
                iTheckMathFunction.Rad = false;
                labelModeAngle.Text = "DEG";
                kAngle++;
            }
            else
            {
                iTheckMathFunction.Deg = false;
                iTheckMathFunction.Rad = true;
                labelModeAngle.Text = "RAD";
                kAngle = 0;
            }
            textBoxPoseOperation.Focus();
            //textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            buttonReset_Click();
        }
        private void buttonReset_Click()
        {
            labelOpp.Text = string.Empty;
            textBoxAns.Text = "0";
            textBoxPoseOperation.Text = string.Empty;
            OApres = iTheckClassMXDeltaBox.TabOperationLength();
            OAvant = -1;
            textBoxPoseOperation.Focus();
        }

        private void buttonCalculer_Click(object sender, EventArgs e)
        {
            Calcule();
            textBoxPoseOperation.Focus();
        }

        private void KeyDownMethode(object sender, KeyEventArgs e)
        {
            KeyDownMethode(e);
        }
        public void KeyDownMethode(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) Calcule();
            if (e.Control == true && e.KeyCode == Keys.Space)
            {
                textBoxPoseOperation.Text = textBoxAns.Text;
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
            }
            if (e.KeyCode == Keys.Delete)
            {
                textBoxPoseOperation.Text = string.Empty;
                labelOpp.Text = string.Empty;
                OApres = iTheckClassMXDeltaBox.TabOperationLength();
                OAvant = -1;
                textBoxPoseOperation.Focus();
            }
            if (e.Control == true && e.KeyCode == Keys.Left) buttonGauche_Click();
            if (e.Control == true && e.KeyCode == Keys.Right) buttonDroite_Click();
            if (e.Control == true && e.KeyCode == Keys.Add)
            {
                textBoxPoseOperation.Text += textBoxAns.Text;
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
            }
            if (e.Control == true && e.KeyCode == Keys.I)
            {
                MessageForm Mxg = new MessageForm();
                Mxg.Title.Text = "AMGT Info";
                Mxg.BackColor = Color.Gray;
                Mxg.Title.ForeColor = Color.Pink;
                Mxg.Message.ForeColor = Color.White;
                Mxg.Opacity = 80;
                Mxg.Message.Text = "\tInfo Logiciel\nVersion: AMGT MegaCodiThec 4.2.00\nAuteur: Arnold Yanick TCHEGNINOUGBO\nSystème d'exploitation: 32bits\nSécurité: Aucun\nType: Application Windows\nDate de création: 25 Septembre 2015\nHeure de création: 11:38";
                Mxg.StartPosition = FormStartPosition.CenterScreen;
                Mxg.ShowDialog();
            }
            if (e.Control == true && e.KeyCode == Keys.D)
            {
                buttonDRG_Click2();
            }
            if (e.Alt && e.KeyCode == Keys.A) buttonSh_Click();
            if (e.Alt && e.KeyCode == Keys.B) buttonCh_Click();
            if (e.Alt && e.KeyCode == Keys.C) buttonTh_Click();
            if (e.Alt && e.KeyCode == Keys.D) buttonCoth_Click();
            if (e.Alt && e.KeyCode == Keys.E) buttonArgsh_Click();
            if (e.Alt && e.KeyCode == Keys.F) buttonArgch_Click();
            if (e.Alt && e.KeyCode == Keys.G) buttonArgth_Click();
            if (e.Alt && e.KeyCode == Keys.H) buttonPGCD_Click();
            if (e.Alt && e.KeyCode == Keys.I) buttonPPCM_Click();
            if (e.Alt && e.KeyCode == Keys.J) buttonExp2_Click();
            if (e.Alt && e.KeyCode == Keys.K) buttonLn_Click();
            if (e.Alt && e.KeyCode == Keys.L) buttonLog_Click();
            if (e.Alt && e.KeyCode == Keys.M) buttonDiv_Click();
            if (e.Alt && e.KeyCode == Keys.N) buttonReste_Click();
            if (e.Alt && e.KeyCode == Keys.O) buttonSin_Click();
            if (e.Alt && e.KeyCode == Keys.P) buttonPi_Click();
            if (e.Alt && e.KeyCode == Keys.Q) buttonCos_Click();
            if (e.Alt && e.KeyCode == Keys.R) buttonTan_Click();
            if (e.Alt && e.KeyCode == Keys.S) buttonCotan_Click();
            if (e.Alt && e.KeyCode == Keys.T) buttonArcsin_Click();
            if (e.Alt && e.KeyCode == Keys.U) buttonArccos_Click();
            if (e.Alt && e.KeyCode == Keys.V) buttonArctan_Click();
            if (e.Alt && e.KeyCode == Keys.W) buttonArccotan_Click();
            if (e.Alt && e.KeyCode == Keys.X) buttonParenthese_Click();
            if (e.Alt && e.KeyCode == Keys.Y) buttonValeurAbs_Click();
            if (e.Alt && e.KeyCode == Keys.Z) buttonSqrt_Click();
            if (e.Control && e.KeyCode == Keys.Q) buttonSign_Click();
            if (e.Control && e.KeyCode == Keys.E) buttonDeg_Click();
            if (e.Control && e.KeyCode == Keys.R) buttonRad_Click();
            if (e.Alt && e.KeyCode == Keys.F2) buttonFonctionDerivee_Click();
            if ((e.Control && e.KeyCode == Keys.Delete) || e.KeyCode == Keys.Escape) buttonReset_Click();
            if (e.Control && e.KeyCode == Keys.Up) rectangleShapeGlobal_DoubleClick1();
            if (e.Control && e.KeyCode == Keys.Down) rectangleShapeGlobal_DoubleClick2();
            if (e.Control && e.KeyCode == Keys.F1) OptionUsingInfo_Click();
        }

        private void buttonGauche_Click(object sender, EventArgs e)
        {
            buttonGauche_Click();
        }
        private void buttonGauche_Click()
        {
            try
            {
                if (OApres != 0)
                {
                    OApres--;
                    labelOpp.Text = "" + (OApres + 1);
                    OAvant = OApres;
                    textBoxPoseOperation.Text = ParcourOperation(OApres);
                    Calcule2();
                }
            }
            catch { }
        }

        private void buttonDroite_Click(object sender, EventArgs e)
        {
            buttonDroite_Click();
        }
        private void buttonDroite_Click()
        {
            try
            {
                if (OAvant != iTheckClassMXDeltaBox.TabOperationLength() - 1)
                {
                    OAvant++;
                    OApres = OAvant;
                    labelOpp.Text = "" + (OAvant + 1);
                    textBoxPoseOperation.Text = ParcourOperation(OApres);
                    Calcule2();
                }
            }
            catch
            {
            }
        }

        private void MooseLeaveMethode(object sender, EventArgs e)
        {
            labelInfo.Text = string.Empty;
            labelGauche.ForeColor = Color.White;
        }

        private void MooseEnterMethode(object sender, EventArgs e)
        {
            if (sender is Button)
            {
                Button bt = (Button)sender;
                if (bt.Name == "buttonSh") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCh") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonPPCM") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonTh") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonPGCD") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonReste") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonDiv") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonExp") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonLn") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonLog") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCombinaison") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArrangement") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonPUplet") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonPi") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonSin") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCos") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonTan") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArcsin") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArccos") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArctan") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonParenthese") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonExposant") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonValeurAbs") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonFactorielle") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonSqrt") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonPuissance10") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonDelete") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonFonctionDerivee") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonSoustration") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonAddition") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonMultiplication") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonDivision") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonEgale") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button0") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button1") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button2") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button3") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button4") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button5") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button6") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button7") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button8") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "button9") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonVirgule") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonReset") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonOff") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonDRG") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCalculer") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCursorLeft") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCursorRight") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonParenthese1") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonParenthese2") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArgsh") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArgch") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArgth") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonArccotan") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonVAbs1") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonVAbs2") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonSign") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonDeg") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonRad") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCotan") labelInfo.Text = bt.Tag.ToString();
                else if (bt.Name == "buttonCoth") labelInfo.Text = bt.Tag.ToString();
            }
            if (sender is ToolStripButton)
            {
                ToolStripButton but = (ToolStripButton)sender;
                if (but.Name == "buttonGauche") labelInfo.Text = but.Tag.ToString();
                else if (but.Name == "buttonDroite") labelInfo.Text = but.Tag.ToString();
                else if (but.Name == "OutilMenuOption") labelInfo.Text = but.Tag.ToString();
            }
            if (sender is Label)
            {
                Label lb = (Label)sender;
                if (lb.Name == "labelGauche")
                {
                    lb.ForeColor = Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
                    labelInfo.Text = lb.Tag.ToString();
                }
            }
        }

        private void buttonCursorLeft_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxPoseOperation.SelectionStart != 0) textBoxPoseOperation.SelectionStart--;
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.Select(textBoxPoseOperation.SelectionStart, 0);
            }
            catch { }
        }

        private void buttonCursorRight_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBoxPoseOperation.SelectionStart != textBoxPoseOperation.TextLength) textBoxPoseOperation.SelectionStart++;
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.Select(textBoxPoseOperation.SelectionStart, 0);
            }
            catch { }
        }

        private void rectangleShapeGlobal_Click(object sender, EventArgs e)
        {
            textBoxPoseOperation.Focus();
        }

        private void buttonParenthese1_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "(", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonParenthese2_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, ")", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void textBoxAns_Click(object sender, EventArgs e)
        {
            textBoxPoseOperation.Focus();
        }

        private void textBoxAns_DoubleClick(object sender, EventArgs e)
        {
            textBoxPoseOperation.Text = textBoxAns.Text;
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
        }

        private void labelGauche_Click(object sender, EventArgs e)
        {
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.Select(0, 0);
        }

        private void labelGauche_DoubleClick(object sender, EventArgs e)
        {
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.Select(textBoxPoseOperation.TextLength, 0);
        }

        private void buttonArgsh_Click(object sender, EventArgs e)
        {
            buttonArgsh_Click();
        }
        private void buttonArgsh_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argsh", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argsh'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
        }

        private void buttonArgch_Click(object sender, EventArgs e)
        {
            buttonArgch_Click();
        }
        private void buttonArgch_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argch", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argch'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
        }

        private void buttonArgth_Click(object sender, EventArgs e)
        {
            buttonArgth_Click();
        }
        private void buttonArgth_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argth", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "argth'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
        }

        private void rectangleShapeGlobal_DoubleClick(object sender, EventArgs e)
        {
            if (!SmallLength) rectangleShapeGlobal_DoubleClick1();
            else rectangleShapeGlobal_DoubleClick2();
        }
        private void rectangleShapeGlobal_DoubleClick1()
        {
            if (!SmallLength)
            {
                this.rectangleShapeGlobal.Height = 110;
                this.Height = this.rectangleShapeGlobal.Height + 56;
                SmallLength = true;
                button0.Visible = false;
                button1.Visible = false;
                button2.Visible = false;
                button3.Visible = false;
                button4.Visible = false;
                button5.Visible = false;
                button6.Visible = false;
                button7.Visible = false;
                button8.Visible = false;
                button0.Visible = false;
                button9.Visible = false;
                buttonVirgule.Visible = false;
                buttonSh.Visible = false;
                buttonCh.Visible = false;
                buttonTh.Visible = false;
                buttonCoth.Visible = false;
                buttonArgsh.Visible = false;
                buttonArgch.Visible = false;
                buttonArgth.Visible = false;
                buttonPGCD.Visible = false;
                buttonPPCM.Visible = false;
                buttonReste.Visible = false;
                buttonLn.Visible = false;
                buttonPi.Visible = false;
                buttonExp.Visible = false;
                buttonLog.Visible = false;
                buttonCombinaison.Visible = false;
                buttonArrangement.Visible = false;
                buttonPUplet.Visible = false;
                buttonDiv.Visible = false;
                buttonSin.Visible = false;
                buttonCos.Visible = false;
                buttonTan.Visible = false;
                buttonCotan.Visible = false;
                buttonArcsin.Visible = false;
                buttonArccos.Visible = false;
                buttonArccotan.Visible = false;
                buttonParenthese.Visible = false;
                buttonExposant.Visible = false;
                buttonValeurAbs.Visible = false;
                buttonFactorielle.Visible = false;
                buttonSqrt.Visible = false;
                buttonCursorLeft.Visible = false;
                buttonCursorRight.Visible = false;
                buttonParenthese1.Visible = false;
                buttonParenthese2.Visible = false;
                buttonVAbs1.Visible = false;
                buttonVAbs2.Visible = false;
                buttonDelete.Visible = false;
                buttonFonctionDerivee.Visible = false;
                buttonDivision.Visible = false;
                buttonMultiplication.Visible = false;
                buttonSoustration.Visible = false;
                buttonAddition.Visible = false;
                buttonEgale.Visible = false;
                rectangleShape4.Visible = false;
                rectangleShape5.Visible = false;
                rectangleShape6.Visible = false;
                rectangleShape7.Visible = false;
                rectangleShapeOperateur.Visible = false;
                rectangleShapeNum.Visible = false;
                rectangleShapeFonction.Visible = false;
                labelFonction.Visible = false;
                labelOpérateur.Visible = false;
                labelPaveNumeric.Visible = false;
            }
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
        }
        private void rectangleShapeGlobal_DoubleClick2()
        {
            if (SmallLength)
            {
                this.rectangleShapeGlobal.Height = 433;
                this.Height = this.rectangleShapeGlobal.Height + 56;
                SmallLength = false;
                button0.Visible = true;
                button1.Visible = true;
                button2.Visible = true;
                button3.Visible = true;
                button4.Visible = true;
                button5.Visible = true;
                button6.Visible = true;
                button7.Visible = true;
                button8.Visible = true;
                button0.Visible = true;
                button9.Visible = true;
                buttonVirgule.Visible = true;
                buttonSh.Visible = true;
                buttonCh.Visible = true;
                buttonCoth.Visible = true;
                buttonTh.Visible = true;
                buttonArgsh.Visible = true;
                buttonArgch.Visible = true;
                buttonArgth.Visible = true;
                buttonPGCD.Visible = true;
                buttonPPCM.Visible = true;
                buttonReste.Visible = true;
                buttonLn.Visible = true;
                buttonPi.Visible = true;
                buttonExp.Visible = true;
                buttonLog.Visible = true;
                buttonCombinaison.Visible = true;
                buttonArrangement.Visible = true;
                buttonPUplet.Visible = true;
                buttonDiv.Visible = true;
                buttonSin.Visible = true;
                buttonCos.Visible = true;
                buttonTan.Visible = true;
                buttonCotan.Visible = true;
                buttonArcsin.Visible = true;
                buttonArccos.Visible = true;
                buttonArccotan.Visible = true;
                buttonParenthese.Visible = true;
                buttonExposant.Visible = true;
                buttonValeurAbs.Visible = true;
                buttonFactorielle.Visible = true;
                buttonSqrt.Visible = true;
                buttonCursorLeft.Visible = true;
                buttonCursorRight.Visible = true;
                buttonParenthese1.Visible = true;
                buttonParenthese2.Visible = true;
                buttonVAbs1.Visible = true;
                buttonVAbs2.Visible = true;
                buttonDelete.Visible = true;
                buttonFonctionDerivee.Visible = true;
                buttonDivision.Visible = true;
                buttonMultiplication.Visible = true;
                buttonSoustration.Visible = true;
                buttonAddition.Visible = true;
                buttonEgale.Visible = true;
                rectangleShape4.Visible = true;
                rectangleShape5.Visible = true;
                rectangleShape6.Visible = true;
                rectangleShape7.Visible = true;
                rectangleShapeOperateur.Visible = true;
                rectangleShapeNum.Visible = true;
                rectangleShapeFonction.Visible = true;
                labelFonction.Visible = true;
                labelOpérateur.Visible = true;
                labelPaveNumeric.Visible = true;
            }
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
        }

        private void buttonCotan_Click(object sender, EventArgs e)
        {
            buttonCotan_Click();
        }
        private void buttonCotan_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "cotan", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "cotan'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 6, 0);
            }
        }

        private void buttonArccotan_Click(object sender, EventArgs e)
        {
            buttonArccotan_Click();
        }
        private void buttonArccotan_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arccotg", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 7, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "arccotg'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 8, 0);
            }
        }

        private void buttonVAbs1_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "[", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonVAbs2_Click(object sender, EventArgs e)
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "]", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 1, 0);
        }

        private void buttonSign_Click(object sender, EventArgs e)
        {
            buttonSign_Click();
        }
        private void buttonSign_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "sign", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 4, 0);
        }

        private void buttonRad_Click(object sender, EventArgs e)
        {
            buttonRad_Click();
        }
        private void buttonRad_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "rad", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 3, 0);
        }

        private void buttonDeg_Click(object sender, EventArgs e)
        {
            buttonDeg_Click();
        }
        private void buttonDeg_Click()
        {
            int ind = textBoxPoseOperation.SelectionStart;
            textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "deg", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
            textBoxPoseOperation.Focus();
            textBoxPoseOperation.SelectionLength = 0;
            textBoxPoseOperation.Select(ind + 3, 0);
        }

        private void OptionResultat_Click(object sender, EventArgs e)
        {
            OptionAnsForm OptionAns = new OptionAnsForm(this);
            OptionAns.Show();
        }

        private void MXDeltaBoxForm_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private void buttonCoth_Click(object sender, EventArgs e)
        {
            buttonCoth_Click();
        }
        private void buttonCoth_Click()
        {
            if (!this.FonctionDerive)
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "coth", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 4, 0);
            }
            else
            {
                int ind = textBoxPoseOperation.SelectionStart;
                textBoxPoseOperation.Text = iTheckClassMXDeltaBox.Saisie(textBoxPoseOperation.Text, "coth'", textBoxPoseOperation.SelectionStart, textBoxPoseOperation.TextLength);
                textBoxPoseOperation.Focus();
                textBoxPoseOperation.SelectionLength = 0;
                textBoxPoseOperation.Select(ind + 5, 0);
            }
        }

        private void OptionUsingInfo_Click(object sender, EventArgs e)
        {
            OptionUsingInfo_Click();
        }
        private void OptionUsingInfo_Click()
        {
            UsingInfo Info = new UsingInfo();
            Info.Show();
        }

        
    }
}
