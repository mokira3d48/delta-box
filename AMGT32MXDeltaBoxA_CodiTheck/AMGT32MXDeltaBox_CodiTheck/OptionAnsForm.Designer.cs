﻿namespace AMGT32MXDeltaBox_CodiTheck
{
    partial class OptionAnsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OptionAnsForm));
            this.Outil = new System.Windows.Forms.ToolStrip();
            this.buttonOK = new System.Windows.Forms.ToolStripButton();
            this.Option = new System.Windows.Forms.ToolStripSplitButton();
            this.OptionNormal = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionFrationReal = new System.Windows.Forms.ToolStripMenuItem();
            this.OptionFractionAngle = new System.Windows.Forms.ToolStripMenuItem();
            this.radioButtonFaiblePrecision = new System.Windows.Forms.RadioButton();
            this.radioButtonDoublePrecision = new System.Windows.Forms.RadioButton();
            this.trackBarFraction = new System.Windows.Forms.TrackBar();
            this.labelFraction = new System.Windows.Forms.Label();
            this.Outil.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFraction)).BeginInit();
            this.SuspendLayout();
            // 
            // Outil
            // 
            this.Outil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Outil.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.buttonOK,
            this.Option});
            this.Outil.Location = new System.Drawing.Point(0, 0);
            this.Outil.Name = "Outil";
            this.Outil.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.Outil.Size = new System.Drawing.Size(506, 25);
            this.Outil.TabIndex = 0;
            this.Outil.Text = "toolStrip1";
            // 
            // buttonOK
            // 
            this.buttonOK.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.buttonOK.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOK.ForeColor = System.Drawing.SystemColors.Window;
            this.buttonOK.Image = ((System.Drawing.Image)(resources.GetObject("buttonOK.Image")));
            this.buttonOK.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.buttonOK.Name = "buttonOK";
            this.buttonOK.Size = new System.Drawing.Size(26, 22);
            this.buttonOK.Text = "OK";
            this.buttonOK.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // Option
            // 
            this.Option.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.Option.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OptionNormal,
            this.OptionFrationReal,
            this.OptionFractionAngle});
            this.Option.Image = ((System.Drawing.Image)(resources.GetObject("Option.Image")));
            this.Option.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.Option.Name = "Option";
            this.Option.Size = new System.Drawing.Size(32, 22);
            this.Option.Text = "toolStripSplitButton1";
            // 
            // OptionNormal
            // 
            this.OptionNormal.Name = "OptionNormal";
            this.OptionNormal.Size = new System.Drawing.Size(171, 22);
            this.OptionNormal.Text = "Normal";
            this.OptionNormal.Click += new System.EventHandler(this.OptionNormal_Click);
            // 
            // OptionFrationReal
            // 
            this.OptionFrationReal.Name = "OptionFrationReal";
            this.OptionFrationReal.Size = new System.Drawing.Size(171, 22);
            this.OptionFrationReal.Text = "Fraction réelle";
            this.OptionFrationReal.Click += new System.EventHandler(this.OptionFrationReal_Click);
            // 
            // OptionFractionAngle
            // 
            this.OptionFractionAngle.Name = "OptionFractionAngle";
            this.OptionFractionAngle.Size = new System.Drawing.Size(171, 22);
            this.OptionFractionAngle.Text = "Fratcion angulaire";
            this.OptionFractionAngle.Click += new System.EventHandler(this.OptionFractionAngle_Click);
            // 
            // radioButtonFaiblePrecision
            // 
            this.radioButtonFaiblePrecision.AutoSize = true;
            this.radioButtonFaiblePrecision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.radioButtonFaiblePrecision.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButtonFaiblePrecision.ForeColor = System.Drawing.SystemColors.Window;
            this.radioButtonFaiblePrecision.Location = new System.Drawing.Point(79, 5);
            this.radioButtonFaiblePrecision.Name = "radioButtonFaiblePrecision";
            this.radioButtonFaiblePrecision.Size = new System.Drawing.Size(99, 17);
            this.radioButtonFaiblePrecision.TabIndex = 1;
            this.radioButtonFaiblePrecision.TabStop = true;
            this.radioButtonFaiblePrecision.Text = "Faible Précision";
            this.radioButtonFaiblePrecision.UseVisualStyleBackColor = false;
            this.radioButtonFaiblePrecision.CheckedChanged += new System.EventHandler(this.radioButtonFaiblePrecision_CheckedChanged);
            // 
            // radioButtonDoublePrecision
            // 
            this.radioButtonDoublePrecision.AutoSize = true;
            this.radioButtonDoublePrecision.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.radioButtonDoublePrecision.Cursor = System.Windows.Forms.Cursors.Hand;
            this.radioButtonDoublePrecision.ForeColor = System.Drawing.SystemColors.Window;
            this.radioButtonDoublePrecision.Location = new System.Drawing.Point(184, 5);
            this.radioButtonDoublePrecision.Name = "radioButtonDoublePrecision";
            this.radioButtonDoublePrecision.Size = new System.Drawing.Size(105, 17);
            this.radioButtonDoublePrecision.TabIndex = 2;
            this.radioButtonDoublePrecision.TabStop = true;
            this.radioButtonDoublePrecision.Text = "Double Précision";
            this.radioButtonDoublePrecision.UseVisualStyleBackColor = false;
            this.radioButtonDoublePrecision.CheckedChanged += new System.EventHandler(this.radioButtonDoublePrecision_CheckedChanged);
            // 
            // trackBarFraction
            // 
            this.trackBarFraction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.trackBarFraction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.trackBarFraction.Location = new System.Drawing.Point(295, 0);
            this.trackBarFraction.Name = "trackBarFraction";
            this.trackBarFraction.Size = new System.Drawing.Size(146, 45);
            this.trackBarFraction.TabIndex = 3;
            this.trackBarFraction.ValueChanged += new System.EventHandler(this.trackBarFraction_ValueChanged);
            // 
            // labelFraction
            // 
            this.labelFraction.AutoSize = true;
            this.labelFraction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.labelFraction.ForeColor = System.Drawing.SystemColors.Window;
            this.labelFraction.Location = new System.Drawing.Point(447, 7);
            this.labelFraction.Name = "labelFraction";
            this.labelFraction.Size = new System.Drawing.Size(35, 13);
            this.labelFraction.TabIndex = 4;
            this.labelFraction.Text = "label1";
            // 
            // OptionAnsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.ClientSize = new System.Drawing.Size(506, 30);
            this.Controls.Add(this.labelFraction);
            this.Controls.Add(this.trackBarFraction);
            this.Controls.Add(this.radioButtonDoublePrecision);
            this.Controls.Add(this.radioButtonFaiblePrecision);
            this.Controls.Add(this.Outil);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OptionAnsForm";
            this.Opacity = 0.95D;
            this.Text = "OptionAnsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.OptionAnsForm_FormClosing);
            this.Load += new System.EventHandler(this.OptionAnsForm_Load);
            this.Outil.ResumeLayout(false);
            this.Outil.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarFraction)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip Outil;
        private System.Windows.Forms.ToolStripButton buttonOK;
        private System.Windows.Forms.ToolStripSplitButton Option;
        private System.Windows.Forms.ToolStripMenuItem OptionNormal;
        private System.Windows.Forms.ToolStripMenuItem OptionFrationReal;
        private System.Windows.Forms.ToolStripMenuItem OptionFractionAngle;
        private System.Windows.Forms.RadioButton radioButtonFaiblePrecision;
        private System.Windows.Forms.RadioButton radioButtonDoublePrecision;
        private System.Windows.Forms.TrackBar trackBarFraction;
        private System.Windows.Forms.Label labelFraction;
    }
}